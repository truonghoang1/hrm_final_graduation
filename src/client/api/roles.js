import {ApiClient} from "./index"

const getRolesAPI = payload =>ApiClient.get(`admin/api/role?limit=${payload.limit||10}&&page=${payload.page||1}`)
const addRolesAPI = payload =>ApiClient.post(`admin/api/role`,payload)
const updateRolesAPI = payload =>ApiClient.put(`admin/api/role/${payload._id}`,payload)
const deleteRolesAPI = payload =>ApiClient.delete(`admin/api/role/${payload.id}`)
const detailRolesAPI = payload=>ApiClient.get(`admin/api/role/${payload.id}`)
export {
    getRolesAPI,addRolesAPI,updateRolesAPI,deleteRolesAPI,detailRolesAPI
}