import {ApiClient,exportXlsx} from "./index"

const addEmployee = payload =>ApiClient.post(`admin/api/employee?limit=${payload.limit||10}`,payload)
const getEmployee = payload =>ApiClient.get(`admin/api/employee?page=${payload.page||1}&&limit=${payload.limit||10}&&keySearch=${payload.keySearch}`)
const getDetailEmployee =payload =>ApiClient.get(`admin/api/employee/${payload.id}`)
const updateEmployee = payload =>ApiClient.put(`admin/api/employee/${payload._id}`,payload)
const deleteEmployee = payload => ApiClient.delete(`admin/api/employee/${payload.id}`)

const exportEmployee = payload => exportXlsx(`admin/api/employee/export`,payload)

const importEmployee = payload=>  ApiClient.post(`admin/api/employee/import`, payload)

export{
    addEmployee,getEmployee,updateEmployee,deleteEmployee,getDetailEmployee,importEmployee,exportEmployee
}