import {ApiClient} from "./index"

const addAccount = payload =>ApiClient.post(`admin/api/account?limit=${payload.limit||10}`,payload)
const getAccount = payload =>ApiClient.get(`admin/api/account?page=${payload.page||1}&&limit=${payload.limit||10}`)
const getDetailAccount =payload =>ApiClient.get(`admin/api/account/${payload.id}`)
const updateAccount = payload =>ApiClient.put(`admin/api/account/${payload._id}`,payload)
const deleteAccount = payload =>ApiClient.delete(`admin/api/account/${payload.id}`)

export{
    addAccount,getAccount,updateAccount,deleteAccount,getDetailAccount
}