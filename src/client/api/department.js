import {ApiClient} from "./index"

const addDepartment = payload =>ApiClient.post(`admin/api/department?limit=${payload.limit||10}`,payload)
const getDepartment = payload =>ApiClient.get(`admin/api/department?page=${payload.page||1}&&limit=${payload.limit||10}`)
const getDetailDepartment =payload =>ApiClient.get(`admin/api/department/${payload.id}`)
const updateDepartment = payload =>ApiClient.put(`admin/api/department/${payload._id}`,payload)
const deleteDepartment = payload =>ApiClient.delete(`admin/api/department/${payload.id}`)

export{
    addDepartment,getDepartment,updateDepartment,deleteDepartment,getDetailDepartment
}