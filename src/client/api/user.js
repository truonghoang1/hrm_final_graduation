import { ApiClient } from './index'
const signin = payload => ApiClient.post(`admin/api/user/signin`,payload)
const signup = payload => ApiClient.post(`admin/api/user/signup`,payload)
//end-user
const signupGuest = payload => ApiClient.post(`user/api/register`,payload)
const activationAccount =payload => ApiClient.post(`user/api/activation`,payload)
const forgotPassword = payload =>ApiClient.post(`user/api/forgot-password`,payload)
const resetPassword = payload =>ApiClient.post(`user/api/reset`,payload)
const changePwAPI =payload=>ApiClient.post("/user/api/changepass",payload)
//
const updateProfileApi =payload=>ApiClient.post(`/user/api/update-profile`,payload)
const statistialAPI = payload=>ApiClient.get("admin/api/user/statistial",payload)
//get like count
const getCountLikeFacebook = payload =>ApiClient.get(process.env.URL_GET_COUNT_LIKE_FACEBOOK,payload)
//offline
export {
    signin, signup,signupGuest,activationAccount,forgotPassword,resetPassword,statistialAPI,getCountLikeFacebook,changePwAPI,updateProfileApi
}