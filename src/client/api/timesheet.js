import {ApiClient} from "./index"

const addTimeSheetAdmin = payload =>ApiClient.post(`admin/api/timesheet?limit=${payload.limit||10}`,payload)
const getTimeSheetAdmin = payload =>ApiClient.get(`admin/api/timesheet?page=${payload.page||1}&&limit=${payload.limit||10}`)
const getDetailTimeSheetAdmin =payload =>ApiClient.get(`admin/api/timesheet/${payload.id}`)
const updateTimeSheetAdmin = payload =>ApiClient.put(`admin/api/timesheet/${payload.id}`,payload)
const deleteTimeSheetAdmin = payload => ApiClient.delete(`admin/api/timesheet/${payload.id}`)

const exportTimeSheetAdmin = payload => ApiClient.post(`admin/api/export/timesheet`, payload)
const importTimeSheetAdmin = payload=> ApiClient.post(`admin/api/import/timesheet`,payload)

const getLeaveDateEmployee =()=>ApiClient.get(`user/api/leave-date`)
//time employee
const getListTimeEmployee = payload=>ApiClient.get(`user/api/timesheet?page=1&&limit=20`)
const addLogTimeEmployee =()=>ApiClient.post(`user/api/log-time`)
const getListLeaveAppEmployee = (payload)=>ApiClient.get(`user/api/leave-app?page=${payload.page}&&limit=${payload.limit}`)
const addLeaveApplication =(payload)=>ApiClient.post(`user/api/leave-app`,payload)
export{getListTimeEmployee,addLogTimeEmployee,getLeaveDateEmployee,getListLeaveAppEmployee,addLeaveApplication,
    addTimeSheetAdmin,getTimeSheetAdmin,updateTimeSheetAdmin,deleteTimeSheetAdmin,getDetailTimeSheetAdmin,importTimeSheetAdmin,exportTimeSheetAdmin
}