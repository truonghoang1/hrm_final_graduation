import {ApiClient,exportXlsx} from "./index"

const addSalary = payload =>ApiClient.post(`admin/api/salary?limit=${payload.limit||10}`,payload)
const getSalary = payload =>ApiClient.get(`admin/api/salary?page=${payload.page||1}&&limit=${payload.limit||10}&&keySearch=${payload.keySearch}`)
const getDetailSalary =payload =>ApiClient.get(`admin/api/salary/${payload.id}`)
const updateSalary = payload =>ApiClient.put(`admin/api/salary/${payload.id}`,payload)
const deleteSalary = payload =>ApiClient.delete(`admin/api/salary/${payload.id}`)
const importSalary =payload=>ApiClient.post("admin/api/import/salary",payload)
const exportSalary =payload =>exportXlsx(`admin/api/export/salary`,payload)
export{
    addSalary,getSalary,updateSalary,deleteSalary,getDetailSalary,importSalary,exportSalary
}