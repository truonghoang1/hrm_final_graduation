import {ApiClient} from "./index"

const addBonus = payload =>ApiClient.post(`admin/api/bonus?limit=${payload.limit||10}`,payload)
const getBonus = payload =>ApiClient.get(`admin/api/bonus?page=${payload.page||1}&&limit=${payload.limit||10}`)
const getDetailBonus =payload =>ApiClient.get(`admin/api/bonus/${payload.id}`)
const updateBonus = payload =>ApiClient.put(`admin/api/bonus/${payload._id}`,payload)
const deleteBonus = payload =>ApiClient.delete(`admin/api/bonus/${payload.id}`)

export{
    addBonus,getBonus,updateBonus,deleteBonus,getDetailBonus
}