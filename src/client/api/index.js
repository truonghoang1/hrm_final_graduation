import axios from "axios";
import Cookie from "js-cookie";
import queryString from "query-string";
import Swal from "sweetalert2";
import moment from "moment";

function createAxios() {
  var axiosInstant = axios.create();
  axiosInstant.defaults.baseURL = "http://localhost:3001";
  // axiosInstant.defaults.baseURL = "http://localhost:8888";
  axiosInstant.defaults.timeout = 20000;
  axiosInstant.defaults.headers = { "Content-Type": "application/json" };
  axiosInstant.defaults.headers = { "access-control-allow-origin": "*" };

  axiosInstant.interceptors.request.use(
    async (config) => {
      config.headers.token = Cookie.get("SESSION_ID");
      // Cookie.get('SESSION_ID')
      return config;
    },
    (error) => {
      Promise.reject(error);
    }
  );
  axiosInstant.interceptors.response.use(
    (response) => {
      if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
        // Reactotron.apisauce(response);
      } else {
        // production code
      }

      if (response.data.code === 403) {
        Cookie.remove("SESSION_ID");
        window.location.reload();
      } else if (response.data.status !== 1)
        setTimeout(() => {
          alert(response.data.message);
        }, 300);
      return response;
    },
    (error) => {
      console.log(error);
      if (error.response.data.code === 403|| error.response.data.code===401) {
        Swal.fire({
          titleText: error.response.data.message,
          icon: "error",
        });
        window.reload();
      } else {
        Swal.fire({
          titleText: error.response.data.message,
          icon: "error",
        });
      }
    }
  );
  return axiosInstant;
}

export const axiosClient = createAxios();

/* Support function */
function handleResult(api) {
  return api.then((res) => {
    // alert(JSON.stringify(res.data));
    if (res.data.status !== 1) {
      if (res.data.code === 403) {
        Cookie.remove("SESSION_ID");
        alert("Phiên đăng nhập hết hạn.");
      }
      return Promise.reject(res.data);
    }
    return Promise.resolve(res.data);
  });
}
// handle url
function handleUrl(url, query) {
  return queryString.stringifyUrl({ url: url, query });
}
// export file xlsx
export function exportXlsx(url, payload) {
  const hostName = "http://localhost:3001/";
  // axios({

  //   url: `${hostName+url}?start=${payload.start}&&end=${payload.end}`,
  //   method: 'POST',

  //   responseType: 'blob', // important
  // }).then((response) => {
  //   const url = window.URL.createObjectURL(new Blob([response.data]));
  //   const link = document.createElement('a');
  //   link.href = url;
  //   link.setAttribute('download', `${new Date()}.xlsx`);
  //   document.body.appendChild(link);
  //   link.click();
  // });
  axios
    .post(
      `${hostName + url}?start=${payload.start}&&end=${payload.end}`,
      payload.opsSkip,
      { responseType: "blob" }
    )
    .then(function (response) {
      console.log(response);
      let fileName = `${moment(new Date()).format("YYYY-MM-DD")}`;
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        // IE variant
        window.navigator.msSaveOrOpenBlob(
          new Blob([response.data], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          }),
          fileName
        );
      } else {
        const url = window.URL.createObjectURL(
          new Blob([response.data], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          })
        );
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute(
          "download",
          fileName
        );
        document.body.appendChild(link);
        link.click();
      }
    });
}

export const ApiClient = {
  get: (url, payload) => handleResult(axiosClient.get(handleUrl(url, payload))),
  post: (url, payload) => handleResult(axiosClient.post(url, payload)),
  put: (url, payload) => handleResult(axiosClient.put(url, payload)),
  path: (url, payload) => handleResult(axiosClient.patch(url, payload)),
  delete: (url, payload) => handleResult(axiosClient.delete(url, payload)),
};
