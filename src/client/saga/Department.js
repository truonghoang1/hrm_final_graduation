import { put, select, takeLatest } from "redux-saga/effects";
import {
  addDepartment,
  getDepartment,
  getDetailDepartment,
  updateDepartment,
  deleteDepartment,
} from "../api/department";
import * as actions from "../actions/department";
import Swal from "sweetalert2";
function* addDepartmentSaga({ payload }) {
  try {
    
      const { limit } = yield select((state) => state.departmentState);
      const res = yield addDepartment(payload);
      if (res.code == 200) {
        yield put(actions.addDepartmentsuccess(res.message));
        yield Swal.fire({
          titleText: " Add Success",
          icon: "success",
        });
        yield put(actions.getDepartmentRequest({page:Math.ceil(res.data.total/limit),limit}))
       
      }
    
  } catch (error) {
   
      yield put(actions.addDepartmentfailure(" server error 500"));
    
  }
}
function* getDepartmentSaga({ payload }) {
  try {
    const { keySearch } = yield select((state) => state.departmentState);
    if (keySearch) {
      const res = yield getDepartment({
        keySearch,
        page: payload.page,
        limit: payload.limit,
      });
      if (res.message == 200) {
        yield put(
          actions.getDepartmentsuccess({
            data: res.data.data,
            totalPage: res.data.totalPage,
            page: res.data.page,
            keySearch: res.data.keySearch,
          })
        );
      }
    } else {
      const res = yield getDepartment(payload);
      if (res.code == 200) {
        yield put(
          actions.getDepartmentsuccess({
            data: res.data.data,
            totalPage: res.data.totalPage,
            page: res.data.page,
          })
        );
      }
    }
  } catch (error) {
      yield put(actions.getDepartmentDetailfailure(" server error 500"));
  }
}
function* updateDepartmentSaga({ payload }) {
  try {
      const { limit, page, keySearch } = yield select(
        (state) => state.departmentState
      );
      if (keySearch) {
        const res = yield updateDepartment(payload);
        if (res.code == 200) {
          yield put(actions.updateDepartmentsuccess(res.message));
          yield Swal.fire({
            title:res.message,
            icon:"success"
          })
          yield put(actions.getDepartmentRequest({page,limit,keySearch}))
        }
      } else {
        const res = yield updateDepartment(payload);
        if (res.code == 200) {
          yield put(actions.updateDepartmentsuccess(res.message));
          yield Swal.fire({
            title:res.message,
            icon:"success"
          })
          yield put(actions.getDepartmentRequest({page,limit}))
        }
      }
    
  } catch (error) {}
}
function* deleteDepartmentSaga({ payload }) {
  try {
   
      const res = yield deleteDepartment(payload);
      const { limit, page, totalPage, data, keySearch } = yield select(
        (state) => state.departmentState
      );
      if (res.code== 200) {
        
        yield put(actions.deleteDepartmentsuccess(res.message));
        if (keySearch) {
          if (totalPage > 1 && totalPage === pageIndex && data.length === 1) {
            yield put(
              actions.getDepartmentRequest({
                keySearch: keySearch || "",
                page: page - 1,
                limit,
              })
            );
          } else if (
            totalPage === 1 &&
            totalPage === pageIndex &&
            data.length === 1
          ) {
            yield put(
              actions.getDepartmentRequest({
                keySearch,
                page: page,
                limit,
              })
            );
          } else {
            yield put(
              actions.getDepartmentRequest({
                keySearch,
                page: page,
                limit,
              })
            );
          }
        } else {
          if (totalPage > 1 && totalPage === pageIndex && data.length === 1) {
            yield put(
              actions.getDepartmentRequest({ page: page - 1, limit })
            );
          } else if (
            totalPage === 1 &&
            totalPage === page &&
            data.length === 1
          ) {
            yield put(actions.getDepartmentRequest({ page, limit }));
          } else {
            yield put(actions.getDepartmentRequest({ page, limit }));
          }
        }
      }
    
  } catch (error) {}
}
function* getDetailDepartmentSaga({ payload }) {
  try {
   
      const res = yield getDetailDepartment(payload);
      if (res.code == 200) {
        
        yield put(actions.getDepartmentDetailsuccess(res.data));
      }
    
  } catch (error) {
    yield put(actions.getDepartmentDetailfailure(error));
  }
}

export const DepartmentSaga = [
  takeLatest(actions.addDepartmentRequest.type, addDepartmentSaga),
  takeLatest(actions.getDepartmentRequest.type, getDepartmentSaga),
  takeLatest(actions.updateDepartmentRequest.type, updateDepartmentSaga),
  takeLatest(actions.deleteDepartmentRequest.type, deleteDepartmentSaga),
  takeLatest(actions.getDepartmentDetailRequest.type, getDetailDepartmentSaga),
];
