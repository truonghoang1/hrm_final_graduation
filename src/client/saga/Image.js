import { put, takeLatest } from "redux-saga/effects";
import { uploadImg } from "../api/upload";
import * as actions from "../actions/image";

function* addImageSaga({ payload }) {
  try {
    const res = yield uploadImg(payload);
    if (res.code === 401) {
      yield window.location.reload();
    } else if (res.message !== "success") {
      yield put(actions.addImageSuccess(res.message));
    } else {
      yield put(actions.addImageSuccess(res.data));
    }
  } catch (error) {
    yield put(actions.addImageFailure(error));
  }
}

export const ImageSaga = [
  takeLatest(actions.addImageRequest.type, addImageSaga),
];
