import { put, select, takeLatest } from "redux-saga/effects";

import {
  addTimeSheetAdmin,
  getTimeSheetAdmin,
  getDetailTimeSheetAdmin,
  updateTimeSheetAdmin,
  deleteTimeSheetAdmin,
  getListTimeEmployee,addLogTimeEmployee,getLeaveDateEmployee,getListLeaveAppEmployee,addLeaveApplication
  // importTimeSheetAdmin,exportTimeSheetAdmin
} from "../api/timesheet";
import * as actions from "../actions/timesheet";
import Swal from "sweetalert2";
//employee

function* addTimeSheetEmployeeSaga({ payload }) {
  try {
    const { limit } = yield select((state) => state.timesheetState);
   
    const res = yield addLogTimeEmployee();
     if (res.code == 200) {
     
      yield put(actions.addTimeSheetEmployeesuccess(res.message));
      yield Swal.fire({
        titleText: res.message,
        icon: "success",
      });
      yield put(actions.getTimeSheetEmployeeRequest({
        // page:Math.ceil(res.data.total/limit),limit
        page:1,limit
      }))
      
    } 
  } catch (error) {
    
      yield put(actions.addTimeSheetEmployeefailure(error.TypeError))
    
  }
}
function* getTimeSheetEmployeeSaga({ payload }) {
  try {

    const res = yield getListTimeEmployee(payload);
    if (res.code == 200) {
      
      yield put(
        actions.getTimeSheetEmployeesuccess({
          data: res.data.data,
          totalPage: res.data.totalPage,
          page: res.data.page,
          
        })
      );
    
   }
  } catch (error) {
      yield put(actions.getTimeSheetEmployeefailure(error.TypeError))
  }
}
//leave app
function* addLeaveApplicationSaga({ payload }) {
  try {
    const { limit } = yield select((state) => state.timesheetState);
   
    const res = yield addLeaveApplication(payload);
     if (res.code == 200) {
     
      yield put(actions.addLeaveApplicationsuccess(res.message));
      yield Swal.fire({
        titleText: res.message,
        icon: "success",
      });
      yield put(actions.getListLeaveApplicationRequest({
        // page:Math.ceil(res.data.total/limit),limit
        page:1,limit
      }))
      
    } 
  } catch (error) {
    
      yield put(actions.addOvertimeApplicationfailure(error.TypeError))
    
  }
}
function* getLeaveAppSaga({ payload }) {
  
  try {

    const res = yield getListLeaveAppEmployee(payload);
    if (res.code == 200) {
      
      yield put(
        actions.getListLeaveApplicationsuccess({
          data: res.data.data,
          totalPage: res.data.totalPage,
          page: res.data.page,
          
        })
      );
    
   }
  } catch (error) {
      yield put(actions.getListLeaveApplicationfailure(error.TypeError))
  }
}

//admin


function* getTimeSheetAdminSaga({ payload }) {
  try {
   
    const res = yield getTimeSheetAdmin(payload);
    if (res.code == 200) {
      
      yield put(
        actions.getTimeSheetAdminsuccess({
          data: res.data,
      
        })
      );
    }
   
  } catch (error) {
      yield put(actions.getTimeSheetAdminfailure(error.TypeError))
  }
}
function* updateTimeSheetAdminSaga({ payload }) {
  try {
    const { limit, page,keySearch } = yield select((state) => state.timesheetState);

    const res = yield updateTimeSheetAdmin(payload);
    if (res.code == 200) {
     
      yield put(actions.updateTimeSheetAdminsuccess(res.message));
      yield Swal.fire({
        icon:"success",
        title:res.message
      })
    if(keySearch){
      yield put(actions.getTimeSheetAdminRequest({page,limit,keySearch}))
    }else{
      yield put(actions.getTimeSheetAdminRequest({page,limit}))
    }
    }
  } catch (error) {
    if(error.TypeError===undefined){
      yield put(actions.updateTimeSheetAdminfailure(error.TypeError))
    }
  }
}
function* deleteTimeSheetAdminSaga({ payload }) {
  try {
    const res = yield deleteTimeSheetAdmin(payload);
    const { totalPage, pageIndex, data, keySearch, limit } = yield select(
      (state) => state.timesheetState
    );
    if (res.code == 200) {
     yield Swal.fire({
      icon:"success",
      text:res.message
     })
      yield put(actions.deleteTimeSheetAdminsuccess(res.message));
      if (keySearch) {
        if (totalPage > 1 && totalPage === pageIndex && data.length === 1) {
          yield put(
            actions.getTimeSheetAdminRequest({
              keySearch: keySearch || "",
              page: pageIndex - 1,
              limit,
            })
          );
        } else if (
          totalPage === 1 &&
          totalPage === pageIndex &&
          data.length === 1
        ) {
          yield put(
            actions.getTimeSheetAdminRequest({ keySearch, page: pageIndex, limit })
          );
        } else {
          yield put(
            actions.getTimeSheetAdminRequest({ keySearch, page: pageIndex, limit })
          );
        }
      } else {
        if (totalPage > 1 && totalPage === pageIndex && data.length === 1) {
          yield put(actions.getTimeSheetAdminRequest({ page: pageIndex - 1, limit }));
        } else if (
          totalPage === 1 &&
          totalPage === pageIndex &&
          data.length === 1
        ) {
          yield put(actions.getTimeSheetAdminRequest({ page: pageIndex, limit }));
        } else {
          yield put(actions.getTimeSheetAdminRequest({ page: pageIndex, limit }));
        }
      }
    }
  } catch (error) {
    yield put(actions.deleteTimeSheetAdminfailure(error));
  }
}
function* getDetailTimeSheetAdminSaga({ payload }) {
  try {
    const res = yield getDetailTimeSheetAdmin(payload);
     if (res.code == 200) {
     
      yield put(actions.getTimeSheetAdminDetailsuccess(res.data));
    }
  } catch (error) {
    yield put(actions.getTimeSheetAdminDetailfailure(error));
  }
}
// function* importTimeSheetAdminSaga({payload}){
//   try {
//     const res= yield importTimeSheetAdmin(payload)
//     if(res.message!=="success"){
//       yield put(actions.importTimeSheetAdminsFailure(res.message))
//     }else{
//       yield put(actions.importTimeSheetAdminsSuccess(res.message))
//       yield put(actions.getTimeSheetAdminRequest({
//         page:res.data.totalPage,limit:10
//       }))
//     }
//   } catch (error) {
//     if(error.TypeError===undefined){
//       yield put(actions.importTimeSheetAdminsFailure(error.TypeError))
//     }
//   }
// }
// function* exportTimeSheetAdminSaga({payload}){
//   try {
//     yield exportTimeSheetAdmin(payload)
//     yield put(actions.exportTimeSheetAdminsSuccess())
//   } catch (error) {
//     yield put(actions.exportTimeSheetAdminsFailure(error))
//   }
  
// }
export const TimeSheetAdminSaga = [

  takeLatest(actions.getTimeSheetAdminRequest.type, getTimeSheetAdminSaga),
  takeLatest(actions.updateTimeSheetAdminRequest.type, updateTimeSheetAdminSaga),
  takeLatest(actions.deleteTimeSheetAdminRequest.type, deleteTimeSheetAdminSaga),
  takeLatest(actions.getTimeSheetAdminDetailRequest.type, getDetailTimeSheetAdminSaga),
  takeLatest(actions.getTimeSheetEmployeeRequest.type,getTimeSheetEmployeeSaga),
  takeLatest(actions.addTimeSheetEmployeeRequest.type,addTimeSheetEmployeeSaga),
  takeLatest(actions.getListLeaveApplicationRequest.type,getLeaveAppSaga),
  takeLatest(actions.addLeaveApplicationRequest.type,addLeaveApplicationSaga),
  // takeLatest(actions.importTimeSheetAdminsRequest.type,importTimeSheetAdminSaga),
  //  takeLatest(actions.exportTimeSheetAdminsRequest.type,exportTimeSheetAdminSaga)
];
