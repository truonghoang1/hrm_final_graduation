import { put, select, takeLatest } from "redux-saga/effects";
import {
  addRolesAPI,
  getRolesAPI,
  detailRolesAPI,
  updateRolesAPI,
  deleteRolesAPI,
} from "../api/roles";
import * as actions from "../actions/roles";
import Swal from "sweetalert2";
function* addRolesSaga({ payload }) {
  try {
    
      const { limit } = yield select((state) => state.roleState);
      const res = yield addRolesAPI(payload);
      if (res.code == 200) {
        yield put(actions.addRolesSuccess(res.message));
        yield Swal.fire({
          titleText: " Add Success",
          icon: "success",
        });
        yield put(actions.getRolesRequest({page:Math.ceil(res.data.total/limit),limit}))
       
      }
    
  } catch (error) {
   
      yield put(actions.addRolesFailure(" server error 500"));
    
  }
}
function* getRolesSaga({ payload }) {
  try {
    const { keySearch } = yield select((state) => state.roleState);
    if (keySearch) {
      const res = yield getRolesAPI({
        keySearch,
        page: payload.page,
        limit: payload.limit,
      });
      if (res.message == 200) {
        yield put(
          actions.getRolesSuccess({
            data: res.data.data,
            totalPage: res.data.totalPage,
            page: res.data.page,
            keySearch: res.data.keySearch,
          })
        );
      }
    } else {
      const res = yield getRolesAPI(payload);
      if (res.code == 200) {
        yield put(
          actions.getRolesSuccess({
            data: res.data.data,
            totalPage: res.data.totalPage,
            page: res.data.page,
          })
        );
      }
    }
  } catch (error) {
      yield put(actions.getRolesFailure(" server error 500"));
  }
}
function* updateRolesSaga({ payload }) {
  try {
      const { limit, page, keySearch } = yield select(
        (state) => state.roleState
      );
      if (keySearch) {
        const res = yield updateRolesAPI(payload);
        if (res.code == 200) {
          yield put(actions.updateRolesSuccess(res.message));
          yield Swal.fire({
            title:res.message,
            icon:"success"
          })
          yield put(actions.getRolesRequest({page,limit,keySearch}))
        }
      } else {
        const res = yield updateRolesAPI(payload);
        if (res.code == 200) {
          yield put(actions.updateRolesSuccess(res.message));
          yield Swal.fire({
            title:res.message,
            icon:"success"
          })
          yield put(actions.getRolesRequest({page,limit}))
        }
      }
    
  } catch (error) {}
}
function* deleteRolesSaga({ payload }) {
  try {
   
      const res = yield deleteRolesAPI(payload);
      const { limit, page, totalPage, data, keySearch } = yield select(
        (state) => state.roleState
      );
      if (res.code== 200) {
        
        yield put(actions.deleteRolesSuccess(res.message));
        if (keySearch) {
          if (totalPage > 1 && totalPage === pageIndex && data.length === 1) {
            yield put(
              actions.getRolesRequest({
                keySearch: keySearch || "",
                page: page - 1,
                limit,
              })
            );
          } else if (
            totalPage === 1 &&
            totalPage === pageIndex &&
            data.length === 1
          ) {
            yield put(
              actions.getRolesRequest({
                keySearch,
                page: page,
                limit,
              })
            );
          } else {
            yield put(
              actions.getRolesRequest({
                keySearch,
                page: page,
                limit,
              })
            );
          }
        } else {
          if (totalPage > 1 && totalPage === pageIndex && data.length === 1) {
            yield put(
              actions.getRolesRequest({ page: page - 1, limit })
            );
          } else if (
            totalPage === 1 &&
            totalPage === page &&
            data.length === 1
          ) {
            yield put(actions.getRolesRequest({ page, limit }));
          } else {
            yield put(actions.getRolesRequest({ page, limit }));
          }
        }
      }
    
  } catch (error) {}
}
function* getDetailRolesSaga({ payload }) {
  try {
   
      const res = yield detailRolesAPI(payload);
      if (res.code == 200) {
        
        yield put(actions.detailRolesSuccess(res.data));
      }
    
  } catch (error) {
    yield put(actions.detailRolesFailure(error));
  }
}

export const roleSaga = [
  takeLatest(actions.addRolesRequest.type, addRolesSaga),
  takeLatest(actions.getRolesRequest.type, getRolesSaga),
  takeLatest(actions.updateRolesRequest.type, updateRolesSaga),
  takeLatest(actions.deleteRolesRequest.type, deleteRolesSaga),
  takeLatest(actions.detailRolesRequest.type, getDetailRolesSaga),
];
