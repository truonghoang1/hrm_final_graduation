import { put, select, takeLatest } from "redux-saga/effects";

import {
  addEmployee,
  getEmployee,
  getDetailEmployee,
  updateEmployee,
  deleteEmployee,
   importEmployee,exportEmployee
} from "../api/employee";
import * as actions from "../actions/employee";
import Swal from "sweetalert2";


function* addEmployeeSaga({ payload }) {
  try {
    const { limit } = yield select((state) => state.employeeState);
    console.log(payload);
    const res = yield addEmployee(payload);
     if (res.code == 200) {
     
      yield put(actions.addEmployeesuccess(res.message));
      yield Swal.fire({
        titleText: " Add Success",
        icon: "success",
      });
      yield put(actions.getEmployeeRequest({
        page:Math.ceil(res.data.total/limit),limit
      }))
      
    } 
  } catch (error) {
    
      yield put(actions.addEmployeefailure(error.TypeError))
    
  }
}
function* getEmployeeSaga({ payload }) {
  try {
    const {keySearch}= yield select(state=>state.employeeState)
   if(keySearch){
    const res = yield getEmployee(payload);
    if (res.code == 200) {
     
      yield put(
        actions.getEmployeesuccess({
          data: res.data.data,
          totalPage: res.data.totalPage,
          page: res.data.page,
          keySearch:res.data.keySearch
        })
      );
    }
   }else{
    const res = yield getEmployee(payload);
    if (res.code == 200) {
      
      yield put(
        actions.getEmployeesuccess({
          data: res.data.data,
          totalPage: res.data.totalPage,
          page: res.data.page,
          
        })
      );
    }
   }
  } catch (error) {
      yield put(actions.getEmployeefailure(error.TypeError))
  }
}
function* updateEmployeeSaga({ payload }) {
  try {
    const { limit, page,keySearch } = yield select((state) => state.employeeState);

    const res = yield updateEmployee(payload);
    if (res.code == 200) {
     
      yield put(actions.updateEmployeesuccess(res.message));
      yield Swal.fire({
        icon:"success",
        title:res.message
      })
    if(keySearch){
      yield put(actions.getEmployeeRequest({page,limit,keySearch}))
    }else{
      yield put(actions.getEmployeeRequest({page,limit}))
    }
    }
  } catch (error) {
    if(error.TypeError===undefined){
      yield put(actions.updateEmployeefailure(error.TypeError))
    }
  }
}
function* deleteEmployeeSaga({ payload }) {
  try {
    const res = yield deleteEmployee(payload);
    const { totalPage, pageIndex, data, keySearch, limit } = yield select(
      (state) => state.employeeState
    );
    if (res.code == 200) {
     yield Swal.fire({
      icon:"success",
      text:res.message
     })
      yield put(actions.deleteEmployeesuccess(res.message));
      if (keySearch) {
        if (totalPage > 1 && totalPage === pageIndex && data.length === 1) {
          yield put(
            actions.getEmployeeRequest({
              keySearch: keySearch || "",
              page: pageIndex - 1,
              limit,
            })
          );
        } else if (
          totalPage === 1 &&
          totalPage === pageIndex &&
          data.length === 1
        ) {
          yield put(
            actions.getEmployeeRequest({ keySearch, page: pageIndex, limit })
          );
        } else {
          yield put(
            actions.getEmployeeRequest({ keySearch, page: pageIndex, limit })
          );
        }
      } else {
        if (totalPage > 1 && totalPage === pageIndex && data.length === 1) {
          yield put(actions.getEmployeeRequest({ page: pageIndex - 1, limit }));
        } else if (
          totalPage === 1 &&
          totalPage === pageIndex &&
          data.length === 1
        ) {
          yield put(actions.getEmployeeRequest({ page: pageIndex, limit }));
        } else {
          yield put(actions.getEmployeeRequest({ page: pageIndex, limit }));
        }
      }
    }
  } catch (error) {
    yield put(actions.deleteEmployeefailure(error));
  }
}
function* getDetailEmployeeSaga({ payload }) {
  try {
    const res = yield getDetailEmployee(payload);
     if (res.code == 200) {
     
      yield put(actions.getEmployeeDetailsuccess(res.data));
    }
  } catch (error) {
    yield put(actions.getEmployeeDetailfailure(error));
  }
}
function* importEmployeeSaga({payload}){
  try {
    const res= yield importEmployee(payload)
    if(res.code==200){
      yield Swal.fire({
        icon:'success',
        titleText:'Import success'
      })
      yield put(actions.importEmployeesuccess(res.message))
      yield put(actions.getEmployeeRequest({
        page:res.data.totalPage,limit:10
      }))
    }
  } catch (error) {
   
      yield put(actions.importEmployeefailure(error.TypeError))
  
  }
}
function* exportEmployeeSaga({payload}){
  try {
    yield exportEmployee(payload)
    yield put(actions.exportEmployeesuccess())
  } catch (error) {
    yield put(actions.exportEmployeefailure(error))
  }
  
}
export const EmployeeSaga = [
  takeLatest(actions.addEmployeeRequest.type, addEmployeeSaga),
  takeLatest(actions.getEmployeeRequest.type, getEmployeeSaga),
  takeLatest(actions.updateEmployeeRequest.type, updateEmployeeSaga),
  takeLatest(actions.deleteEmployeeRequest.type, deleteEmployeeSaga),
  takeLatest(actions.getEmployeeDetailRequest.type, getDetailEmployeeSaga),
  takeLatest(actions.importEmployeeRequest.type,importEmployeeSaga),
   takeLatest(actions.exportEmployeeRequest.type,exportEmployeeSaga)
];
