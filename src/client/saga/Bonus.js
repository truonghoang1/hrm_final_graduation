import { put, select, takeLatest } from "redux-saga/effects";
import {
  addBonus,
  getBonus,
  getDetailBonus,
  updateBonus,
  deleteBonus,
} from "../api/bonus";
import * as actions from "../actions/bonus";
import Swal from "sweetalert2";
function* addBonusSaga({ payload }) {
  try {
    
      const { limit } = yield select((state) => state.bonusState);
      const res = yield addBonus(payload);
      if (res.code == 200) {
        yield put(actions.addBonussuccess(res.message));
        yield Swal.fire({
          titleText: " Add Success",
          icon: "success",
        });
        yield put(actions.getBonusRequest({page:Math.ceil(res.data.total/limit),limit}))
       
      }
    
  } catch (error) {
   
      yield put(actions.addBonusfailure(" server error 500"));
    
  }
}
function* getBonusSaga({ payload }) {
  try {
    const { keySearch } = yield select((state) => state.bonusState);
    if (keySearch) {
      const res = yield getBonus({
        keySearch,
        page: payload.page,
        limit: payload.limit,
      });
      if (res.message == 200) {
        yield put(
          actions.getBonussuccess({
            data: res.data.data,
            totalPage: res.data.totalPage,
            page: res.data.page,
            keySearch: res.data.keySearch,
          })
        );
      }
    } else {
      const res = yield getBonus(payload);
      if (res.code == 200) {
        yield put(
          actions.getBonussuccess({
            data: res.data.data,
            totalPage: res.data.totalPage,
            page: res.data.page,
          })
        );
      }
    }
  } catch (error) {
      yield put(actions.getBonusDetailfailure(" server error 500"));
  }
}
function* updateBonusSaga({ payload }) {
  try {
      const { limit, page, keySearch } = yield select(
        (state) => state.bonusState
      );
      if (keySearch) {
        const res = yield updateBonus(payload);
        if (res.code == 200) {
          yield put(actions.updateBonussuccess(res.message));
          yield Swal.fire({
            title:res.message,
            icon:"success"
          })
          yield put(actions.getBonusRequest({page,limit,keySearch}))
        }
      } else {
        const res = yield updateBonus(payload);
        if (res.code == 200) {
          yield put(actions.updateBonussuccess(res.message));
          yield Swal.fire({
            title:res.message,
            icon:"success"
          })
          yield put(actions.getBonusRequest({page,limit}))
        }
      }
    
  } catch (error) {}
}
function* deleteBonusSaga({ payload }) {
  try {
   
      const res = yield deleteBonus(payload);
      const { limit, page, totalPage, data, keySearch } = yield select(
        (state) => state.bonusState
      );
      if (res.code== 200) {
        
        yield put(actions.deleteBonussuccess(res.message));
        if (keySearch) {
          if (totalPage > 1 && totalPage === pageIndex && data.length === 1) {
            yield put(
              actions.getBonusRequest({
                keySearch: keySearch || "",
                page: page - 1,
                limit,
              })
            );
          } else if (
            totalPage === 1 &&
            totalPage === pageIndex &&
            data.length === 1
          ) {
            yield put(
              actions.getBonusRequest({
                keySearch,
                page: page,
                limit,
              })
            );
          } else {
            yield put(
              actions.getBonusRequest({
                keySearch,
                page: page,
                limit,
              })
            );
          }
        } else {
          if (totalPage > 1 && totalPage === pageIndex && data.length === 1) {
            yield put(
              actions.getBonusRequest({ page: page - 1, limit })
            );
          } else if (
            totalPage === 1 &&
            totalPage === page &&
            data.length === 1
          ) {
            yield put(actions.getBonusRequest({ page, limit }));
          } else {
            yield put(actions.getBonusRequest({ page, limit }));
          }
        }
      }
    
  } catch (error) {}
}
function* getDetailBonusSaga({ payload }) {
  try {
   
      const res = yield getDetailBonus(payload);
      if (res.code == 200) {
        
        yield put(actions.getBonusDetailsuccess(res.data));
      }
    
  } catch (error) {
    yield put(actions.getBonusDetailfailure(error));
  }
}

export const BonusSaga = [
  takeLatest(actions.addBonusRequest.type, addBonusSaga),
  takeLatest(actions.getBonusRequest.type, getBonusSaga),
  takeLatest(actions.updateBonusRequest.type, updateBonusSaga),
  takeLatest(actions.deleteBonusRequest.type, deleteBonusSaga),
  takeLatest(actions.getBonusDetailRequest.type, getDetailBonusSaga),
];
