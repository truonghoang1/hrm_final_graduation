import {all} from 'redux-saga/effects'

import {userSaga} from './User'
import { roleSaga } from './Roles'
import { ImageSaga } from './Image'
import { EmployeeSaga } from './Employee'
import {DepartmentSaga} from "./Department"
import { BonusSaga } from './Bonus'
import {SalarySaga} from "./Salary"
import {AccountSaga} from "./Account"
import {TimeSheetAdminSaga} from "./TimeSheet"

export default function* rootSaga(){
    yield all([
        ...userSaga,
        ...roleSaga,
        ...ImageSaga,
        ...EmployeeSaga,
        ...DepartmentSaga,
        ...SalarySaga,
        ...AccountSaga,
        ...BonusSaga,
        ...TimeSheetAdminSaga
    ])
}