import { put, select, takeLatest } from "redux-saga/effects";
import {
  addAccount,
  getAccount,
  getDetailAccount,
  updateAccount,
  deleteAccount,
} from "../api/account";
import * as actions from "../actions/account";
import Swal from "sweetalert2";
function* addAccountSaga({ payload }) {
  try {
    
      const { limit } = yield select((state) => state.accountState);
      const res = yield addAccount(payload);
      if (res.code == 200) {
        yield put(actions.addAccountsuccess(res.message));
        yield Swal.fire({
          titleText: " Add Success",
          icon: "success",
        });
        yield put(actions.getAccountRequest({page:Math.ceil(res.data.total/limit),limit}))
       
      }
    
  } catch (error) {
   
      yield put(actions.addAccountfailure(" server error 500"));
    
  }
}
function* getAccountSaga({ payload }) {
  try {
    const { keySearch } = yield select((state) => state.accountState);
    if (keySearch) {
      const res = yield getAccount({
        keySearch,
        page: payload.page,
        limit: payload.limit,
      });
      if (res.message == 200) {
        yield put(
          actions.getAccountsuccess({
            data: res.data.data,
            totalPage: res.data.totalPage,
            page: res.data.page,
            keySearch: res.data.keySearch,
          })
        );
      }
    } else {
      const res = yield getAccount(payload);
      if (res.code == 200) {
        yield put(
          actions.getAccountsuccess({
            data: res.data.data,
            totalPage: res.data.totalPage,
            page: res.data.page,
          })
        );
      }
    }
  } catch (error) {
      yield put(actions.getAccountDetailfailure(" server error 500"));
  }
}
function* updateAccountSaga({ payload }) {
  try {
      const { limit, page, keySearch } = yield select(
        (state) => state.accountState
      );
      if (keySearch) {
        const res = yield updateAccount(payload);
        if (res.code == 200) {
          yield put(actions.updateAccountsuccess(res.message));
          yield Swal.fire({
            title:res.message,
            icon:"success"
          })
          yield put(actions.getAccountRequest({page,limit,keySearch}))
        }
      } else {
        const res = yield updateAccount(payload);
        if (res.code == 200) {
          yield put(actions.updateAccountsuccess(res.message));
          yield Swal.fire({
            title:res.message,
            icon:"success"
          })
          yield put(actions.getAccountRequest({page,limit}))
        }
      }
    
  } catch (error) {}
}
function* deleteAccountSaga({ payload }) {
  try {
   
      const res = yield deleteAccount(payload);
      const { limit, page, totalPage, data, keySearch } = yield select(
        (state) => state.accountState
      );
      if (res.code== 200) {
        
        yield put(actions.deleteAccountsuccess(res.message));
        if (keySearch) {
          if (totalPage > 1 && totalPage === pageIndex && data.length === 1) {
            yield put(
              actions.getAccountRequest({
                keySearch: keySearch || "",
                page: page - 1,
                limit,
              })
            );
          } else if (
            totalPage === 1 &&
            totalPage === pageIndex &&
            data.length === 1
          ) {
            yield put(
              actions.getAccountRequest({
                keySearch,
                page: page,
                limit,
              })
            );
          } else {
            yield put(
              actions.getAccountRequest({
                keySearch,
                page: page,
                limit,
              })
            );
          }
        } else {
          if (totalPage > 1 && totalPage === pageIndex && data.length === 1) {
            yield put(
              actions.getAccountRequest({ page: page - 1, limit })
            );
          } else if (
            totalPage === 1 &&
            totalPage === page &&
            data.length === 1
          ) {
            yield put(actions.getAccountRequest({ page, limit }));
          } else {
            yield put(actions.getAccountRequest({ page, limit }));
          }
        }
      }
    
  } catch (error) {}
}
function* getDetailAccountSaga({ payload }) {
  try {
   
      const res = yield getDetailAccount(payload);
      if (res.code == 200) {
        
        yield put(actions.getAccountDetailsuccess(res.data));
      }
    
  } catch (error) {
    yield put(actions.getAccountDetailfailure(error));
  }
}

export const AccountSaga = [
  takeLatest(actions.addAccountRequest.type, addAccountSaga),
  takeLatest(actions.getAccountRequest.type, getAccountSaga),
  takeLatest(actions.updateAccountRequest.type, updateAccountSaga),
  takeLatest(actions.deleteAccountRequest.type, deleteAccountSaga),
  takeLatest(actions.getAccountDetailRequest.type, getDetailAccountSaga),
];
