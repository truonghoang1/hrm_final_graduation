import {put,takeLatest,call} from 'redux-saga/effects'
import *as actions from '../actions/user'
import {signin,signup,forgotPassword,signupGuest,resetPassword,activationAccount,statistialAPI,getCountLikeFacebook,changePwAPI,updateProfileApi} from '../api/user'
import cookie from 'js-cookie'
import SweetAlert from 'sweetalert2'
import { success } from '../../server/common/res'
const token = cookie.get("SESSION_ID")
 function* signinSaga({payload}){
    try {
        const res = yield signin(payload)
      
        if(res.code !==200){
            console.log(res);
            yield SweetAlert.fire({
                title:"Error Password",
                text:res.message,
                icon:"error"
            })
            yield put(actions.signInFailure(res.message))
           
        }else{
           
            yield cookie.set("SESSION_ID",res.data.token,{ expires: (1/1440)*60 })
            yield cookie.set("role",res.data.role,{ expires: (1/1440)*60 })
            yield put(actions.signInSuccess({
                token:res.data.token
            }))
        }
        
    } catch (error) {
        yield put(actions.signInFailure(error))
       
    }
 }
 function* signupSaga({payload}){
    try {
        const res = yield signup(payload)
        if(res.code!==200){
            yield put(actions.signupFailure(res.message))
            yield SweetAlert.fire({
                title:res.message,
                icon:'error'
            })
        }else{  
       if(token){
        yield put(actions.signupSuccess(res.data))
        yield SweetAlert.fire({title:res.message,icon:"success"})
       }else{
        yield put(actions.signupSuccess(res.data))
        yield SweetAlert.fire({title:res.message,icon:"success"})
         yield window.location.href ='/login'
       }
        }
    } catch (error) {
        yield put(actions.signupFailure(error))
    }
 }

 function* forgotPasswordSaga({payload}){
    try {
        
        const res = yield forgotPassword(payload)
        // console.log("hello",res)
        if(res.code!==200){
            yield put(actions.forgotPasswordFailure(res.message))
        }else{
            yield put(actions.forgotPasswordSuccess(res.data))
            yield SweetAlert.fire({
                titleText:res.message,
                icon:"success"
            })
        }
    } catch (error) {
        
    }
 }
 function* resetPasswordSaga({payload}){
    try {
        const res =yield resetPassword(payload)
        if(res.code!==200){
            yield put(actions.resetPasswordFailure(res.message))
        }else{
            yield put(actions.resetPasswordSuccess(res.message))
            yield SweetAlert.fire({
                titleText:res.message,
                icon:'success'
            })
        }
    } catch (error) {
        
    }
 }
 function* activationAccSaga({payload}){
    try {
        const res =yield activationAccount(payload)
        console.log("res",res);
        if(res.code!==200){
            yield put(actions.activationAccountFailure(res.message))
        }else{
            yield put(actions.activationAccountSuccess(res.message))
            yield SweetAlert.fire({
                title:"Active sucess",
                text:"Login now",
                icon:'success'
            })
        }
    } catch (error) {
    if(error.TypeError===undefined){
        yield put(actions.activationAccountFailure("Token expired"))
    }   
    }
 }
 function* signupGuestSaga({payload}){
    try {
        const res = yield signupGuest(payload)
        if(res.code!==200){
            yield put(actions.signupGuestFailure(res.message))
        }else{
            yield  put(actions.signupGuestSuccess(res.message))
            yield SweetAlert.fire({
                title:"await active",
                text:"check mail to active email",
                icon:'warning'
            })
        }
    } catch (error) {
        
    }
 }
function* statisticSaga({payload}){
    try {
        const res = yield statistialAPI(payload)
        if(res.code!==200){
            yield put(actions.statisticalFailure(res.message))
        }else{
            yield put(actions.statisticalSuccess(res.data))
        }
    } catch (error) {
        
    }
}
function* getCountLikeFaceBookSaga({payload}){
    try {
        const res = yield getCountLikeFacebook(payload)
        yield put(actions.getLikeCountFacebookSuccess(res.fan_count))
    } catch (error) {
        yield put(actions.getLikeCountFacebookFailure(error))
        
    }
}
function* changePwSaga({payload}){
    try {
        const res = yield changePwAPI(payload)
        if(res.code==200){
            yield SweetAlert.fire({
                title:res.message,
                icon:success
            })
            yield put(actions.changePasswordSuccess(res.data))
        }
    } catch (error) {
        yield put(actions.changePasswordFailure(error))
        
    }
}
function* updateProfileSaga({payload}){
    try {
        const res = yield updateProfileApi(payload)
        if(res.code==200){
            yield SweetAlert.fire({title:res.message,icon:success})
            yield put(actions.updateProfileSuccess())
        }
    } catch (error) {
        yield put(actions.updateProfileFailure(error))
    }
}
 export const userSaga =[
    takeLatest(actions.signInRequest.type,signinSaga),
    takeLatest(actions.signupRequest.type,signupSaga),
    takeLatest(actions.forgotPasswordRequest.type,forgotPasswordSaga),
    takeLatest(actions.resetPasswordRequest.type,resetPasswordSaga),
    takeLatest(actions.activationAccountRequest.type,activationAccSaga),
    takeLatest(actions.signupGuestRequest.type,signupGuestSaga),
    takeLatest(actions.statisticalRequest.type,statisticSaga),
    takeLatest(actions.getLikeCountFacebookRequest.type,getCountLikeFaceBookSaga),
    takeLatest(actions.changePasswordRequest.type,changePwSaga),
    takeLatest(actions.updateProfileRequest.type,updateProfileSaga)

 ]