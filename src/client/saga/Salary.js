import { put, select, takeLatest } from "redux-saga/effects";

import {
  addSalary,
  getSalary,
  getDetailSalary,
  updateSalary,
  deleteSalary,
  // importSalary,exportSalary
} from "../api/salary";
import * as actions from "../actions/salary";
import Swal from "sweetalert2";


function* addSalarySaga({ payload }) {
  // try {
    const { limit } = yield select((state) => state.salaryState);
    console.log(payload);
    const res = yield addSalary(payload);
     if (res.code == 200) {
     
      yield put(actions.addSalarysuccess(res.message));
      yield Swal.fire({
        titleText: " Add Success",
        icon: "success",
      });
      yield put(actions.getSalaryRequest({
        page:Math.ceil(res.data.total/limit),limit
      }))
      
    } 
  // } catch (error) {
    
      // yield put(actions.addSalaryfailure(error.TypeError))
    
  // }
}
function* getSalarySaga({ payload }) {
  try {
    const {keySearch}= yield select(state=>state.salaryState)
   if(keySearch){
    const res = yield getSalary(payload);
    if (res.code == 200) {
     
      yield put(
        actions.getSalarysuccess({
          data: res.data.data,
          totalPage: res.data.totalPage,
          page: res.data.page,
          keySearch:res.data.keySearch
        })
      );
    }
   }else{
    const res = yield getSalary(payload);
    if (res.code == 200) {
      
      yield put(
        actions.getSalarysuccess({
          data: res.data.data,
          totalPage: res.data.totalPage,
          page: res.data.page,
          
        })
      );
    }
   }
  } catch (error) {
      yield put(actions.getSalaryfailure(error.TypeError))
  }
}
function* updateSalarySaga({ payload }) {
  try {
    const { limit, page,keySearch } = yield select((state) => state.salaryState);

    const res = yield updateSalary(payload);
    if (res.code !== "success") {
      yield put(actions.updateSalaryfailure(res.message));
    } else {
      yield put(actions.updateSalarysuccess(res.message));
    if(keySearch){
      yield put(actions.getSalaryRequest({page,limit,keySearch}))
    }else{
      yield put(actions.getSalaryRequest({page,limit}))
    }
    }
  } catch (error) {
    if(error.TypeError===undefined){
      yield put(actions.updateSalaryfailure(error.TypeError))
    }
  }
}
function* deleteSalarySaga({ payload }) {
  try {
    const res = yield deleteSalary(payload);
    const { totalPage, pageIndex, data, keySearch, limit } = yield select(
      (state) => state.salaryState
    );
    if (res.code == 200) {
     
      yield put(actions.deleteSalarysuccess(res.message));
      if (keySearch) {
        if (totalPage > 1 && totalPage === pageIndex && data.length === 1) {
          yield put(
            actions.getSalaryRequest({
              keySearch: keySearch || "",
              page: pageIndex - 1,
              limit,
            })
          );
        } else if (
          totalPage === 1 &&
          totalPage === pageIndex &&
          data.length === 1
        ) {
          yield put(
            actions.getSalaryRequest({ keySearch, page: pageIndex, limit })
          );
        } else {
          yield put(
            actions.getSalaryRequest({ keySearch, page: pageIndex, limit })
          );
        }
      } else {
        if (totalPage > 1 && totalPage === pageIndex && data.length === 1) {
          yield put(actions.getSalaryRequest({ page: pageIndex - 1, limit }));
        } else if (
          totalPage === 1 &&
          totalPage === pageIndex &&
          data.length === 1
        ) {
          yield put(actions.getSalaryRequest({ page: pageIndex, limit }));
        } else {
          yield put(actions.getSalaryRequest({ page: pageIndex, limit }));
        }
      }
    }
  } catch (error) {
    yield put(actions.deleteSalaryfailure(error));
  }
}
function* getDetailSalarySaga({ payload }) {
  try {
    const res = yield getDetailSalary(payload);
    if (res.code === 401) {
      yield window.location.reload();
    } else if (res.message !== "success") {
      yield put(actions.getSalaryDetailfailure(res.message));
    } else {
      yield put(actions.getSalaryDetailsuccess(res.data));
    }
  } catch (error) {
    yield put(actions.getSalaryDetailfailure(error));
  }
}
// function* importSalarySaga({payload}){
//   try {
//     const res= yield importSalary(payload)
//     if(res.message!=="success"){
//       yield put(actions.importSalarysFailure(res.message))
//     }else{
//       yield put(actions.importSalarysSuccess(res.message))
//       yield put(actions.getSalaryRequest({
//         page:res.data.totalPage,limit:10
//       }))
//     }
//   } catch (error) {
//     if(error.TypeError===undefined){
//       yield put(actions.importSalarysFailure(error.TypeError))
//     }
//   }
// }
// function* exportSalarySaga({payload}){
//   try {
//     yield exportSalary(payload)
//     yield put(actions.exportSalarysSuccess())
//   } catch (error) {
//     yield put(actions.exportSalarysFailure(error))
//   }
  
// }
export const SalarySaga = [
  takeLatest(actions.addSalaryRequest.type, addSalarySaga),
  takeLatest(actions.getSalaryRequest.type, getSalarySaga),
  takeLatest(actions.updateSalaryRequest.type, updateSalarySaga),
  takeLatest(actions.deleteSalaryRequest.type, deleteSalarySaga),
  takeLatest(actions.getSalaryDetailRequest.type, getDetailSalarySaga),
  // takeLatest(actions.importSalarysRequest.type,importSalarySaga),
  //  takeLatest(actions.exportSalarysRequest.type,exportSalarySaga)
];
