import {createAction} from "@reduxjs/toolkit"
 const HEADER_TYPE = "Employee-action"

  const addEmployeeRequest = createAction(`add/${HEADER_TYPE}/request`)
  const addEmployeesuccess = createAction(`add/${HEADER_TYPE}/success`)
  const addEmployeefailure = createAction(`add/${HEADER_TYPE}/failure`)

  const getEmployeeRequest = createAction(`get/${HEADER_TYPE}/request`)
  const getEmployeesuccess = createAction(`get/${HEADER_TYPE}/success`)
  const getEmployeefailure = createAction(`get/${HEADER_TYPE}/failure`)

  const getEmployeeDetailRequest = createAction(`detail/${HEADER_TYPE}/request`)
  const getEmployeeDetailsuccess = createAction(`detail/${HEADER_TYPE}/success`)
  const getEmployeeDetailfailure = createAction(`detail/${HEADER_TYPE}/failure`)

  const updateEmployeeRequest = createAction(`update/${HEADER_TYPE}/request`)
  const updateEmployeesuccess = createAction(`update/${HEADER_TYPE}/success`)
  const updateEmployeefailure = createAction(`update/${HEADER_TYPE}/failure`)

  const deleteEmployeeRequest = createAction(`delete/${HEADER_TYPE}/request`)
  const deleteEmployeesuccess = createAction(`delete/${HEADER_TYPE}/success`)
const deleteEmployeefailure = createAction(`delete/${HEADER_TYPE}/failure`)
  
const importEmployeeRequest = createAction(`import/${HEADER_TYPE}/request`)
const importEmployeesuccess = createAction(`import/${HEADER_TYPE}/success`)
const importEmployeefailure = createAction(`import/${HEADER_TYPE}/failure`)
  
  const exportEmployeeRequest = createAction(`export/${HEADER_TYPE}/request`)
  const exportEmployeesuccess = createAction(`export/${HEADER_TYPE}/success`)
  const exportEmployeefailure = createAction(`export/${HEADER_TYPE}/failure`)

  const actionDefault = createAction("default-state")

  export{
    addEmployeeRequest,addEmployeefailure,addEmployeesuccess,
    getEmployeeRequest,getEmployeefailure,getEmployeesuccess,
    updateEmployeeRequest,updateEmployeefailure,updateEmployeesuccess,
    deleteEmployeeRequest,deleteEmployeefailure,deleteEmployeesuccess,
    getEmployeeDetailRequest, getEmployeeDetailfailure, getEmployeeDetailsuccess,
    importEmployeeRequest, importEmployeefailure, importEmployeesuccess,
    exportEmployeeRequest,exportEmployeefailure,exportEmployeesuccess,actionDefault
  }