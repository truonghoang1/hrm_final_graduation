import { createAction } from "@reduxjs/toolkit";

const HEADER_ACTION_TYPE = 'account';
const signInRequest = createAction(`${HEADER_ACTION_TYPE}/signin/request`)
const signInSuccess = createAction(`${HEADER_ACTION_TYPE}/signin/success`)
const signInFailure = createAction(`${HEADER_ACTION_TYPE}/signin/failure`)

const signupRequest = createAction(`${HEADER_ACTION_TYPE}/signup/request`)
const signupSuccess = createAction(`${HEADER_ACTION_TYPE}/signup/success`)
const signupFailure = createAction(`${HEADER_ACTION_TYPE}/signup/failure`)

const signupGuestRequest = createAction(`${HEADER_ACTION_TYPE}/guest/request`)
const signupGuestSuccess = createAction(`${HEADER_ACTION_TYPE}/guest/success`)
const signupGuestFailure = createAction(`${HEADER_ACTION_TYPE}/guest/failure`)

const forgotPasswordRequest = createAction(`forgot-password/request`)
const forgotPasswordSuccess = createAction(`forgot-password/success`)
const forgotPasswordFailure = createAction(`forgot-password/failure`)

const resetPasswordRequest = createAction(`reset-password/request`)
const resetPasswordSuccess = createAction(`reset-password/success`)
const resetPasswordFailure = createAction(`reset-password/failure`)

const activationAccountRequest = createAction(`activation-account/request`)
const activationAccountSuccess = createAction(`activation-account/success`)
const activationAccountFailure = createAction(`activation-account/failure`)

const statisticalRequest = createAction(`statistical-request`)
const statisticalSuccess = createAction(`statistical-Success`)
const statisticalFailure = createAction(`statistical-Failure`)

const changePasswordRequest = createAction(`change-password/request`)
const changePasswordSuccess = createAction(`change-password/success`)
const changePasswordFailure = createAction(`change-password/failure`)

const updateProfileRequest = createAction(`update-profile/request`)
const updateProfileSuccess = createAction(`update-profile/success`)
const updateProfileFailure = createAction(`update-profile/failure`)

const getLikeCountFacebookRequest = createAction(`facebook-get-count-like-request`)
const getLikeCountFacebookSuccess = createAction(`facebook-get-count-like-success`)
const getLikeCountFacebookFailure = createAction(`facebook-get-count-like-failure`)
const actionDefault = createAction("default-state")
export{
    signInFailure,signInRequest,signInSuccess,
    signupFailure,signupRequest,signupSuccess,
   forgotPasswordFailure,forgotPasswordRequest,forgotPasswordSuccess,
    signupGuestFailure,signupGuestRequest,signupGuestSuccess,
    resetPasswordFailure,resetPasswordRequest,resetPasswordSuccess,
    activationAccountFailure,activationAccountRequest,activationAccountSuccess,
    statisticalFailure,statisticalRequest,statisticalSuccess,
    getLikeCountFacebookFailure,getLikeCountFacebookRequest,getLikeCountFacebookSuccess,
    changePasswordFailure,changePasswordRequest,changePasswordSuccess,
    updateProfileFailure,updateProfileRequest,updateProfileSuccess
}