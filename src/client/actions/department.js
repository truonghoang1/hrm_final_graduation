import {createAction} from "@reduxjs/toolkit"
 const HEADER_TYPE = "department-action"

  const addDepartmentRequest = createAction(`add/${HEADER_TYPE}/request`)
  const addDepartmentsuccess = createAction(`add/${HEADER_TYPE}/success`)
  const addDepartmentfailure = createAction(`add/${HEADER_TYPE}/failure`)

  const getDepartmentRequest = createAction(`get/${HEADER_TYPE}/request`)
  const getDepartmentsuccess = createAction(`get/${HEADER_TYPE}/success`)
  const getDepartmentfailure = createAction(`get/${HEADER_TYPE}/failure`)

  const getDepartmentDetailRequest = createAction(`get/${HEADER_TYPE}/detail/request`)
  const getDepartmentDetailsuccess = createAction(`get/${HEADER_TYPE}/detail/success`)
  const getDepartmentDetailfailure = createAction(`get/${HEADER_TYPE}/detail/failure`)

  const updateDepartmentRequest = createAction(`update/${HEADER_TYPE}/request`)
  const updateDepartmentsuccess = createAction(`update/${HEADER_TYPE}/success`)
  const updateDepartmentfailure = createAction(`update/${HEADER_TYPE}/failure`)

  const deleteDepartmentRequest = createAction(`delete/${HEADER_TYPE}/request`)
  const deleteDepartmentsuccess = createAction(`delete/${HEADER_TYPE}/success`)
  const deleteDepartmentfailure = createAction(`delete/${HEADER_TYPE}/failure`)

  const actionDefault = createAction("default-state")
  export{
    addDepartmentRequest,addDepartmentfailure,addDepartmentsuccess,
    getDepartmentRequest,getDepartmentfailure,getDepartmentsuccess,
    updateDepartmentRequest,updateDepartmentfailure,updateDepartmentsuccess,
    deleteDepartmentRequest,deleteDepartmentfailure,deleteDepartmentsuccess,
    getDepartmentDetailRequest,getDepartmentDetailfailure,getDepartmentDetailsuccess,actionDefault
  }