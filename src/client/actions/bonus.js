import {createAction} from "@reduxjs/toolkit"
 const HEADER_TYPE = "bonus-action"

  const addBonusRequest = createAction(`add/${HEADER_TYPE}/request`)
  const addBonussuccess = createAction(`add/${HEADER_TYPE}/success`)
  const addBonusfailure = createAction(`add/${HEADER_TYPE}/failure`)

  const getBonusRequest = createAction(`get/${HEADER_TYPE}/request`)
  const getBonussuccess = createAction(`get/${HEADER_TYPE}/success`)
  const getBonusfailure = createAction(`get/${HEADER_TYPE}/failure`)

  const getBonusDetailRequest = createAction(`get/${HEADER_TYPE}/detail/request`)
  const getBonusDetailsuccess = createAction(`get/${HEADER_TYPE}/detail/success`)
  const getBonusDetailfailure = createAction(`get/${HEADER_TYPE}/detail/failure`)

  const updateBonusRequest = createAction(`update/${HEADER_TYPE}/request`)
  const updateBonussuccess = createAction(`update/${HEADER_TYPE}/success`)
  const updateBonusfailure = createAction(`update/${HEADER_TYPE}/failure`)

  const deleteBonusRequest = createAction(`delete/${HEADER_TYPE}/request`)
  const deleteBonussuccess = createAction(`delete/${HEADER_TYPE}/success`)
  const deleteBonusfailure = createAction(`delete/${HEADER_TYPE}/failure`)

  const actionDefault = createAction("default-state")
  export{
    addBonusRequest,addBonusfailure,addBonussuccess,
    getBonusRequest,getBonusfailure,getBonussuccess,
    updateBonusRequest,updateBonusfailure,updateBonussuccess,
    deleteBonusRequest,deleteBonusfailure,deleteBonussuccess,
    getBonusDetailRequest,getBonusDetailfailure,getBonusDetailsuccess,actionDefault
  }