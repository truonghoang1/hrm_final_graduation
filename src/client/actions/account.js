import {createAction} from "@reduxjs/toolkit"
 const HEADER_TYPE = "account-action"

  const addAccountRequest = createAction(`add/${HEADER_TYPE}/request`)
  const addAccountsuccess = createAction(`add/${HEADER_TYPE}/success`)
  const addAccountfailure = createAction(`add/${HEADER_TYPE}/failure`)

  const getAccountRequest = createAction(`get/${HEADER_TYPE}/request`)
  const getAccountsuccess = createAction(`get/${HEADER_TYPE}/success`)
  const getAccountfailure = createAction(`get/${HEADER_TYPE}/failure`)

  const getAccountDetailRequest = createAction(`get/${HEADER_TYPE}/detail/request`)
  const getAccountDetailsuccess = createAction(`get/${HEADER_TYPE}/detail/success`)
  const getAccountDetailfailure = createAction(`get/${HEADER_TYPE}/detail/failure`)

  const updateAccountRequest = createAction(`update/${HEADER_TYPE}/request`)
  const updateAccountsuccess = createAction(`update/${HEADER_TYPE}/success`)
  const updateAccountfailure = createAction(`update/${HEADER_TYPE}/failure`)

  const deleteAccountRequest = createAction(`delete/${HEADER_TYPE}/request`)
  const deleteAccountsuccess = createAction(`delete/${HEADER_TYPE}/success`)
  const deleteAccountfailure = createAction(`delete/${HEADER_TYPE}/failure`)

  const actionDefault = createAction("default-state")
  export{
    addAccountRequest,addAccountfailure,addAccountsuccess,
    getAccountRequest,getAccountfailure,getAccountsuccess,
    updateAccountRequest,updateAccountfailure,updateAccountsuccess,
    deleteAccountRequest,deleteAccountfailure,deleteAccountsuccess,
    getAccountDetailRequest,getAccountDetailfailure,getAccountDetailsuccess,actionDefault
  }