import {createAction} from "@reduxjs/toolkit"
 const HEADER_TYPE = "TimeSheetAdmin-action"
  const HEADER_TYPE_EMPLOYEE ="timesheet-employee-action"
  const addTimeSheetAdminRequest = createAction(`add/${HEADER_TYPE}/request`)
  const addTimeSheetAdminsuccess = createAction(`add/${HEADER_TYPE}/success`)
  const addTimeSheetAdminfailure = createAction(`add/${HEADER_TYPE}/failure`)

  const getTimeSheetAdminRequest = createAction(`get/${HEADER_TYPE}/request`)
  const getTimeSheetAdminsuccess = createAction(`get/${HEADER_TYPE}/success`)
  const getTimeSheetAdminfailure = createAction(`get/${HEADER_TYPE}/failure`)

  const getTimeSheetAdminDetailRequest = createAction(`detail/${HEADER_TYPE}/request`)
  const getTimeSheetAdminDetailsuccess = createAction(`detail/${HEADER_TYPE}/success`)
  const getTimeSheetAdminDetailfailure = createAction(`detail/${HEADER_TYPE}/failure`)

  const updateTimeSheetAdminRequest = createAction(`update/${HEADER_TYPE}/request`)
  const updateTimeSheetAdminsuccess = createAction(`update/${HEADER_TYPE}/success`)
  const updateTimeSheetAdminfailure = createAction(`update/${HEADER_TYPE}/failure`)

  const deleteTimeSheetAdminRequest = createAction(`delete/${HEADER_TYPE}/request`)
  const deleteTimeSheetAdminsuccess = createAction(`delete/${HEADER_TYPE}/success`)
const deleteTimeSheetAdminfailure = createAction(`delete/${HEADER_TYPE}/failure`)
  
const importTimeSheetAdminRequest = createAction(`import/${HEADER_TYPE}/request`)
const importTimeSheetAdminsuccess = createAction(`import/${HEADER_TYPE}/success`)
const importTimeSheetAdminfailure = createAction(`import/${HEADER_TYPE}/failure`)
  
  const exportTimeSheetAdminRequest = createAction(`export/${HEADER_TYPE}/request`)
  const exportTimeSheetAdminsuccess = createAction(`export/${HEADER_TYPE}/success`)
  const exportTimeSheetAdminfailure = createAction(`export/${HEADER_TYPE}/failure`)

  const actionDefault = createAction("default-state")

//employee
const addTimeSheetEmployeeRequest = createAction(`add/${HEADER_TYPE_EMPLOYEE}/request`)
const addTimeSheetEmployeesuccess = createAction(`add/${HEADER_TYPE_EMPLOYEE}/success`)
const addTimeSheetEmployeefailure = createAction(`add/${HEADER_TYPE_EMPLOYEE}/failure`)

const getTimeSheetEmployeeRequest = createAction(`get/${HEADER_TYPE_EMPLOYEE}/request`)
const getTimeSheetEmployeesuccess = createAction(`get/${HEADER_TYPE_EMPLOYEE}/success`)
const getTimeSheetEmployeefailure = createAction(`get/${HEADER_TYPE_EMPLOYEE}/failure`)

const getListLeaveApplicationRequest = createAction(`get-list-leave-application-request`)
const getListLeaveApplicationsuccess = createAction(`get-list-leave-application-success`)
const getListLeaveApplicationfailure = createAction(`get-list-leave-application-failure`)

const addLeaveApplicationRequest = createAction(`add-leave-application-request`)
const addLeaveApplicationsuccess = createAction(`add-leave-application-success`)
const addLeaveApplicationfailure = createAction(`add-leave-application-failure`)
  
const getListOvertimeApplicationRequest = createAction(`get-list-overtime-application-request`)
const getListOvertimeApplicationsuccess = createAction(`get-list-overtime-application-success`)
const getListOvertimeApplicationfailure = createAction(`get-list-overtime-application-failure`)

const addOvertimeApplicationRequest = createAction(`add-overtime-application-request`)
const addOvertimeApplicationsuccess = createAction(`add-overtime-application-success`)
const addOvertimeApplicationfailure = createAction(`add-overtime-application-failure`)

const getLeaveDateRequest = createAction(`get-leave-date-request`)
const getLeaveDatesuccess = createAction(`get-leave-date-success`)
const getLeaveDatefailure = createAction(`get-leave-date-failure`)
export{
    addTimeSheetAdminRequest,addTimeSheetAdminfailure,addTimeSheetAdminsuccess,
    getTimeSheetAdminRequest,getTimeSheetAdminfailure,getTimeSheetAdminsuccess,
    updateTimeSheetAdminRequest,updateTimeSheetAdminfailure,updateTimeSheetAdminsuccess,
    deleteTimeSheetAdminRequest,deleteTimeSheetAdminfailure,deleteTimeSheetAdminsuccess,
    getTimeSheetAdminDetailRequest, getTimeSheetAdminDetailfailure, getTimeSheetAdminDetailsuccess,
    importTimeSheetAdminRequest, importTimeSheetAdminfailure, importTimeSheetAdminsuccess,
    exportTimeSheetAdminRequest,exportTimeSheetAdminfailure,exportTimeSheetAdminsuccess,actionDefault,
    getTimeSheetEmployeeRequest,getTimeSheetEmployeefailure,getTimeSheetEmployeesuccess,
    addTimeSheetEmployeeRequest,addTimeSheetEmployeefailure,addTimeSheetEmployeesuccess,
    getListLeaveApplicationRequest,getListLeaveApplicationfailure,getListLeaveApplicationsuccess,
    getListOvertimeApplicationRequest,getListOvertimeApplicationfailure,getListOvertimeApplicationsuccess,
    addLeaveApplicationRequest,addLeaveApplicationfailure,addLeaveApplicationsuccess,
    addOvertimeApplicationRequest,addOvertimeApplicationfailure,addOvertimeApplicationsuccess,getLeaveDateRequest,getLeaveDatefailure,getLeaveDatesuccess,
  }