import {createAction} from "@reduxjs/toolkit"
 const HEADER_TYPE = "salary-action"

  const addSalaryRequest = createAction(`add/${HEADER_TYPE}/request`)
  const addSalarysuccess = createAction(`add/${HEADER_TYPE}/success`)
  const addSalaryfailure = createAction(`add/${HEADER_TYPE}/failure`)

  const getSalaryRequest = createAction(`get/${HEADER_TYPE}/request`)
  const getSalarysuccess = createAction(`get/${HEADER_TYPE}/success`)
  const getSalaryfailure = createAction(`get/${HEADER_TYPE}/failure`)

  const getSalaryDetailRequest = createAction(`get/${HEADER_TYPE}/detail/request`)
  const getSalaryDetailsuccess = createAction(`get/${HEADER_TYPE}/detail/success`)
  const getSalaryDetailfailure = createAction(`get/${HEADER_TYPE}/detail/failure`)

  const updateSalaryRequest = createAction(`update/${HEADER_TYPE}/request`)
  const updateSalarysuccess = createAction(`update/${HEADER_TYPE}/success`)
  const updateSalaryfailure = createAction(`update/${HEADER_TYPE}/failure`)

  const deleteSalaryRequest = createAction(`delete/${HEADER_TYPE}/request`)
  const deleteSalarysuccess = createAction(`delete/${HEADER_TYPE}/success`)
  const deleteSalaryfailure = createAction(`delete/${HEADER_TYPE}/failure`)

  const importSalarysRequest = createAction(`import-Salary-request`)
  const importSalarysSuccess = createAction(`import-Salary-Success`)
  const importSalarysFailure = createAction(`import-Salary-Failure`)

  const exportSalarysRequest = createAction(`export-Salary-request`)
  const exportSalarysSuccess = createAction(`export-Salary-Success`)
  const exportSalarysFailure = createAction(`export-Salary-Failure`)
  const actionDefault = createAction("default-state")
  export{
    addSalaryRequest,addSalaryfailure,addSalarysuccess,
    getSalaryRequest,getSalaryfailure,getSalarysuccess,
    updateSalaryRequest,updateSalaryfailure,updateSalarysuccess,
    deleteSalaryRequest,deleteSalaryfailure,deleteSalarysuccess,
    getSalaryDetailRequest,getSalaryDetailfailure,getSalaryDetailsuccess,
    importSalarysFailure,importSalarysRequest,importSalarysSuccess,
    exportSalarysFailure,exportSalarysRequest,exportSalarysSuccess,actionDefault
  }