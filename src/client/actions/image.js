import {createAction} from '@reduxjs/toolkit'

const HEADER_ACTION_ROLES = 'image'

const addImageRequest = createAction(`add-${HEADER_ACTION_ROLES}-request`)
const addImageSuccess = createAction(`add-${HEADER_ACTION_ROLES}-success`)
const addImageFailure = createAction(`add-${HEADER_ACTION_ROLES}-failure`)



export {
    addImageFailure,addImageRequest,addImageSuccess
      
}