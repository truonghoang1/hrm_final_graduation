import {createAction} from '@reduxjs/toolkit'
const collapsed = createAction(`collapsed-sideBar`)
const showModel =createAction(`show-model`)
const showProfile = createAction("show-profile")

export {
    collapsed,showModel,showProfile
}