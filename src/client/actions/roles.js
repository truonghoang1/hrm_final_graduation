import {createAction} from '@reduxjs/toolkit'

const HEADER_ACTION_ROLES = 'roles'

const getRolesRequest = createAction(`get-${HEADER_ACTION_ROLES}-request`)
const getRolesSuccess = createAction(`get-${HEADER_ACTION_ROLES}-success`)
const getRolesFailure = createAction(`get-${HEADER_ACTION_ROLES}-failure`)

const addRolesRequest = createAction(`add-${HEADER_ACTION_ROLES}-request`)
const addRolesSuccess = createAction(`add-${HEADER_ACTION_ROLES}-success`)
const addRolesFailure = createAction(`add-${HEADER_ACTION_ROLES}-failure`)

const updateRolesRequest = createAction(`update-${HEADER_ACTION_ROLES}-request`)
const updateRolesSuccess = createAction(`update-${HEADER_ACTION_ROLES}-success`)
const updateRolesFailure = createAction(`update-${HEADER_ACTION_ROLES}-failure`)

const detailRolesRequest = createAction(`detail-${HEADER_ACTION_ROLES}-request`)
const detailRolesSuccess = createAction(`detail-${HEADER_ACTION_ROLES}-success`)
const detailRolesFailure = createAction(`detail-${HEADER_ACTION_ROLES}-failure`)


const deleteRolesRequest = createAction(`delete-${HEADER_ACTION_ROLES}-request`)
const deleteRolesSuccess = createAction(`delete-${HEADER_ACTION_ROLES}-success`)
const deleteRolesFailure = createAction(`delete-${HEADER_ACTION_ROLES}-failure`)
const actionDefault = createAction("default-state")
export {
    getRolesFailure,getRolesSuccess,getRolesRequest,
    addRolesRequest,addRolesSuccess,addRolesFailure,
    updateRolesRequest,updateRolesFailure,updateRolesSuccess,
    deleteRolesFailure,deleteRolesRequest,deleteRolesSuccess,actionDefault,
    detailRolesFailure,detailRolesRequest,detailRolesSuccess
}