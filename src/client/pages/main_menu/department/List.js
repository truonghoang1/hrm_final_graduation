import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import Table from "../../../components/common/Table";
import FormFormik from "../../../components/common/Form";
import { Spin, Tag, Modal } from "antd";
import Detail from "../../../components/common/Detail";
import { EditFilled, DeleteFilled, ContainerFilled, } from "@ant-design/icons";
import * as time from "../../../ultis/dateTime";
import * as actions from "../../../actions/department";
import * as yup from "yup";
import { departmentInput,departmentObject } from "../../../constants/item_form";
import { useLocation, Link } from "react-router-dom";
import queryString from "query-string"

const DepartmentPage = (props) => {
  const [department, setDepartment] = useState([]);
  const [show, setShow] = useState(false);
  const [edit, setEdit] = useState(false);
  const [del, setDel] = useState({
    show: false,
    id: "",
    name:""
  });
  const [dataEdit, setDataEdit] = useState(departmentObject);
  const location = useLocation()
  const [detail, setDetail] = useState(false);
  const ref = React.useRef(null);
  const propsDatas = {
    fields: departmentInput,
    onDispatch: (data) => {
      if (edit === true) {
        props.updateDepartment(data);
        setEdit(!edit)
        setShow(!show)
        
      } else {
        props.addDepartment(data);
        setShow(!show)
      }
    },
    onCancel: () => {
      setEdit(false);
      setShow(!show);
    },
    edit,
  };
  const formik = {
    initialValues: !edit ? departmentObject : dataEdit,
    validationSchema: yup.object({
      name: yup.string().required(),
      position: yup.string().required(),
      description: yup.string()
    }),
  };

  const columns = [
    { title: "STT", dataIndex: "stt", align: "center", width: 50 ,fixed: "left",},
    {
      title: "Tên phòng ban",
      dataIndex: "name",
      align: "center",
      width: 250,
      ellipsis: true,
      fixed: "left",
    },

    {
      title: "Vị trí chức vụ",
      dataIndex: "position",
      align: "center",
      width: 200,
      ellipsis: true,
    },
    {
      title: "CreatedAt",
      dataIndex: "createdAt",
      align: "center",
      width: 100
    },
    {
      title: "Edit",
      dataIndex: "edit",
      align: "center",
      fixed: "right",
      width: 50,
      render: (value, record, index) => {
        return (
          <p
            key={index + 1}
            onClick={() => {
              props.detailDepartment({ id: record._id });
              setEdit(true);
            }}
          >
            <EditFilled style={{ color: "#66bb6a" }} />
          </p>
        );
      },
    },
    {
      title: "Delete",
      dataIndex: "delete",
      align: "center",
      fixed: "right",
      width: 70,
      render: (value, record, index) => {
        return (
          <p
            key={index + 1}
            onClick={() => {
              setDel({
                show: true,
                id: record._id,
                name:record.name
              });
            }}
          >
            <DeleteFilled style={{ color: "#ef5350" }} />
          </p>
        );
      },
    },
  ];
  //mounting
  useEffect(() => {
   let query = queryString.parse(location.search)
   let page = Number.parseInt(query.page)
   let limit =Number.parseInt(query.limit)
  if(!location.search){
       props.getListDepartment({page:1,limit:10})
  }else{
    props.getListDepartment({page,limit})
  }
  }, []);
  //updating
  useEffect(() => {
    //data change
    if (!props.departmentState.data?.length < 1) {
      let data = props.departmentState.data?.map((item, index) => {
        return {
          ...item,
          key:index+1,
          stt: (props.departmentState.page - 1) * props.departmentState.limit + index + 1,
          createdAt: time.formatDateTime(item.createdAt),
          updatedAt: time.formatDateTime(item.updatedAt),
        };
      });
      setDepartment(data);
    }else{
      setDepartment([])
    }
    //detail change
    if (props.departmentState.dataDetail!==null && edit == true) {
      console.log("helllo");
      setDataEdit(props.departmentState.dataDetail);
      setShow(!show);
    } else if (props.departmentState.dataDetail !== null) {
      setDetail(!detail);
    }
  }, [ref, props.departmentState.data, props.departmentState.dataDetail]);
console.log(edit,show);
  return (
    <div>
      {props.departmentState.isLoading === true ? (
        <Spin>
          <Table />
        </Spin>
      ) : (
        <>
          <Table
            onAdd={() => {
              setShow(!show);
            }}
            columns={columns}
            data={department}
            totalPage={props.departmentState.totalPage}
            pageIndex={props.departmentState.page}
            onPagination={props.getListDepartment}
          />
          <Modal
          centered
            open={del.show}
            onOk={() => {
              props.deleteDepartment({ id: del.id });
              setDel({ show: false, id: "" });
            }}
            onCancel={() => {
              setDel({ ...del, show: false });
            }}
          >
            <p>Delete {del.name}?</p>
          </Modal>
          {/* <Detail
            open={detail}
            onCancel={(state) => {
              setDetail(state);
              props.defaultState();
            }}
            onOpen={(state) => setDetail(state)}
            data={props.departmentState.detail}
            nameDetail={"app"}
          /> */}

          <FormFormik
            propsDatas={propsDatas}
            formik={formik}
            showForm={(status) => {
              setShow(status);
            }}
            show={show}
          />
        </>
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    departmentState: state.departmentState,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getListDepartment: (data) => {
      dispatch(actions.getDepartmentRequest(data));
    },
    addDepartment: (data) => {
      console.log(data);
      dispatch(actions.addDepartmentRequest(data));
    },
    updateDepartment: (data) => {
      dispatch(actions.updateDepartmentRequest(data));
    },
    deleteDepartment: (data) => {
      dispatch(actions.deleteDepartmentRequest(data));
    },
    detailDepartment: (data) => {
      dispatch(actions.getDepartmentDetailRequest(data));
    },
    defaultState: () => {
      dispatch(actions.actionDefault());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DepartmentPage);
