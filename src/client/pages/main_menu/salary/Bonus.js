import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import Table from "../../../components/common/Table";
import FormFormik from "../../../components/common/Form";
import { Spin, Tag, Modal } from "antd";
import Detail from "../../../components/common/Detail";
import { EditFilled, DeleteFilled, ContainerFilled, } from "@ant-design/icons";
import * as time from "../../../ultis/dateTime";
import * as actions from "../../../actions/bonus";
import * as yup from "yup";
import { bonusInput,bonusObject } from "../../../constants/item_form";
import { useLocation, Link } from "react-router-dom";
import queryString from "query-string"

const BonusPage = (props) => {
  const [bonus, setBonus] = useState([]);
  const [show, setShow] = useState(false);
  const [edit, setEdit] = useState(false);
  const [del, setDel] = useState({
    show: false,
    id: "",
  });
  
  const [dataEdit, setDataEdit] = useState(bonusObject);

  const location = useLocation()
  const [detail, setDetail] = useState(false);
  const ref = React.useRef(null);
  const propsDatas = {
    fields: bonusInput,
    onDispatch: (data) => {
      if (edit === true) {
        props.updateBonus(data);
      } else {
        props.addBonus(data);
      }
    },
    onCancel: () => {
      setEdit(false);
      setShow(!show);
    },
    edit,
  };
  const formik = {
    initialValues: !edit ? bonusObject : dataEdit,
    validationSchema: yup.object({
      name: yup.string().required(),
      amount: yup.string().required(),
    }),
  };
  const columns = [
    { title: "STT", dataIndex: "stt", align: "center", width: 50 ,fixed: "left",},
    {
      title: "Tên trợ cấp",
      dataIndex: "name",
      align: "center",
      width: 250,
      ellipsis: true,
      fixed: "left",
    },

    {
      title: "mức hỗ trợ",
      dataIndex: "amount",
      align: "center",
      width: 200,
      ellipsis: true,
    },
    {
      title: "CreatedAt",
      dataIndex: "createdAt",
      align: "center",
      width: 100
    },
    {
      title: "Edit",
      dataIndex: "edit",
      align: "center",
      fixed: "right",
      width: 50,
      render: (value, record, index) => {
        return (
          <p
            key={index + 1}
            onClick={() => {
              props.detailBonus({ id: record._id });
              setOpen("");
              setEdit(true);
            }}
          >
            <EditFilled style={{ color: "#66bb6a" }} />
          </p>
        );
      },
    },
    {
      title: "Delete",
      dataIndex: "delete",
      align: "center",
      fixed: "right",
      width: 70,
      render: (value, record, index) => {
        return (
          <p
            key={index + 1}
            onClick={() => {
              setDel({
                show: true,
                id: record._id,
              });
            }}
          >
            <DeleteFilled style={{ color: "#ef5350" }} />
          </p>
        );
      },
    },
   
  ];
  //mounting
  useEffect(() => {
   let query = queryString.parse(location.search)
   let page = Number.parseInt(query.page)
   let limit =Number.parseInt(query.limit)
  if(!location.search){
       props.getListBonus({page:1,limit:10})
  }else{
    props.getListBonus({page,limit})
  }
  }, []);
  //updating
  useEffect(() => {
    //data change
    if (!props.bonusState.data?.length < 1) {
      let data = props.bonusState.data?.map((item, index) => {
        return {
          ...item,
          key:index+1,
          stt: (props.bonusState.page - 1) * props.bonusState.limit + index + 1,
          createdAt: time.formatDateTime(item.createdAt),
          updatedAt: time.formatDateTime(item.updatedAt),
        };
      });
      setBonus(data);
    }else{
        setBonus([])
    }
    //detail change
    if (props.bonusState.detail && edit == true) {
      setDataEdit(props.bonusState.detail);
      setShow(true);
    } else if (props.bonusState.detail !== null) {
      setDetail(!detail);
    }
  }, [ref, props.bonusState.data, props.bonusState.detail]);

  return (
    <div>
      {props.bonusState.isLoading === true ? (
        <Spin>
          <Table />
        </Spin>
      ) : (
        <>
          <Table
            onAdd={() => {
              setShow(!show);
            }}
            columns={columns}
            data={bonus}
            totalPage={props.bonusState.totalPage}
            pageIndex={props.bonusState.page}
            onPagination={props.getListBonus}
          />
          <Modal
          centered
            open={del.show}
            onOk={() => {
              props.deleteBonus({ id: del.id });
              setDel({ show: false, id: "" });
            }}
            onCancel={() => {
              setDel({ ...del, show: false });
            }}
          >
            <p>Delete {del.name}?</p>
          </Modal>
          {/* <Detail
            open={detail}
            onCancel={(state) => {
              setDetail(state);
              props.defaultState();
            }}
            onOpen={(state) => setDetail(state)}
            data={props.bonusState.detail}
            nameDetail={"app"}
          /> */}

          <FormFormik
            propsDatas={propsDatas}
            formik={formik}
            showForm={(status) => {
              setShow(status);
            }}
            show={show}
          />
        </>
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    bonusState: state.bonusState,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getListBonus: (data) => {
      dispatch(actions.getBonusRequest(data));
    },
    addBonus: (data) => {
      console.log(data);
      dispatch(actions.addBonusRequest(data));
    },
    updateBonus: (data) => {
      dispatch(actions.updateBonusRequest(data));
    },
    deleteBonus: (data) => {
      dispatch(actions.deleteBonusRequest(data));
    },
    detailBonus: (data) => {
      dispatch(actions.getBonusDetailRequest(data));
    },
    defaultState: () => {
      dispatch(actions.actionDefault());
    },
   
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BonusPage);
