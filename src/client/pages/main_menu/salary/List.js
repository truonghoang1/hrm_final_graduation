import React, { useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import Table from "../../../components/common/Table";
import FormFormik from "../../../components/common/Form";
import { Spin, Tag, Modal } from "antd";
import Detail from "../../../components/common/Detail";
import { EditFilled, DeleteFilled, ContainerFilled, } from "@ant-design/icons";
import * as time from "../../../ultis/dateTime";
import * as actions from "../../../actions/salary";
import {getBonusRequest} from "../../../actions/bonus"
import {getEmployeeRequest} from "../../../actions/employee"
import * as yup from "yup";
import { salaryInput,salaryObject } from "../../../constants/item_form";
import { useLocation, Link } from "react-router-dom";
import queryString from "query-string"

const SalaryPage = (props) => {
  const [salary, setSalary] = useState([]);
  const [open, setOpen] = useState("");
  const [show, setShow] = useState(false);
  const [edit, setEdit] = useState(false);
  const [del, setDel] = useState({
    show: false,
    id: "",
  });
  
  const [dataEdit, setDataEdit] = useState(salaryObject);

  const location = useLocation()
  const [detail, setDetail] = useState(false);
  const ref = React.useRef(null);
  const propsDatas = useMemo(()=>{
    return{
      fields: salaryInput,
      onDispatch: (data) => {
        if (edit === true) {
          props.updateSalary(data);
        } else {
          props.addSalary(data);
        }
      },
      bonus: props.bonusState.data?props.bonusState.data.map((item)=>{
        return{name:item.name,value:item._id}
      }):[],
      staff: props.employeeState.data? props.employeeState.data.map(item=>{
        return{name:item.full_name,value:item._id}
      }):[],
      onCancel: () => {
        setEdit(false);
        setShow(!show);
      },
      edit,
    };
  },[
    props.bonusState.data,props.employeeState.data
  ])
  const formik = {
    initialValues: !edit ? salaryObject : dataEdit,
    validationSchema: yup.object({
      staff: yup.string().required(),
      salary: yup.string().required(),
    }),
  };
  const columns = [
    { title: "STT", dataIndex: "stt", align: "center", width: 50 ,fixed: "left",},
    {
      title: "Họ Tên",
      dataIndex: "full_name",
      align: "center",
      width: 250,
      ellipsis: true,
      fixed: "left",
    },

    {
      title: "Email",
      dataIndex: "email",
      align: "center",
      width: 200,
      ellipsis: true,
    },{
      title: "Lương",
      dataIndex: "salary",
      align: "center",
      width: 200,
      ellipsis: true,
    }
    ,
    {
      title: "CreatedAt",
      dataIndex: "createdAt",
      align: "center",
      width: 100
    },
    {
      title: "Edit",
      dataIndex: "edit",
      align: "center",
      fixed: "right",
      width: 50,
      render: (value, record, index) => {
        return (
          <p
            key={index + 1}
            onClick={() => {
              props.detailApp({ id: record._id });
              setOpen("");
              setEdit(true);
            }}
          >
            <EditFilled style={{ color: "#66bb6a" }} />
          </p>
        );
      },
    },
    {
      title: "Delete",
      dataIndex: "delete",
      align: "center",
      fixed: "right",
      width: 70,
      render: (value, record, index) => {
        return (
          <p
            key={index + 1}
            onClick={() => {
              setDel({
                show: true,
                id: record._id,
              });
            }}
          >
            <DeleteFilled style={{ color: "#ef5350" }} />
          </p>
        );
      },
    },
    // {
    //   title: "Detail",
    //   dataIndex: "detail",
    //   align: "center",
    //   fixed: "right",
    //   width: 70,
    //   render: (value, record, index) => {
    //     return (
    //       <p key={index+1}
    //         onClick={() => {
    //           props.detailApp({ id: record._id });
    //         }}
    //       >
    //         <ContainerFilled style={{ color: "#7e57c2" }} />
    //       </p>
    //     );
    //   },
    // },
  ];
  //mounting
  useEffect(() => {
   let query = queryString.parse(location.search)
   let page = Number.parseInt(query.page)
   let limit =Number.parseInt(query.limit)
  if(!location.search){
       props.getListSalary({page:1,limit:10})
  }else{
    props.getListSalary({page,limit})
  }
  }, []);
  //updating
  useEffect(() => {
    //data change
    if (!props.salaryState.data?.length < 1) {
      let data = props.salaryState.data?.map((item, index) => {
        const {staff,...rest}=item
        let
         salary={...rest,full_name:item.staff?.full_name,email:item.staff?.email}
        return {
          ...salary,
          key:index+1,
          stt: (props.salaryState.page - 1) * props.salaryState.limit + index + 1,
          createdAt: time.formatDateTime(item.createdAt),
          updatedAt: time.formatDateTime(item.updatedAt),
        };
      });
      setSalary(data);
    }else{
      setSalary([])
    }
    //detail change
    if (props.salaryState.detail && edit == true) {
      setDataEdit(props.salaryState.detail);
      setShow(true);
    } else if (props.salaryState.detail !== null) {
      setDetail(!detail);
    }
  }, [ref, props.salaryState.data, props.salaryState.detail]);
 console.log(props.salaryState.data);
  return (
    <div>
      {props.salaryState.isLoading === true ? (
        <Spin>
          <Table />
        </Spin>
      ) : (
        <>
          <Table
            onAdd={() => {
              props.getBonus({page:1,limit:10})
              props.getEmployee({page:1,limit:10})
              setShow(!show);
            }}
            onPagination={props.getListSalary}
            columns={columns}
            data={salary}
            totalPage={props.salaryState.totalPage}
            pageIndex={props.salaryState.page}
            onSearch={(data)=>{props.getListSalary({page:1,limit:10,...data})}}
          />
          <Modal
          centered
            open={del.show}
            onOk={() => {
              props.deleteSalary({ id: del.id });
              setDel({ show: false, id: "" });
            }}
            onCancel={() => {
              setDel({ ...del, show: false });
            }}
          >
            <p>Delete {del.name}?</p>
          </Modal>
          {/* <Detail
            open={detail}
            onCancel={(state) => {
              setDetail(state);
              props.defaultState();
            }}
            onOpen={(state) => setDetail(state)}
            data={props.salaryState.detail}
            nameDetail={"app"}
          /> */}

          <FormFormik
            propsDatas={propsDatas}
            formik={formik}
            showForm={(status) => {
              setShow(status);
            }}
            show={show}
          />
        </>
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    salaryState: state.salaryState,
    bonusState:state.bonusState,
    employeeState:state.employeeState
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getListSalary: (data) => {
      dispatch(actions.getSalaryRequest(data));
    },
    addSalary: (data) => {
      console.log(data);
      dispatch(actions.addSalaryRequest(data));
    },
    updateSalary: (data) => {
      dispatch(actions.updateSalaryRequest(data));
    },
    deleteSalary: (data) => {
      dispatch(actions.deleteSalaryRequest(data));
    },
    detailSalary: (data) => {
      dispatch(actions.getSalaryDetailRequest(data));
    },
    defaultState: () => {
      dispatch(actions.actionDefault());
    },
    importFileXLSX:(data)=>{
      dispatch(actions.importSalarysRequest(data))
    },
    exportFileXlsx:(data)=>{
      dispatch(actions.exportSalarysRequest(data))
    },
    getBonus:(data)=>{
      dispatch(getBonusRequest(data))
    },getEmployee:(data)=>{
      dispatch(getEmployeeRequest(data))
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SalaryPage);
