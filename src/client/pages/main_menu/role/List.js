import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import Table from "../../../components/common/Table";
import FormFormik from "../../../components/common/Form";
import { Spin, Tag, Modal } from "antd";
import Detail from "../../../components/common/Detail";
import { EditFilled, DeleteFilled, ContainerFilled, } from "@ant-design/icons";
import * as time from "../../../ultis/dateTime";
import * as actions from "../../../actions/roles";
import * as yup from "yup";
import { roleInput,roleObject } from "../../../constants/item_form";
import { useLocation, Link } from "react-router-dom";
import queryString from "query-string"

const RolePage = (props) => {
  const [role, setRole] = useState([]);
  const [show, setShow] = useState(false);
  const [edit, setEdit] = useState(false);
  const [del, setDel] = useState({
    show: false,
    id: "",
    name:""
  });
  const [dataEdit, setDataEdit] = useState(roleObject);
  const location = useLocation()
  const [detail, setDetail] = useState(false);
  const ref = React.useRef(null);
  const propsDatas = {
    fields: roleInput,
    onDispatch: (data) => {
      if (edit === true) {
        props.updateRole(data);
        setEdit(!edit)
        setShow(!show)
        
      } else {
        props.addRole(data);
        setShow(!show)
      }
    },
    onCancel: () => {
      setEdit(false);
      setShow(!show);
    },
    edit,
  };
  const formik = {
    initialValues: !edit ? roleObject : dataEdit,
    validationSchema: yup.object({
      name: yup.string().required(),
      level: yup.string().required(),
      description: yup.string()
    }),
  };

  const columns = [
    { title: "STT", dataIndex: "stt", align: "center", width: 50 ,fixed: "left",},
    {
      title: "Tên quyền",
      dataIndex: "name",
      align: "center",
      width: 250,
      ellipsis: true,
      fixed: "left",
    },

    {
      title: "quyền",
      dataIndex: "level",
      align: "center",
      width: 200,
      ellipsis: true,
    },
    {
      title: "CreatedAt",
      dataIndex: "createdAt",
      align: "center",
      width: 100
    },
    {
      title: "Edit",
      dataIndex: "edit",
      align: "center",
      fixed: "right",
      width: 50,
      render: (value, record, index) => {
        return (
          <p
            key={index + 1}
            onClick={() => {
              props.detailRole({ id: record._id });
              setEdit(true);
            }}
          >
            <EditFilled style={{ color: "#66bb6a" }} />
          </p>
        );
      },
    },
    {
      title: "Delete",
      dataIndex: "delete",
      align: "center",
      fixed: "right",
      width: 70,
      render: (value, record, index) => {
        return (
          <p
            key={index + 1}
            onClick={() => {
              setDel({
                show: true,
                id: record._id,
                name:record.name
              });
            }}
          >
            <DeleteFilled style={{ color: "#ef5350" }} />
          </p>
        );
      },
    },
  ];
  //mounting
  useEffect(() => {
   let query = queryString.parse(location.search)
   let page = Number.parseInt(query.page)
   let limit =Number.parseInt(query.limit)
  if(!location.search){
       props.getListRole({page:1,limit:10})
  }else{
    props.getListRole({page,limit})
  }
  }, []);
  //updating
  useEffect(() => {
    //data change
    if (!props.RoleState.data?.length < 1) {
      let data = props.RoleState.data?.map((item, index) => {
        return {
          ...item,
          key:index+1,
          stt: (props.RoleState.page - 1) * props.RoleState.limit + index + 1,
          createdAt: time.formatDateTime(item.createdAt),
          updatedAt: time.formatDateTime(item.updatedAt),
        };
      });
      setRole(data);
    }else{
      setRole([])
    }
    //detail change
    if (props.RoleState.dataDetail!==null && edit == true) {
      console.log("helllo");
      setDataEdit(props.RoleState.dataDetail);
      setShow(!show);
    } else if (props.RoleState.dataDetail !== null) {
      setDetail(!detail);
    }
  }, [ref, props.RoleState.data, props.RoleState.dataDetail]);
console.log(edit,show);
  return (
    <div>
      {props.RoleState.isLoading === true ? (
        <Spin>
          <Table />
        </Spin>
      ) : (
        <>
          <Table
            onAdd={() => {
              setShow(!show);
            }}
            columns={columns}
            data={role}
            totalPage={props.RoleState.totalPage}
            pageIndex={props.RoleState.page}
            onPagination={props.getListRole}
          />
          <Modal
          centered
            open={del.show}
            onOk={() => {
              props.deleteRole({ id: del.id });
              setDel({ show: false, id: "" });
            }}
            onCancel={() => {
              setDel({ ...del, show: false });
            }}
          >
            <p>Delete {del.name}?</p>
          </Modal>
          {/* <Detail
            open={detail}
            onCancel={(state) => {
              setDetail(state);
              props.defaultState();
            }}
            onOpen={(state) => setDetail(state)}
            data={props.RoleState.detail}
            nameDetail={"app"}
          /> */}

          <FormFormik
            propsDatas={propsDatas}
            formik={formik}
            showForm={(status) => {
              setShow(status);
            }}
            show={show}
          />
        </>
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    RoleState: state.roleState,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getListRole: (data) => {
      dispatch(actions.getRolesRequest(data));
    },
    addRole: (data) => {
      console.log(data);
      dispatch(actions.addRolesRequest(data));
    },
    updateRole: (data) => {
      dispatch(actions.updateRolesRequest(data));
    },
    deleteRole: (data) => {
      dispatch(actions.deleteRolesRequest(data));
    },
    detailRole: (data) => {
      dispatch(actions.detailRolesRequest(data));
    },
    defaultState: () => {
      dispatch(actions.actionDefault());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RolePage);
