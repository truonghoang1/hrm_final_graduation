import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import Table from "../../../components/common/Table";
import FormFormik from "../../../components/common/Form";
import { Spin, Tag, Modal } from "antd";
import Detail from "../../../components/common/Detail";
import { EditFilled, DeleteFilled, ContainerFilled, } from "@ant-design/icons";
import * as time from "../../../ultis/dateTime";
import * as actions from "../../../actions/employee";
import {getDepartmentRequest} from "../../../actions/department"
import * as yup from "yup";
import { employeeInput,employeeObject } from "../../../constants/item_form";
import { useLocation, Link } from "react-router-dom";
import queryString from "query-string"

const EmployeePage = (props) => {
  const [employee, setEmployes] = useState([]);
  const [show, setShow] = useState(false);
  const [edit, setEdit] = useState(false);
  const [del, setDel] = useState({
    show: false,
    id: "",
    name:""
  });
  
  const [dataEdit, setDataEdit] = useState(employeeObject);
  const [dataDetail,setDataDetail] = useState({})
  const location = useLocation()
  const [detail, setDetail] = useState(false);
  const ref = React.useRef(null);
  const propsDatas = React.useMemo(()=>{
    return{
      fields: employeeInput,
      onDispatch: (data) => {
        if (edit === true) {
          props.updateEmployee(data);
          setEdit(false)
          setShow(!show)
        } else {
          props.addEmployee(data);
        }
      },
      department: props.departmentState.data?props.departmentState.data.map((item)=>{return{name:item.name,value:item._id}}):[],
      onCancel: () => {
        setEdit(false);
        setShow(!show);
      },
      edit,
    };
  },[props.departmentState.data])
  
  const formik = {
    initialValues: !edit ? employeeObject : dataEdit,
    validationSchema: yup.object({
      full_name: yup.string().required(),
      email: yup.string().required(),
      gender: yup.string().required(),
      cmnd: yup.string().required()
    }),
  };

  const columns = [
    { title: "STT", dataIndex: "stt", align: "center", width: 50 ,fixed: "left",},
    {
      title: "Họ Tên",
      dataIndex: "full_name",
      align: "center",
      width: 250,
      ellipsis: true,
      fixed: "left",
    },

    {
      title: "Email",
      dataIndex: "email",
      align: "center",
      width: 200,
      ellipsis: true,
    },

    {
      title: "cmnd",
      dataIndex: "cmnd",
      align: "center",
      width:100,
    },
    {
      title: "CreatedAt",
      dataIndex: "createdAt",
      align: "center",
      width: 100
    },
    {
      title: "Edit",
      dataIndex: "edit",
      align: "center",
      fixed: "right",
      width: 50,
      render: (value, record, index) => {
        return (
          <p
            key={index + 1}
            onClick={() => {
              props.detailEmployee({ id: record._id });
              props.getDepartment({page:1,limit:10})
              setEdit(true);
            }}
          >
            <EditFilled style={{ color: "#66bb6a" }} />
          </p>
        );
      },
    },
    {
      title: "Delete",
      dataIndex: "delete",
      align: "center",
      fixed: "right",
      width: 70,
      render: (value, record, index) => {
        return (
          <p
            key={index + 1}
            onClick={() => {
              setDel({
                show: true,
                id: record._id,
                name:record.full_name
              });
            }}
          >
            <DeleteFilled style={{ color: "#ef5350" }} />
          </p>
        );
      },
    },
    {
      title: "Detail",
      dataIndex: "detail",
      align: "center",
      fixed: "right",
      width: 70,
      render: (value, record, index) => {
        return (
          <p key={index+1}
            onClick={() => {
              props.detailEmployee({ id: record._id });
            }}
          >
            <ContainerFilled style={{ color: "#7e57c2" }} />
          </p>
        );
      },
    },
  ];
  //mounting
  useEffect(() => {
   let query = queryString.parse(location.search)
   let page = Number.parseInt(query.page)
   let limit =Number.parseInt(query.limit)
  if(!location.search){
       props.getListEmployee({page:1,limit:10})
  }else{
    props.getListEmployee({page,limit})
  }
  }, []);
  //updating
  useEffect(() => {
    //data change
    if (!props.employeeState.data?.length < 1) {
      let data = props.employeeState.data?.map((item, index) => {
       
        return {
         ...item,
          key:index+1,
          stt: (props.employeeState.page - 1) * props.employeeState.limit + index + 1,
          createdAt: time.formatDateTime(item.createdAt),
          updatedAt: time.formatDateTime(item.updatedAt),
        };
      });
      setEmployes(data);
    }else{
      setEmployes([])
    }
    //detail change
    if (props.employeeState.detail && edit == true) {
      let {department,...rest}=props.employeeState.detail
      let dataD ={
        ...rest,department:department?.name
      }
      setDataEdit(dataD);
      setShow(true);
    } else if (props.employeeState.detail !== null) {
       setDataDetail(props.employeeState.detail)
      setDetail(!detail);
    }
  }, [ref, props.employeeState.data, props.employeeState.detail]);

  return (
    <div>
      {props.employeeState.isLoading === true ? (
        <Spin>
          <Table />
        </Spin>
      ) : (
        <>
       
          <Table
            onAdd={() => {
              props.getDepartment({page:1,limit:10})
              setShow(!show);
            }}
            onSearch={(data)=>{props.getListEmployee({page:1,limit:10,...data})}}
            columns={columns}
            data={employee}
            totalPage={props.employeeState.totalPage}
            pageIndex={props.employeeState.page}
            onImportFile={props.importFileXLSX}
            onExportFile={props.exportFileXlsx}
            dataFilterExport={employeeObject}
            onPagination={props.getListEmployee}
          />
 
          <Modal
          centered
            open={del.show}
            onOk={() => {
              props.deleteEmployee({ id: del.id });
              setDel({ show: false, id: "" });
            }}
            onCancel={() => {
              setDel({ ...del, show: false });
            }}
          >
            <p>Delete {del.name}?</p>
          </Modal>
          <Detail
            open={detail}
            onCancel={(state) => {
              setDetail(state);
              props.defaultState();
            }}
            onOpen={(state) => setDetail(state)}
            data={dataDetail}
            nameDetail={"employee"}
          />

          <FormFormik
            propsDatas={propsDatas}
            formik={formik}
            showForm={(status) => {
              setShow(status);
            }}
            show={show}
          />
        </>
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    employeeState: state.employeeState,
    departmentState:state.departmentState
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getListEmployee: (data) => {
      dispatch(actions.getEmployeeRequest(data));
    },
    addEmployee: (data) => {
      console.log(data);
      dispatch(actions.addEmployeeRequest(data));
    },
    updateEmployee: (data) => {
      dispatch(actions.updateEmployeeRequest(data));
    },
    deleteEmployee: (data) => {
      dispatch(actions.deleteEmployeeRequest(data));
    },
    detailEmployee: (data) => {
      dispatch(actions.getEmployeeDetailRequest(data));
    },
    defaultState: () => {
      dispatch(actions.actionDefault());
    },
    importFileXLSX:(data)=>{
      dispatch(actions.importEmployeeRequest(data))
    },
    exportFileXlsx:(data)=>{
      dispatch(actions.exportEmployeeRequest(data))
    },getDepartment:(data)=>{
      dispatch(getDepartmentRequest(data))
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EmployeePage);
