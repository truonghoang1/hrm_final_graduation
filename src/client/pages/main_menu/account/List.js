import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import Table from "../../../components/common/Table";
import FormFormik from "../../../components/common/Form";
import { Spin, Tag, Modal } from "antd";
import { EditFilled, DeleteFilled } from "@ant-design/icons";
import * as time from "../../../ultis/dateTime";
import * as actions from "../../../actions/account";
import * as yup from "yup";
import { accountInput,objectAccount } from "../../../constants/item_form";
import { useLocation, Link } from "react-router-dom";
import queryString from "query-string"

const AccountPage = (props) => {
  const [account, setAccount] = useState([]);
  const [show, setShow] = useState(false);
  const [edit, setEdit] = useState(false);
  const [del, setDel] = useState({
    show: false,
    id: "",
    name:""
  });
  const [dataEdit, setDataEdit] = useState(objectAccount);
  const location = useLocation()
  const [detail, setDetail] = useState(false);
  const ref = React.useRef(null);
  const propsDatas = {
    fields: accountInput,
    onDispatch: (data) => {
      if (edit === true) {
        props.updateAccount(data);
        setEdit(!edit)
        setShow(!show)
        
      } else {
        props.addAccount(data);
        setShow(!show)
      }
    },
    onCancel: () => {
      setEdit(false);
      setShow(!show);
    },
    edit,
  };
  const formik = {
    initialValues: !edit ? objectAccount : dataEdit,
    validationSchema: yup.object({
      name: yup.string().required(),
      position: yup.string().required(),
      description: yup.string()
    }),
  };

  const columns = [
    { title: "STT", dataIndex: "stt", align: "center", width: 50 ,fixed: "left",},
    {
      title: "Email",
      dataIndex: "email",
      align: "center",
      width: 250,
      ellipsis: true,
      fixed: "left",
    },

    {
      title: "role",
      dataIndex: "role",
      align: "center",
      width: 200,
      ellipsis: true,
    },
    {
      title: "CreatedAt",
      dataIndex: "createdAt",
      align: "center",
      width: 100
    },
    {
      title: "Edit",
      dataIndex: "edit",
      align: "center",
      fixed: "right",
      width: 50,
      render: (value, record, index) => {
        return (
          <p
            key={index + 1}
            onClick={() => {
              props.detailAccount({ id: record._id });
              setEdit(true);
            }}
          >
            <EditFilled style={{ color: "#66bb6a" }} />
          </p>
        );
      },
    },
    {
      title: "Delete",
      dataIndex: "delete",
      align: "center",
      fixed: "right",
      width: 70,
      render: (value, record, index) => {
        return (
          <p
            key={index + 1}
            onClick={() => {
              setDel({
                show: true,
                id: record._id,
                name:record.email
              });
            }}
          >
            <DeleteFilled style={{ color: "#ef5350" }} />
          </p>
        );
      },
    },
  ];
  //mounting
  useEffect(() => {
   let query = queryString.parse(location.search)
   let page = Number.parseInt(query.page)
   let limit =Number.parseInt(query.limit)
  if(!location.search){
       props.getListAccount({page:1,limit:10})
  }else{
    props.getListAccount({page,limit})
  }
  }, []);
  //updating
  useEffect(() => {
    //data change
    if (!props.accountState.data?.length < 1) {
      let data = props.accountState.data?.map((item, index) => {
        let  {role,...rest}=item
        let account = {...rest,role:item.role?.name}
        return {
          ...account,
          key:index+1,
          stt: (props.accountState.page - 1) * props.accountState.limit + index + 1,
          createdAt: time.formatDateTime(item.createdAt),
          updatedAt: time.formatDateTime(item.updatedAt),
        };
      });
      setAccount(data);
    }else{
      setAccount([])
    }
    //detail change
    if (props.accountState.dataDetail!==null && edit == true) {
      console.log("helllo");
      setDataEdit(props.accountState.dataDetail);
      setShow(!show);
    } else if (props.accountState.dataDetail !== null) {
      setDetail(!detail);
    }
  }, [ref, props.accountState.data, props.accountState.dataDetail]);
console.log(edit,show);
  return (
    <div>
      {props.accountState.isLoading === true ? (
        <Spin>
          <Table />
        </Spin>
      ) : (
        <>
          <Table
            onAdd={() => {
              setShow(!show);
            }}
            columns={columns}
            data={account}
            totalPage={props.accountState.totalPage}
            pageIndex={props.accountState.page}
            onPagination={props.getListAccount}
          />
          <Modal
          centered
            open={del.show}
            onOk={() => {
              props.deleteAccount({ id: del.id });
              setDel({ show: false, id: "" });
            }}
            onCancel={() => {
              setDel({ ...del, show: false });
            }}
          >
            <p>Delete {del.name}?</p>
          </Modal>
          {/* <Detail
            open={detail}
            onCancel={(state) => {
              setDetail(state);
              props.defaultState();
            }}
            onOpen={(state) => setDetail(state)}
            data={props.accountState.detail}
            nameDetail={"app"}
          /> */}

          <FormFormik
            propsDatas={propsDatas}
            formik={formik}
            showForm={(status) => {
              setShow(status);
            }}
            show={show}
          />
        </>
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    accountState: state.accountState,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getListAccount: (data) => {
      dispatch(actions.getAccountRequest(data));
    },
    addAccount: (data) => {
      console.log(data);
      dispatch(actions.addAccountRequest(data));
    },
    updateAccount: (data) => {
      dispatch(actions.updateAccountRequest(data));
    },
    deleteAccount: (data) => {
      dispatch(actions.deleteAccountRequest(data));
    },
    detailAccount: (data) => {
      dispatch(actions.getAccountDetailRequest(data));
    },
    defaultState: () => {
      dispatch(actions.actionDefault());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AccountPage);
