import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import Table from "../../../components/common/Table";
import FormFormik from "../../../components/common/Form";
import { Spin, Tag, Modal } from "antd";
import Detail from "../../../components/common/Detail";
import { CheckOutlined } from "@ant-design/icons";
import * as time from "../../../ultis/dateTime";
import * as actions from "../../../actions/timesheet";
import * as yup from "yup";
import { timeSheetInput,timeSheetObject } from "../../../constants/item_form";
import { useLocation, Link } from "react-router-dom";
import queryString from "query-string"

const TimeSheetAdminPage = (props) => {
  const [timeSheetAdmin, setEmployes] = useState([]);
  const [show, setShow] = useState(false);
  const [edit, setEdit] = useState(false);
  const [del, setDel] = useState({
    show: false,
    id: "",
    name:""
  });
  
  const [dataEdit, setDataEdit] = useState(timeSheetObject);
  const [dataDetail,setDataDetail] = useState({})
  const location = useLocation()
  const [detail, setDetail] = useState(false);
  const ref = React.useRef(null);
  const propsDatas = React.useMemo(()=>{
    return{
      fields: timeSheetInput,
      onDispatch: (data) => {
        if (edit === true) {
          props.updateTimeSheetAdmin(data);
        } else {
          props.addTimeSheetAdmin(data);
        }
      },
      
      onCancel: () => {
        setEdit(false);
        setShow(!show);
      },
      edit,
    };
  },[])
  
  const formik = {
    initialValues: !edit ? timeSheetObject : dataEdit,
    validationSchema: yup.object({
      
    }),
  };

  const columns = [
    { title: "STT", dataIndex: "stt", align: "center", width: 50 ,fixed: "left",},
    {  title: "Email",
    dataIndex: "email",
    align: "center",
    width: 100},
    {
      title: "Tên",
      dataIndex: "name",
      align: "center",
      width: 100
    },
    {
      title: "Ngày",
      dataIndex: "date",
      align: "center",
      width: 100
    },
    {
      title: "Loại",
      dataIndex: "type",
      align: "center",
      width: 100
    },
    {
      title: "Duyệt",
      dataIndex: "edit",
      align: "center",
      fixed: "right",
      width: 50,
      render: (value, record, index) => {
        return (
          <p
            key={index + 1}
            onClick={() => {
              props.updateTimeSheetAdmin({ id: record._id });
        
            
            }}
          >
            <CheckOutlined style={{ color: "#66bb6a" }} />
           
          </p>
        );
      },
    },

    // {
    //   title: "Detail",
    //   dataIndex: "detail",
    //   align: "center",
    //   fixed: "right",
    //   width: 70,
    //   render: (value, record, index) => {
    //     return (
    //       <p key={index+1}
    //         onClick={() => {
    //           props.detailTimeSheetAdmin({ id: record._id });
    //         }}
    //       >
    //         <ContainerFilled style={{ color: "#7e57c2" }} />
    //       </p>
    //     );
    //   },
    // },
  ];
  //mounting
  useEffect(() => {
   let query = queryString.parse(location.search)
   let page = Number.parseInt(query.page)
   let limit =Number.parseInt(query.limit)
  if(!location.search){
       props.getListTimeSheetAdmin({page:1,limit:10})
  }else{
    props.getListTimeSheetAdmin({page,limit})
  }
  }, []);
  //updating
  useEffect(() => {
    //data change
    if (!props.timesheetState.data?.length < 1) {
      let data = props.timesheetState.data?.map((item, index) => {
        
        return {
          ...item,
          email:item.staff?.email,
          key:index+1,
          stt: (props.timesheetState.page - 1) * props.timesheetState.limit + index + 1,
          createdAt: time.formatDateTime(item.createdAt),
          updatedAt: time.formatDateTime(item.updatedAt),
        };
      });
      setEmployes(data);
    }else{
      setEmployes([])
    }
    //detail change
    if (props.timesheetState.detail && edit == true) {
      const {account,...rest} = props.timesheetState.detail
      const detailEmp = {
        ...rest,
        email:account?.email
      }
      setDataEdit(detailEmp);
      setShow(true);
    } else if (props.timesheetState.detail !== null) {
       const {account,...rest} = props.timesheetState.detail
       const data = {...rest,email:account?.email}
       setDataDetail(data)
      setDetail(!detail);
    }
  }, [ref, props.timesheetState.data, props.timesheetState.detail]);

  return (
    <div>
      {props.timesheetState.isLoading === true ? (
        <Spin>
          <Table />
        </Spin>
      ) : (
        <>
       
          <Table
            onAdd={() => {
              props.getDepartment({page:1,limit:10})
              setShow(!show);
            }}
            columns={columns}
            data={timeSheetAdmin}
            totalPage={props.timesheetState.totalPage}
            pageIndex={props.timesheetState.page}
            onPagination={props.getListTimeSheetAdmin}
            onExportFile={props.exportFileXlsx}
            dataFilterExport={timeSheetObject}
          />
 
          <Modal
          centered
            open={del.show}
            onOk={() => {
              props.deleteTimeSheetAdmin({ id: del.id });
              setDel({ show: false, id: "" });
            }}
            onCancel={() => {
              setDel({ ...del, show: false });
            }}
          >
            <p>Delete {del.name}?</p>
          </Modal>
          <Detail
            open={detail}
            onCancel={(state) => {
              setDetail(state);
              props.defaultState();
            }}
            onOpen={(state) => setDetail(state)}
            data={dataDetail}
            nameDetail={"timeSheetAdmin"}
          />

          <FormFormik
            propsDatas={propsDatas}
            formik={formik}
            showForm={(status) => {
              setShow(status);
            }}
            show={show}
          />
        </>
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    timesheetState: state.timesheetState,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getListTimeSheetAdmin: (data) => {
      dispatch(actions.getTimeSheetAdminRequest(data));
    },
    updateTimeSheetAdmin: (data) => {
      dispatch(actions.updateTimeSheetAdminRequest(data));
    },
    detailTimeSheetAdmin: (data) => {
      dispatch(actions.getTimeSheetAdminDetailRequest(data));
    },
    defaultState: () => {
      dispatch(actions.actionDefault());
    },
   
    exportFileXlsx:(data)=>{
      dispatch(actions.exportTimeSheetAdminRequest(data))
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TimeSheetAdminPage);
