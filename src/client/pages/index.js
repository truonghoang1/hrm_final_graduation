//authenication
import ForgotPassword from "./authentication/forgot_password/ForgotPassword";
import Login from "./authentication/login/Login";
import Register from "./authentication/register/Register";
import ErrorPage from "./authentication/error_page/Error";
import ActiveAccount from "./authentication/register/ActiveAccount";
import ResetPassword from "./authentication/forgot_password/ResetPassword"

//employee
import EmployeeList from "../pages/main_menu/employee/List"
// salary
import SalaryList from "../pages/main_menu/salary/List"
import Bonus from "../pages/main_menu/salary/Bonus"
//department
import DepartmentList from "../pages/main_menu/department/List"
//role
import RoleList from "../pages/main_menu/role/List"

//tỉmesheet
import TimeSheetAdmin from "./main_menu/timesheet/TimeSheetAdmin";
//profile
import ProfileUser from "./profile_user/ProfileUser";
import RegisterAdmin from "./authentication/registerAdmin/RegisterAdmin";
import Account from "./main_menu/account/List"
import TimeSheetEmployee from "./areaEmployee/TimeSheet"
import LeaveApplication from "./areaEmployee/areaLeaveApp/List"
export {
  Login,ActiveAccount,
  Register,RegisterAdmin,
  ForgotPassword,ResetPassword,
  ErrorPage,
   EmployeeList,
    DepartmentList,
  SalaryList,
  RoleList,
  ProfileUser,Account,Bonus,TimeSheetAdmin,TimeSheetEmployee,LeaveApplication
};
