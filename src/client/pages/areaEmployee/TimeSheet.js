import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import Table from "../../components/common/Table";
import FormFormik from "../../components/common/Form";
import { Spin, Tag, Modal, Button } from "antd";
import Detail from "../../components/common/Detail";
import { EditFilled, DeleteFilled, ContainerFilled, } from "@ant-design/icons";
import * as time from "../../ultis/dateTime";
import * as actions from "../../actions/timesheet";
import {getDepartmentRequest} from "../../actions/department"
import * as yup from "yup";
import { timeSheetInput,timeSheetObject } from "../../constants/item_form";
import { useLocation, Link } from "react-router-dom";
import queryString from "query-string"
import moment from "moment"
const TimeSheet = (props) => {
  const [timeSheet, setEmployes] = useState([]);
  const [show, setShow] = useState(false);
  const [edit, setEdit] = useState(false);
  const [del, setDel] = useState({
    show: false,
    id: "",
    name:""
  });
  
  const [dataEdit, setDataEdit] = useState(timeSheetObject);
  const [dataDetail,setDataDetail] = useState({})
  const location = useLocation()
  const [detail, setDetail] = useState(false);
  const ref = React.useRef(null);
  const propsDatas = React.useMemo(()=>{
    return{
      fields: timeSheetInput,
      onDispatch: (data) => {
        if (edit === true) {
          props.updateTimeSheetEmployee(data);
        } else {
          props.addTimeSheetEmployee(data);
        }
      },
      
      onCancel: () => {
        setEdit(false);
        setShow(!show);
      },
      edit,
    };
  },[])
  
  const formik = {
    initialValues: !edit ? timeSheetObject : dataEdit,
    validationSchema: yup.object({
      
    }),
  };

  const columns = [
    {
        title: "Ngày",
        dataIndex: "date_log",
        align: "center",
        width: 100,fixed: "left",
      },
    {
      title: "Thời gian",
      dataIndex: "time_log",
      align: "center",
      width: 100,
      fixed: "left",
    },
    
    {
      title: "Muộn",
      dataIndex: "late",
      align: "center",
     
      width: 50,
      render: (value, record, index) => {
        return (
          <p
          >
           { moment(record.time_log,"hh:mm").isBefore(moment('09:00',"hh:mm"))==true?"đúng giờ":
          (moment(record.time_log,"hh:mm").isBefore(moment('13:30',"hh:mm"))==true&&moment(record.time_log,"hh:mm").isAfter(moment('09:00',"hh:mm"))==true?"muộn nửa ngày":"muộn")}
          </p>
        );
      },
    },
    { title: "Ngày công", dataIndex: "rol", align: "center", width: 50 , },
    
  ];
  //mounting
  useEffect(() => {
   let query = queryString.parse(location.search)
   let page = Number.parseInt(query.page)
   let limit =Number.parseInt(query.limit)
  if(!location.search){
       props.getListTimeSheetEmployee({page:1,limit:10})
  }else{
    props.getListTimeSheetEmployee({page,limit})
  }
  }, []);
  //updating
  useEffect(() => {
    //data change
    if (!props.timesheetState.data?.length < 1) {
      let data = props.timesheetState.data?.map((item, index) => {
        
        return {
          ...item,
          key:index+1,
          createdAt: time.formatDateTime(item.createdAt),
          updatedAt: time.formatDateTime(item.updatedAt),
        };
      });
      setEmployes(data);
    }else{
      setEmployes([])
    }
    
    
  }, [ props.timesheetState.data]);

  return (
    <div>
      {props.timesheetState.isLoading === true ? (
        <Spin>
          <Table />
        </Spin>
      ) : (
        <>
             <Button onClick={()=>{props.addTimeSheetEmployee()}}>Log time</Button>
          <Table
          
            columns={columns}
            data={timeSheet}
            totalPage={props.timesheetState.totalPage}
            pageIndex={props.timesheetState.page}
            onImportFile={props.importFileXLSX}
            onExportFile={props.exportFileXlsx}
            dataFilterExport={timeSheetObject}
            onPagination={props.getListTimeSheetEmployee}
          />
 
          <Modal
          centered
            open={del.show}
            onOk={() => {
              props.deleteTimeSheetEmployee({ id: del.id });
              setDel({ show: false, id: "" });
            }}
            onCancel={() => {
              setDel({ ...del, show: false });
            }}
          >
            <p>Delete {del.name}?</p>
          </Modal>
          <Detail
            open={detail}
            onCancel={(state) => {
              setDetail(state);
              props.defaultState();
            }}
            onOpen={(state) => setDetail(state)}
            data={dataDetail}
            nameDetail={"timeSheet"}
          />

          <FormFormik
            propsDatas={propsDatas}
            formik={formik}
            showForm={(status) => {
              setShow(status);
            }}
            show={show}
          />
        </>
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    timesheetState: state.timesheetState,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getListTimeSheetEmployee: (data) => {
      dispatch(actions.getTimeSheetEmployeeRequest(data));
    },
    addTimeSheetEmployee: (data) => {
      console.log(data);
      dispatch(actions.addTimeSheetEmployeeRequest(data));
    },
    defaultState: () => {
      dispatch(actions.actionDefault());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TimeSheet);
