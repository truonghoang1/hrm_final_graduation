import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import Table from "../../../components/common/Table";
import FormFormik from "../../../components/common/Form";
import { Spin, Modal, Button ,Tag} from "antd";
import Detail from "../../../components/common/Detail";
import * as time from "../../../ultis/dateTime";
import * as actions from "../../../actions/timesheet";
import * as yup from "yup";
import {leaveAppInput,leaveAppObject} from "../../../constants/item_form"
import { useLocation, Link } from "react-router-dom";
import queryString from "query-string"
import moment from "moment";

const List = (props) => {
  const [timeSheet, setEmployes] = useState([]);
  const [show, setShow] = useState(false);
  const [edit, setEdit] = useState(false);
  const [del, setDel] = useState({
    show: false,
    id: "",
    name:""
  });
  
  const location = useLocation()
  const [detail, setDetail] = useState(false);
  const propsDatas = React.useMemo(()=>{
    return{
      fields: leaveAppInput,
      onDispatch: (data) => {
          props.addLeaveAppEmployee(data);
      },
      
      onCancel: () => {
        setEdit(false);
        setShow(!show);
      },
      edit,
    };
  },[])
  
  const formik = {
    initialValues:  leaveAppObject ,
    validationSchema: yup.object({
      name: yup.string().required(),
      type:yup.string().required(),
      date:yup.string().required(),
      rol:yup.string().required(),
      description:yup.string().required()
    }),
  };

  const columns = [
    {
        title: "Tên giấy nghỉ",
        dataIndex: "name",
        align: "center",
        width: 100,fixed: "left",
      },
    {
      title: "Thời gian",
      dataIndex: "date",
      align: "center",
      width: 100,
      fixed: "left",
    },
    
    {
      title: "Chờ duyệt",
      dataIndex: "complete",
      align: "center",
     
      width: 50,
      render: (value, record, index) => {
        return (
          <div >
          {record?.complete==true?<Tag color="green">Đã duyệt</Tag>: <Tag color="magenta">Chưa duyệt</Tag>}
          </div>
        );
      },
    },
    { title: "Ngày công", dataIndex: "workdate", align: "center", width: 50 ,},
    
  ];
  //mounting
  useEffect(() => {
   let query = queryString.parse(location.search)
   let page = Number.parseInt(query.page)
   let limit =Number.parseInt(query.limit)
  if(!location.search){
       props.getListLeaveAppEmployee({page:1,limit:10})
  }else{
    props.getListTimeSheetEmployee({page,limit})
  }
  }, []);
  //updating
  useEffect(() => {
    //data change
    if (!props.timesheetState.data?.length < 1) {
      let data = props.timesheetState.data?.map((item, index) => {
        
        return {
          ...item,
          key:index+1,
          createdAt: time.formatDateTime(item.createdAt),
          updatedAt: time.formatDateTime(item.updatedAt),
        };
      });
      setEmployes(data);
    }else{
      setEmployes([])
    }
    
    
  }, [ props.timesheetState.data]);

  return (
    <div>
      {props.timesheetState.isLoading === true ? (
        <Spin>
          <Table />
        </Spin>
      ) : (
        <>
            <Button onClick={()=>{setShow(!show)}}>Thêm giấy nghỉ phép</Button>
          <Table
          
            columns={columns}
            data={timeSheet}
            totalPage={props.timesheetState.totalPage}
            pageIndex={props.timesheetState.page}
          />
 
          <Modal
          centered
            open={del.show}
            onOk={() => {
              props.deleteTimeSheetEmployee({ id: del.id });
              setDel({ show: false, id: "" });
            }}
            onCancel={() => {
              setDel({ ...del, show: false });
            }}
          >
            <p>Delete {del.name}?</p>
          </Modal>
          <FormFormik
            propsDatas={propsDatas}
            formik={formik}
            showForm={(status) => {
              setShow(status);
            }}
            show={show}
          />
        </>
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    timesheetState: state.timesheetState,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getListLeaveAppEmployee: (data) => {
      dispatch(actions.getListLeaveApplicationRequest(data));
    },
    addLeaveAppEmployee: (data) => {
      console.log(data);
      dispatch(actions.addLeaveApplicationRequest(data));
    },
    defaultState: () => {
      dispatch(actions.actionDefault());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(List);
