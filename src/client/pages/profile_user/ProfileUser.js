import React from "react";
import { connect } from "react-redux";
import {
  Col,
  Row,
  Card,
  Image,
  Button,
  Tabs,Input
} from "antd";
import * as yup from "yup"
import logo from "../../../public/assets/images/background.jpg";
import {CheckOutlined} from "@ant-design/icons"
import s from "./profile.module.scss";
import * as actions from "../../actions/user"
import Form from "../../components/common/Form"
export const ProfileUser = (props) => {
  const [pw, setPw] = React.useState({
    password:"",
    newP:""
  });
  const [show,setShow] =React.useState(false)
 const employeeField = [
  { type: "text", keyName: "full_name", label: "Họ và Tên ", required: true },
    { type: "date", keyName: "dob", label: "Ngày sinh", required: true },
    { type: "text", keyName: "email", label: " Email", required: true,disabled:false },
    { type: "text", keyName: "cmnd", label: "Căn cước công dân", required: true },
    { type: "text", keyName: "phone", label: "Điện thoại", required: true },
    { type: "description", keyName: "present_address", label: "Chỗ ở hiên tại", required: true },
    { type: "description", keyName: "permanent_address", label: "Quê Quán", required: true },
   
   
  ]
  const propsDatas = React.useMemo(()=>{
    return{
      fields: employeeField,
      onDispatch: (data) => {
          props.updateProfile(data);
          // setEdit(false)
          setShow(!show)
      },
      onCancel: () => {
        // setEdit(false);
        setShow(!show);
      },
    };
  },[])
 
  const inforUser = {
    name:"Truong",
    email:"truonghoangdevdhkt@gmail.com",
    dob:"06-05-2000",
    phone:"****3022",
    address:" Vũ Trọng Khánh Ha Đông."

  }
  const formik = {
    initialValues:  {full_name:"",email:"",dob:"",phone:"",present_address:"",permanent_address:"",cmnd:""},
    validationSchema: yup.object({
      full_name: yup.string().required(),
      email: yup.string().required(),
      cmnd: yup.string().required(),dob:yup.string().required(),
      phone:yup.string().required(),present_address:yup.string().required(),permanent_address:yup.string().required()
    }),
  };

  return (
    <div className={s.wrap_all}>
     <Form
            propsDatas={propsDatas}
            formik={formik}
            showForm={(status) => {
              setShow(status);
            }}
            show={show}
          />
      <Card
        title={
          <div style={{display:'flex'}}>
            <Image style={{ width: "150px", height: "150px",borderRadius:"50%" }} src={logo} />
            <div style={{display:'flex',flexDirection:"column",justifyContent:"center",paddingLeft:"10px"}}>
                <h4>Trường</h4>
                <h6>Team Players Platform</h6>
                <p>Vũ Trọng Khánh, Hà Đông</p>
            </div>
          </div>
        }
        extra={<Button  type="primary"><CheckOutlined /></Button>}
      >
        <Tabs defaultActiveKey="1">
          <Tabs.TabPane tab={<span>Profile</span>} key="1">
            <Row>
              <Col
                xxl={{ span: 16, offset: 1 }}
                xl={{ span: 14, offset: 1 }}
                md={{ span: 20 }}
                sm={{ span: 20 }}
              >
                <Card
                  title="Personal Details"
                  extra={<Button type="primary" onClick={()=>{setShow(!show)}}>Edit</Button>}
                  className={s.cart_left}
                >
                  <Row>
                    <Col span={20} offset={5}>
                     <h4>Name:{inforUser.name}</h4>
                     <h4>Email:{inforUser.email}</h4>
                     <h4>Date of Birth:{inforUser.dob}</h4>
                     <h4>Phone:{inforUser.phone}</h4>
                     <h4>Address:{inforUser.address}</h4>
                    </Col>
                  </Row>
                </Card>
              </Col>
              <Col
                xxl={{ span: 6, offset: 1 }}
                xl={{ span: 6, offset: 1 }}
                md={{ span: 20 }}
                sm={{ span: 20 }}
              >
                <Card
                  title="Account Status"
                  extra={<Button type="primary"><CheckOutlined /></Button>}
                  className={s.cart_right_top}
                >
                    <Button style={{backgroundColor:"#4caf50",color:"#ffffff"}}>Active</Button>
                </Card>
                <Card
                  title="Skills"
                  className={s.cart_right_bottom}
                  extra={<Button type="primary"><CheckOutlined /></Button>}
                >
                  {/* <Button>html</Button> <Button>css</Button> <Button>reactjs</Button> <Button>js</Button> <Button>antd</Button> <Button>material-ui</Button> */}
                </Card>
              </Col>
            </Row>
          </Tabs.TabPane>
          <Tabs.TabPane tab={<span>Password</span>} key="2">
          <Row style={{margin:"10px"}}>
            <Input placeholder="mật khẩu cũ"
               style={{
                  width: 'calc(50% - 200px)',
                }}
                onChange={(e)=>{
                  setPw({...pw,password:e.target.value})
                }}
       
                />
            </Row>
            <Row style={{margin:"10px"}}>
            <Input placeholder="mật khẩu mới"
               style={{
                  width: 'calc(50% - 200px)',
                }}

                onChange={(e)=>{
                  setPw({...pw,newP:e.target.value})
                }}
       
                />
            </Row>
            <Row style={{margin:"10px"}}>

              <Col span={15}>
               <Button type="primary" onClick={(e)=>{
                if(!pw.password&& !pw.newP){
                 e.defaultPrevented()
                }else{
                  props.changeP(pw)
                }
               }}>Submit</Button>
   
              </Col>
            </Row>
           
          </Tabs.TabPane>
        </Tabs>
      </Card>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    changeP: (data) => {
     dispatch(actions.changePasswordRequest(data))
    },
    updateProfile:(data)=>{
      dispatch(actions.updateProfileRequest(data))
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileUser);
