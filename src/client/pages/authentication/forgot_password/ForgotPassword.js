import React from "react";
import { connect } from "react-redux";
import * as actions from "../../../actions/user";
import s from "./forgotPassword.module.scss";
import { DebounceInput } from "react-debounce-input";
import { Form, Button,Row,Col } from "antd";
import { Link } from "react-router-dom";
import logo from "../../../../public/assets/images/logo.png";
import Swal from "sweetalert2";
const ForgotPassword = (props) => {
  const [account, setAccount] = React.useState({
    email: "",

  });
  const [count,setCount]=React.useState({
    count:0,
    verifyEmail:''
  })

  const handleSetState = (e) => {
    setAccount({ ...account, [e.target.name]: e.target.value });
  };
 
  return (
    <div className={s.wrap_fgPass}>
      <div className={s.container_fgPass}>
      <div className={s.wrap_form_left}>
       <h3 style={{fontSize:"30px",color:'#fff'}}>HRM SYSTEM</h3>
      </div>
      <Form className={s.wrap_form_right}>
        <h3>Forgot Password?</h3>
        <h5>Enter your email to get a password reset link</h5>
        <DebounceInput
          className={s.input}
          debounceTimeout={500}
          name="email"
          onChange={handleSetState}
          placeholder="Email"
        />

        <div className={s.wrapBtn}>
          {" "}
          <Button
            className={s.button}
            onClick={(e) => {
               if(account.email==count.verifyEmail){
                if(count.count>5){
                  Swal.fire({text:"Vượt quá 5 lần",icon:'warning'})
                }else{
                    if(count.count==1){
                      setCount({verifyEmail:account.email,count:count.count+1})
                    }else{
                      props.forgotPassword(account);
                      setCount({...count,count:count.count+1})
                    
                   }
                }
               }
               else{
                setCount({count:0,verifyEmail:account.email})
               }
             
            }}
          >
           Summit
          </Button>
        </div>

        <Row>
          <Col span={18} offset={6}>
          Remember your password? <Link className={s.btn_fgPassword} to={"/login"}>Login</Link>
          </Col>
        </Row>
      </Form>
    </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
   
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    forgotPassword: (data) => {
      dispatch(actions.forgotPasswordRequest(data));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);
