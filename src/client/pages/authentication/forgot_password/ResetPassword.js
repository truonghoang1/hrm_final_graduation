import React from 'react'
import { connect } from 'react-redux'
import { DebounceInput } from "react-debounce-input";
import { Form, Button,Row,Col } from "antd";
import { Link,useParams } from "react-router-dom";
import logo from "../../../../public/assets/images/logo.png";
import *as actions from "../../../actions/user"
import s from "./forgotPassword.module.scss"
export const ResetPassword = (props) => {
    const params = useParams()
    const [account, setAccount] = React.useState({
        password: "",
    
      });
    
      const handleSetState = (e) => {
        setAccount({ ...account, [e.target.name]: e.target.value });
      };
     
  return (
    <div className={s.wrap_fgPass}>
    <div className={s.container_fgPass}>
    <div className={s.wrap_form_left}>
    <h3 style={{fontSize:"30px",color:'#fff'}}>HRM SYSTEM</h3>
    </div>
    <Form className={s.wrap_form_right}>
      <h3>Reset Password</h3>
      <h5>Enter your password</h5>
      <DebounceInput
        className={s.input}
        debounceTimeout={500}
        name="password"
        onChange={handleSetState}
        placeholder="Password"
      />

      <div className={s.wrapBtn}>
        {" "}
        <Button
          className={s.button}
          onClick={() => {
            console.log(params)
            props.resetPw({password:account.password,token:params.id});
          }}
        >
         Summit
        </Button>
      </div>

      <Row>
        <Col span={18} offset={6}>
        Remember your password? <Link className={s.btn_fgPassword} to={"/login"}>Login</Link>
        </Col>
      </Row>
    </Form>
  </div>
  </div>
  )
}

const mapStateToProps = (state) => ({})

const mapDispatchToProps = (dispatch)=>{
    return{
        resetPw:(data)=>{
            dispatch(actions.resetPasswordRequest(data))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword)