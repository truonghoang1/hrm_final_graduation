import React from "react";
import { connect } from "react-redux";
import * as actions from "../../../actions/user";
import { DebounceInput } from "react-debounce-input";
import { Form, Button,Divider,Row,Col } from "antd";
import { useHistory, Link } from "react-router-dom";
import Cookies from "js-cookie";
import Swal from "sweetalert2";
import {EyeOutlined,EyeInvisibleOutlined} from "@ant-design/icons"
import s from "./login.module.scss";
import logo from "../../../../public/assets/images/logo.png";

const Login = (props) => {
  const [account, setAccount] = React.useState({
    email: "",
    password: "",
  });
  const [show, setShow] = React.useState(false);
  const history = useHistory();

  const handleSetState = (e) => {
    setAccount({ ...account, [e.target.name]: e.target.value });
  };
  React.useEffect(() => {
    console.log(history);
    const role = Number.parseInt(Cookies.get("role"));
    props.token !== null &&
      (role <=2
       
       ? window.location.href="/"
        : window.location.href="/user/timesheet")
        
  
  }, [props.token]);
const buttonRef = React.useRef(null)

  console.log(account);
  return (
    <div className={s.wrap_login}>
      <div className={s.container_login}>
      <div className={s.wrap_form_left} >
          <h3 style={{ fontSize: "50px", color: "#fff" }}>HRM</h3>
          <h3 style={{ fontSize: "50px", color: "#fff" }}>SYSTEM</h3>
      </div>
      <Form className={s.wrap_form_right}>
        <h3>Login</h3>
        <h5>Access to HRM system</h5>
        <DebounceInput
          className={s.input}
          debounceTimeout={500}
          name="email"
          onChange={handleSetState}
          placeholder="Email"
        />
        <div className={s.inputPw}>
          <DebounceInput
            className={s.inputItem}
            type={show ? "text" : "password"}
            debounceTimeout={500}
            name="password"
            onChange={handleSetState}
            placeholder="Password"
            onKeyDown={(e)=>{
              if(e.key=="Enter"){
                buttonRef.current.focus()
                
              }
              // else{
              //   e.preventDefault()
              // }
            }}
          
          />
          {show ? <EyeOutlined className={s.show} onClick={()=>setShow(!show)}/>:<EyeInvisibleOutlined className={s.show} onClick={()=>setShow(!show)} />}
        </div>
        {/* {account.password&&verifyPassword(account.password).isvalid===false&& <span style={{color:"red"}}>{verifyPassword(account.password).err}</span>} */}
        <div className={s.wrapBtn}>
          {" "}
          <Button
             ref={buttonRef}
            className={s.button}
            onClick={() => {
             setTimeout(()=>{
              props.signIn(account);
             },500)
            }}
          >
            Login
          </Button>
          <Link className={s.btn_fgPassword} to={"/forgot-password"}>Forgot Password?</Link>
        </div>
        {/* <Divider orientation="center" plain style={{fontSize:"20px",fontWeight:600,color:"#a0a0a0"}}>Or</Divider>
        <Row>
          <Col span={18} offset={6}>
          Don’t have an account?  <Link className={s.btn_fgPassword} to={"/register"}>Register</Link>
          </Col>
        </Row> */}
      </Form>
    </div>
    </div>
  );
};

const verifyPassword =(password)=>{
  const regexPw = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W)/
   let isvalid= true
   if(password.length<8){
       return {isvalid:false,err:"password length is too short"}
   }
   else if(!regexPw.test(password)){
       return{isvalid:false,err:"password contains at least [a-z][A-Z][0-9][/W]"} 
   }else{
     return isvalid =true
   }
 }
const mapStateToProps = (state) => {
  return {
    token: state.userState.token,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    signIn: (data) => {
      dispatch(actions.signInRequest(data));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
