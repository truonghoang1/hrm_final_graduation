import React from 'react'
import { connect } from 'react-redux'
import {Link,useParams} from "react-router-dom"
import {CheckOutlined,ExclamationCircleOutlined} from "@ant-design/icons"
import logo from "../../../../public/assets/images/logo.png";
import *as actions from "../../../actions/user"
import s from "./register.module.scss"
export const ActiveAccount = (props) => {
    const params = useParams()
    React.useEffect(()=>{
          console.log(params);
          props.activationAcc(params)
    },[])
  return (
   
    <div className={s.wrap_register}>
      <div className={s.container_register}>
        <div className={s.wrap_form_left}>
        <h3 style={{fontSize:"30px",color:'#fff'}}>HRM SYSTEM</h3>
        </div>
         <div className={s.wrap_form_right} >
  {props.actived===400?<div className={s.containerActiveForm}>
  <ExclamationCircleOutlined style={{color:"#ff5722",fontSize:"50px"}}/>
  <h3>Token expired</h3>
   <Link to={`/register`}>Back to Register</Link>
   </div>: <div className={s.containerActiveForm}>
   <CheckOutlined className={s.iconCheck}/>
   <h3>Active Account Success</h3>
   <Link to={`/login`}>Back to Login</Link>
   </div>}
 </div>
        
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
    return{
        actived :state.userState.active
    }
}

const mapDispatchToProps = (dispatch)=>{
    return{
        activationAcc :(data)=>{
            dispatch(actions.activationAccountRequest(data))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ActiveAccount)