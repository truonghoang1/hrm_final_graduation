import React from "react";
import { connect } from "react-redux";
import { Form, Divider, Button, Row, Col,Select } from "antd";
import { Link } from "react-router-dom";
import { DebounceInput } from "react-debounce-input";
import * as actions from "../../../actions/user";
import {getRolesRequest} from "../../../actions/roles"
import { EyeOutlined, EyeInvisibleOutlined } from "@ant-design/icons";
import Cookies from "js-cookie";
import logo from "../../../../public/assets/images/logo.png";
import s from "./register.module.scss";

export const Register = (props) => {
  const [showPw, setShowPw] = React.useState(false);
  const [showRePw, setRePw] = React.useState(false);
  const [data, setData] = React.useState({});
  const handleChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value,
    });
  };
  const isAdmin =Cookies.get("role")
  React.useEffect(()=>{
       (isAdmin<2 ||isAdmin!==undefined)&& props.getRole({page:1,limit:10})

  },[])
 
  // console.log("🚀 ~ file: Register.js ~ line 24 ~ Register ~ isAdmin", isAdmin)
  console.log(data);
  return (
    <div className={s.wrap_register}>
      <div className={s.container_register}>
        <div className={s.wrap_form_left}>
          {/* <img src={logo} alt="logo" /> */}
          <h3 style={{ fontSize: "50px", color: "#fff" }}>HRM</h3>
          <h3 style={{ fontSize: "50px", color: "#fff" }}>SYSTEM</h3>
        </div>
        <Form className={s.wrap_form_right}>
          <h3>Register</h3>
          <h5>Access to HRM system</h5>
         
          <DebounceInput
            className={s.input}
            debounceTimeout={500}
            name="email"
            onChange={handleChange}
            placeholder="Email"
          />
          {data.hasOwnProperty("email")&&!checkEmail(data.email)&& <span style={{color:"red",marginLeft:'10px'}}>Email invalid</span>}
          
          <div className={s.inputPw}>
            <DebounceInput
              className={s.inputItem}
              type={showPw ? "text" : "password"}
              debounceTimeout={500}
              name="password"
              onChange={handleChange}
              placeholder="Password"
            />
            {showPw ? (
              <EyeOutlined className={s.show} onClick={() => setShowPw(!showPw)} />
            ) : (
              <EyeInvisibleOutlined
                className={s.show}
                onClick={() => setShowPw(!showPw)}
              />
            )}
          </div>
          {data.password&&verifyPassword(data.password).isvalid===false&& <span style={{color:"red"}}>{verifyPassword(data.password).err}</span>}
          <div className={s.inputPw}>
            <DebounceInput
              className={s.inputItem}
              type={showRePw ? "text" : "password"}
              debounceTimeout={500}
              name="re_password"
              onChange={handleChange}
              placeholder="Confirm Password"
            />
            {showRePw ? (
              <EyeOutlined className={s.show} onClick={() => setRePw(!showRePw)} />
            ) : (
              <EyeInvisibleOutlined
                className={s.show}
                onClick={() => setRePw(!showRePw)}
              />
            )}
          </div>
          {
            !comparepw(data.password,data.re_password)&& <span style={{color:"red",marginLeft:'10px'}}>Password not match</span>}
          
          <div className={s.wrapBtn}>
           
            <Button
              className={s.button}
              onClick={() => {
                props.signup(data);
              }}
            >
              Register
            </Button>
      
          </div>
          <Divider
            orientation="center"
            plain
            style={{ fontSize: "20px", fontWeight: 600, color: "#a0a0a0" }}
          >
            Or
          </Divider>
          <Row>
          
            <Col span={18} offset={6}>
              Already have an account?{" "}
              <Link className={s.btn_fgPassword} to={"/login"}>
                Login
              </Link>
            </Col>
           {isAdmin!==undefined&& <Col span={18} offset={6}>
              Go back Page?{" "}
              <Link className={s.btn_fgPassword} to={"/"}>
                GoHome
              </Link>
            </Col>}
          </Row>
        </Form>
      </div>
    </div>
  );
};

const checkEmail =function(state){
  const testEmail =
  /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/i;
  if(testEmail.test(state)){
    return true
  }else{
    return false
  }
}
const comparepw =(pw,repw)=>{
  if(pw&&pw!==repw&&repw){
    
    return false
  }else{
    return true
  }
}
const verifyPassword =(password)=>{
 const regexPw = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W)/
  let isvalid= true
  if(password.length<8){
      return {isvalid:false,err:"password length is too short"}
  }
  else if(!regexPw.test(password)){
      return{isvalid:false,err:"password contains at least [a-z][A-Z][0-9][/W]"} 
  }else{
    return isvalid =true
  }
}
const mapStateToProps = (state) => {
  return {
    role:state.roleState.data
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getRole:(data)=>{
      dispatch(getRolesRequest(data))
    },
    signup: (data) => {
      dispatch(actions.signupGuestRequest(data));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
