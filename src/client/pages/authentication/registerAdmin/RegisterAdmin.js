import React from "react";
import { connect } from "react-redux";
import { Form, Divider, Button, Row, Col,Select } from "antd";
import { Link } from "react-router-dom";
import { DebounceInput } from "react-debounce-input";
import * as actions from "../../../actions/user";
import {getRolesRequest} from "../../../actions/roles"
import { EyeOutlined, EyeInvisibleOutlined } from "@ant-design/icons";
import Cookies from "js-cookie";
import logo from "../../../../public/assets/images/logo.png";
import s from "./registerAdmin.module.scss";


export const RegisterAdmin = (props) => {
  const [showPw, setShowPw] = React.useState(false);
  const [showRePw, setRePw] = React.useState(false);
  const [data, setData] = React.useState({});
  const handleChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value,
    });
  };
  const isAdmin =Cookies.get("role")
  React.useEffect(()=>{
       (isAdmin<2 ||isAdmin!==undefined)&& props.getRole({page:1,limit:10})
  },[])
  console.log(data);
  return (
    <div className={s.wrap_registerAdmin}>
      <div className={s.container_registerAdmin}>
        <div className={s.wrap_form_left}>
          {/* <img src={logo} alt="logo" /> */}
        </div>
        <Form className={s.wrap_form_right}>
          <h3>Sign Up</h3>
         
          <DebounceInput
            className={s.input}
            debounceTimeout={500}
            name="email"
            onChange={handleChange}
            placeholder="Email"
          />
          {/* {data.hasOwnProperty("email")&&!checkEmail(data.email)&& <span style={{color:"red",marginLeft:'10px'}}>Email invalid</span>} */}
          {(Number.parseInt(isAdmin)>2 || isAdmin===undefined) ?"":<Select placeholder="roles" className={s.select}  onChange={(value,option)=>{
            setData({
              ...data,role:value
            })
          }}>
            {props.role!==null && props.role.map((role,index)=>{
              
                return<Select.Option key={index} value={role._id}  >{role.name}</Select.Option>
               
            })}
          </Select>
          }
          <div className={s.inputPw}>
            <DebounceInput
              className={s.inputItem}
              type={showPw ? "text" : "password"}
              debounceTimeout={500}
              name="password"
              onChange={handleChange}
              placeholder="Password"
            />
            {showPw ? (
              <EyeOutlined className={s.show} onClick={() => setShowPw(!showPw)} />
            ) : (
              <EyeInvisibleOutlined
                className={s.show}
                onClick={() => setShowPw(!showPw)}
              />
            )}
          </div>
          <div className={s.inputPw}>
            <DebounceInput
              className={s.inputItem}
              type={showRePw ? "text" : "password"}
              debounceTimeout={500}
              name="re_password"
              onChange={handleChange}
              placeholder="Confirm Password"
            />
            {showRePw ? (
              <EyeOutlined className={s.show} onClick={() => setRePw(!showRePw)} />
            ) : (
              <EyeInvisibleOutlined
                className={s.show}
                onClick={() => setRePw(!showRePw)}
              />
            )}
          </div>
          {
            !comparepw(data.password,data.re_password)&& <span style={{color:"red",marginLeft:'10px'}}>Password not match</span>}
          
          <div className={s.wrapBtn}>
            <Button
              className={s.button}
              onClick={() => {
                props.signup(data); 
              }}
            >
              Register
            </Button>
          </div>
        </Form>
      </div>
    </div>
  );
};

const checkEmail =function(state){
  const testEmail =
  /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/i;
  if(testEmail.test(state)){
    return true
  }else{
    return false
  }
}
const comparepw =(pw,repw)=>{
  if(pw&&pw!==repw&&repw){
    return false
  }else{
    return true
  }
}
const mapStateToProps = (state) => {
  return {
    role:state.roleState.data
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getRole:(data)=>{
      dispatch(getRolesRequest(data))
    },
    signup: (data) => {
      dispatch(actions.signupRequest(data));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RegisterAdmin);
