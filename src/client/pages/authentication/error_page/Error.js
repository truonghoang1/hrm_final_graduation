import React from 'react'
import {WarningFilled} from "@ant-design/icons"
import {Link} from "react-router-dom"
import s from "./err.module.scss"
function ErrorPage() {
  return (
    <div className={s.wrap_err_page}>
 
   <h1>404</h1>
    <h3><WarningFilled /> Oops! Page not found!</h3>
    <p>The page you requested was not found.</p>
    <Link to={"/"}>Back to Home</Link>
   
    </div>
  )
}

export default ErrorPage