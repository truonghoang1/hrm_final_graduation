import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import store  from './store' 
import App from "./App";
import { createBrowserHistory } from 'history'
import 'antd/dist/antd.css';
 const history = createBrowserHistory()
ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter history={history} >
      <App />
    </BrowserRouter>
   </Provider>
 , document.getElementById("root")
);
