import React from 'react'
import {Breadcrumb,Button} from "antd"
import {useLocation} from 'react-router-dom'
import s from "./shared.module.scss"
function Breadcrum() {
  const location = useLocation()
  let path =location.pathname.split("/")
  console.log(path);
  let BreadCrumItem =location.pathname.includes("/edit") ||location.pathname.includes("/reset") || location.pathname.includes("/active")?path.slice(0,path.length-1): path
  // console.log(BreadCrumItem,location.pathname);
  return (
    <div className={s.wrapBreadcrumb}>
   <div className={s.titlebreak} >
           Welcome Admin
    </div>
    <Breadcrumb className={s.breadcrum}>
    <Breadcrumb.Item>Home</Breadcrumb.Item>
    {
       BreadCrumItem.map((item,index)=>{
        
          return ( <Breadcrumb.Item key={index+1}>{item}</Breadcrumb.Item>)
        
     })
     
      }
 </Breadcrumb>

    </div>
  )
}

export default Breadcrum