import React from "react";
import {
  BarsOutlined,
  SearchOutlined,
  BellOutlined,
  DownOutlined,
  UpOutlined,
  AlipayCircleOutlined,
} from "@ant-design/icons";
import {
  collapsed,
  showModel,
  showProfile,
} from "../../actions/sidebar";
import { DebounceInput } from "react-debounce-input";
import { Avatar, Badge, Row, Col, Popover } from "antd";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import ImageUser from "../../../public/assets/images/avatar.png";
import logo from "../../../public/assets/images/logo2.png"
import s from "./shared.module.scss";
import Cookies from "js-cookie";


function Header(props) {

  const text = <div>
    <Avatar src={ImageUser} />
    <span>Truong hoang</span>
  </div>;
  const content = (

    <div className={s.popoverProfile}>

      <Link to={"/profile"}>Profile</Link>
      <Link to={"#"}
        onClick={() => {
          Cookies.remove("SESSION_ID");
          Cookies.remove("role");
          window.location.reload();
        }}
      >
        Đăng xuất
      </Link>
    </div>

  );

  return (
    <Row className={s.wrapHeader}>
      <Col className={!props.collapsed ? s.logo : s.hidden_logo}>
        {!props.collapsed ? (
          <div className={s.brand}>
            {/* <AlipayCircleOutlined
              onClick={() => {
                props.toggle();
              }}
            /> */}
            <img src={logo} alt="logo" width={40} height={40}/>
            <p>
              <span>HRM</span>
            </p>
          </div>
        ) : (
          <img className={s.img_logo} src={logo} alt="logo" width={40} height={40}/>
        )}
      </Col>
      
     
      <Col
        className={s.headerLeft}
        xs={{ span: 15 }}
        sm={{ span: 15 }}
        md={{ span: 13 }}
        lg={!props.collapsed ? { span: 12 } : { span: 13 }}
        xl={!props.collapsed ? { span: 12 } : { span: 13 }}
        xxl={!props.collapsed ? { span: 12 } : { span: 13 }}
      >
        <BarsOutlined
          onClick={() => {
            // props.toggle()
            window.innerWidth < 992 ? props.showModal() : props.toggle();
          }}
        />
        <div className={s.search}>
          {/* <DebounceInput debounceTimeout={500} placeholder="Search hear" disabled
          />
          <SearchOutlined /> */}
        </div>
      </Col>
      <Col
        className={s.headerRight}
        xs={{ span: 7, offset: 1 }}
        sm={{ span: 4, offset: 2 }}
        md={{ span: 5, offset: 5 }}
        lg={!props.collapsed ? { span: 4, offset: 3 } : { span: 4, offset: 4 }}
        xl={!props.collapsed ? { span: 3, offset: 5 } : { span: 3, offset: 6 }}
        xxl={!props.collapsed ? { span: 3, offset: 6 } : { span: 3, offset: 6 }}
      >
        {/* <Badge className={s.badge} count={5}>
          <BellOutlined />
        </Badge> */}

        <Popover
          placement="bottom"
          title={text}
          content={content}
          trigger="click"

        >
          <div className={s.profile} onClick={() => props.showProfile()}>
            <Avatar src={ImageUser} />
            {!props.profile ? <DownOutlined /> : <UpOutlined />}
          </div>
        </Popover>
      </Col>
    </Row>
  );
}

const mapStateToProps = (state) => {
  return {
    collapsed: state.sideBarState.collapsed,
    profile: state.sideBarState.showProfile,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    toggle: () => {
      dispatch(collapsed());
    },
    showModal: () => {
      dispatch(showModel());
    },
    showProfile: () => {
      dispatch(showProfile());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
