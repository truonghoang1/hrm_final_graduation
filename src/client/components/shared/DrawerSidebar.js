import React ,{ useState }from 'react'
import {Link} from 'react-router-dom'
import item_sidebar from '../../constants/item_sidebar'
import { Menu,Divider,Drawer } from 'antd';
import {connect} from "react-redux"
import s from './shared.module.scss';
import {showModel} from "../../actions/sidebar"

let areaDiv =item_sidebar()
export const ModalSideBar = (props) => {
 
 
  return (
   
       <Drawer
        title="PreSchool"
        placement="left"
        onClose={props.toggle}
        visible={props.modal}
        getContainer={false}
        style={{position:"absolute",height:"100%"}}
         width={250}
        
      >
         <div className={s.areaMenu}>
      {
        areaDiv.map((item,index)=>{
         
          return(
            <div key={index+1} >
            <Divider className={s.divider} orientation="left" plain>
            {item.nameArea}
       </Divider>
           <Menu
           className={s.itemMenu}
            mode="inline"  
      >
        {
          
          item.items.map((menuItem,index)=>{
           
          return(
           ! menuItem.hasOwnProperty('children')? <Menu.Item key={menuItem.key} disabled={menuItem.disabled} icon={menuItem.icon}><Link to={menuItem.nlink}>{menuItem.label}</Link></Menu.Item>:
            <Menu.SubMenu key={menuItem.key} icon={menuItem.icon} title={menuItem.label} >
                {
                  menuItem.children.map((child,index)=>{
                    return <Menu.Item key={child.key} disabled={child.disabled} ><Link to={child.nlink}>{child.label}</Link></Menu.Item>
                  })
                }
                
              </Menu.SubMenu>
          )
          })
        }
      </Menu>
        </div>
          )
          
        })
      }
      </div> 
      </Drawer>
     
     
   
  )
}

const mapStateToProps = (state) => {
    return {
    
      modal:state.sideBarState.modal
    };
  };
  
  const mapDispatchToProps = (dispatch) => {
    return {
      toggle:()=>{
        dispatch(showModel())
      },
      
    };
  };
export default connect(mapStateToProps, mapDispatchToProps)(ModalSideBar)