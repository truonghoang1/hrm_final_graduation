import React, { useState } from "react";
import { Link } from "react-router-dom";
import item_sidebar,{areaUser} from "../../constants/item_sidebar";
import { Menu, Divider } from "antd";
import { connect } from "react-redux";
import s from "./shared.module.scss";
import {  showModel } from "../../actions/sidebar";

function SideBar(props) {
  console.log("width", window.innerWidth);
  let areaDiv =window.location.pathname.includes('/user')? areaUser: item_sidebar()
 
  return (
    <div>
      <nav>
        <div className={s.areaMenu}>
          {areaDiv.map((item, index) => {
            let cutName = item.nameArea.slice(0, 1);
            return (
              <div key={index}>
                {!props.collapsed ? (
                  <Divider className={s.divider} orientation="left" plain>
                    {item.nameArea}
                  </Divider>
                ) : (
                  <Divider className={s.divider} orientation="left" plain>
                    {cutName[0]}
                  </Divider>
                )}
                <Menu
                  className={s.itemMenu}
                  mode="inline"
                  // inlineCollapsed={props.collapsed}
                >
                  {item.items?.map((menuItem, index) => {
                    return !menuItem.hasOwnProperty("children") ? (
                      <Menu.Item
                        key={menuItem.key}
                        disabled={menuItem.disabled}
                        icon={menuItem.icon}
                      >
                        <Link to={menuItem.nlink}>{menuItem.label}</Link>
                      </Menu.Item>
                    ) : (
                      <Menu.SubMenu
                        key={menuItem.key}
                        icon={menuItem.icon}
                        title={menuItem.label}
                      >
                        {menuItem.children.map((child, index) => {
                          return (
                            <Menu.Item
                              key={child.key}
                              disabled={child.disabled}
                            
                            >
                              <Link to={child.nlink} >{child.label}</Link>
                            </Menu.Item>
                          );
                        })}
                      </Menu.SubMenu>
                    );
                  })}
                </Menu>
               
              </div>
            );
          })}
        </div>
      </nav>
      :
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    collapsed: state.sideBarState.collapsed,
    modal: state.sideBarState.modal,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    toggle: () => {
      dispatch(showModel());
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
