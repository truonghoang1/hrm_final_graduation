import React from "react";
import {
  Table,
  Pagination,Row,Input,Col,Modal,Checkbox,InputNumber
} from "antd";

import {FormOutlined,AudioOutlined,DownloadOutlined} from '@ant-design/icons'
import s from './table.module.scss'
import { Link,useHistory,useLocation } from "react-router-dom";
import ImportFile from "../importInput";

function ListCommon(props) {
  const location = useLocation()
  const [page, setPage] = React.useState(10);
  const [modal, setModal] = React.useState(false);
  const [filter, setFilter] = React.useState({opsSkip:[],start: 1,end: 2,
  });
  const {
    columns,
    data,
    pageIndex,
    totalPage,
    onSearch,
    onImportFile,
    dataFilterExport,onExportFile,
    onAdd,onPagination
  } = props;
  const condRender = location.pathname.includes("/employee") || location.pathname.includes("/salary")
    // || location.pathname.includes("salary")
 const condSearch = window.location.pathname.includes('/employee/list') || window.location.pathname.includes("/salary/list")
  const history =useHistory()
 
  console.log(filter);
  return (
    <div className={s.wrap_table}>
     {window.location.pathname.includes('/user')?null: <Row>
        <Col span={12}>
        { condSearch && 
          <Input.Search
            placeholder=" search text or date(yyyy-mm-dd)"
            size="large"
            suffix={
              <AudioOutlined
                style={{
                  fontSize: 16,
                  color: "#1890ff",
                }}
              />
            }
            onPressEnter={(e) => {
              if (e.key === "Enter") {
                onSearch({ keySearch: e.target.value });
              }
            }}
            onSearch={(value) => onSearch({ keySearch: value })}
          />}
        </Col>
        <Col span={1} offset={5}>
         {condRender &&  <ImportFile onSaveFile={onImportFile} />}
        </Col>
       
       { condRender&&
       <Col span={2} offset={2}>
          {" "}
          <Modal
            title="Chose information export "
            open={modal}
            onOk={() => {
              onExportFile(filter)
              setModal(!modal)}}
            onCancel={() => setModal(!modal)}
            okText="Export"
          >
            <Checkbox.Group
              onChange={(checkValue) => { 
                const filterOps =  Object.keys(dataFilterExport?dataFilterExport:{}).filter((item) => {
                  return !checkValue.includes(item);
                }).map((item)=>{ return [item,0]})
                let check =  Object.fromEntries(filterOps)
                setFilter({...filter,opsSkip: check })
              }}
            >
              <Row>
                <p>Total current record :{totalPage * page}</p>
              </Row>
              <Row>
                <Col>
                  <span>start index </span>{" "}
                  <InputNumber
                  required={true}
                    min={1}
                    max={totalPage * page - 1}
                    defaultValue={1}
                    onChange={(value) => {
                      setFilter({ ...filter, start: value });
                    }}
                  />
                </Col>
                <Col offset={1}>
                  {" "}
                  <span>end index </span>{" "}
                  <InputNumber
                  required={true}
                    min={2}
                    max={totalPage * page}
                    onChange={(value) => {
                      setFilter({ ...filter, end: value });
                    }}
                  />
                </Col>
              </Row>
              <Row><span>Choose properties:</span></Row>
              <Row>
                {Object.keys(dataFilterExport?dataFilterExport:{}).map((item, index) => {
                  return (
                    <Col span={7} offset={1} key={index}>
                      {" "}
                      <Checkbox value={item}>{item}</Checkbox>
                    </Col>
                  );
                })}
              </Row>
            </Checkbox.Group>
          </Modal>
          <DownloadOutlined
          title="export file"
            style={{fontSize:35,marginTop:"-1px"}}
            onClick={() => {
              setModal(!modal);
            }}
          />
        </Col>
       }
      {!window.location.pathname.includes('/account')&& <FormOutlined className={s.btn_add} onClick={onAdd}/>}
      </Row>}
     
      <Table
       size="small"
       style={{width:"100%",marginTop:'40px'}}
         scroll={{x:1000,y:'100vh'}}
         columns={columns}
         dataSource={data}
        pagination={false}
       
      />
      <Pagination
        className={s.pagination}
        current={pageIndex||1}
        total={(totalPage||1) * 10 }
        pageSizeOptions={[10, 25, 50, 100]}
        pageSize={page}
        onChange={(page, pageSize) => {
          history.push({pathname:location.pathname,search:`?page=${page}&&limit=${pageSize}`})
          onPagination({ page, limit: pageSize });
        }}
        onShowSizeChange={(current, size) => {

          setPage(size);
        }}
      />
    </div>
  );
}

export default React.memo(ListCommon);
