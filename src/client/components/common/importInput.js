import React from 'react'
import s from "./common.module.scss"
import {CloudUploadOutlined} from "@ant-design/icons"
import Swal from 'sweetalert2'
function ImportFileXlSX(props) {
    const {onSaveFile} =props
    const onChange =(e)=>{
        const file =e.target.files[0]
        const checkFile = file.name.split(".")
        if(checkFile[checkFile.length-1]!=="xlsx"){
            Swal.fire({
                text:`current file ${checkFile[checkFile.length-1]}, Error File Extension`,
                icon:"error"
            })
        }else{
        
            const form = new FormData()
            form.append("file",file)
            onSaveFile(form)  ;
            e.target.value=null
        }
       
    }
  return (
    <div className={s.wrap_file} title={"import file xlsx"}>
        <input type={"file"} onChange={onChange}/>
        <CloudUploadOutlined className={s.icon_upload}/>
    </div>
  )
}

export default ImportFileXlSX