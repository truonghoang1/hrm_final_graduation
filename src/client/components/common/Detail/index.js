import React, { memo } from "react";
import { Modal, Col, Row, Divider } from "antd";
import s from "./detail.module.scss";
function index(props) {
  const { open, onOpen, nameDetail, data ,onCancel } = props;
  const properties = [];
  for (const key in data) {
    if (Object.hasOwnProperty.call(data, key)) {
       if(Array.isArray(data[key])){
          let parseString = data[key].join()
          let  element3 ={[key]:parseString}
          properties.push(element3)
      }
      else if(typeof data[key]==='object'){
    for (const key2 in data[key]) {
    if (Object.hasOwnProperty.call(data[key], key2)) {
        let element2 = {[key2]:data[key][key2]};
        properties.push(element2)  
    }
 }
      }
      else{
        let element = { [key]: data[key] };
      properties.push(element);
      }
    }
  } 
  return (
    <Modal
      title={`Detail ${nameDetail}`}
      centered
      open={open}
      onOk={() => onOpen(false)}
      onCancel={() => onCancel(false)}
      bodyStyle={{height:400,overflow:"scroll"}}
      className={s.wrap_detail}
    >
      {properties.map((item, index) => {
        console.log(Object.keys(item)[0]);
        return (
         <>
         <Row key={index} >
            <Col span={7}>
            {Object.keys(item)[0]}:
            </Col>
            <Col span={15} offset={1}>
            {item[Object.keys(item)[0]]}
            </Col>
          </Row>
         <br/>
         </>
        );
      })}
    </Modal>
  );
}

export default React.memo(index);
