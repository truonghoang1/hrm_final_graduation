import React from "react";
import { Form, Formik } from "formik";
import TagInput from "react-tagsinput";
import { SlackOutlined } from "@ant-design/icons";
import { DatePicker, Select, TimePicker, Drawer, Modal } from "antd";
import { DebounceInput } from "react-debounce-input";
import s from "./form.module.scss";
import * as dateTime from "../../../ultis/dateTime";
import moment from "moment";
import Swal from "sweetalert2";
function FormikCommon(props) {
  const { propsDatas, uploadImg, formik, showForm, show } = props;

  const viewOfComponent = (propsFormik) => {
    return (
      <div className={s.inputRender}>
        {propsDatas.fields.map((item, index) => {
          switch (item.type) {
            case "text":
              return (
                <div key={index} className={s.inputText}>
                  <span>
                    {item.label}:{" "}
                    {item.required == true && (
                      <SlackOutlined
                        style={{ fontSize: "12px", color: "#ff5722" }}
                      />
                    )}
                  </span>
                  <DebounceInput
                    disabled={propsDatas.edit && item.disabled}
                    value={propsFormik.values[item.keyName]}
                    debounceTimeout={500}
                    onChange={(e)=>{
                      console.log(e.target.value);
                      propsFormik.setFieldValue(item.keyName, e.target.value);
                    }}
                    name={item.keyName}
                  />
                  {propsFormik.errors[item.keyName] &&
                  propsFormik.touched[item.keyName] ? (
                    <div style={{ color: "red" }}>
                      {propsFormik.errors[item.keyName]}
                    </div>
                  ) : null}
                </div>
              );
           
            case "number":
              return (
                <div key={index} className={s.inputText}>
                  <span>
                    {item.label}:{" "}
                    {item.required == true && (
                      <SlackOutlined
                        style={{ fontSize: "12px", color: "#ff5722" }}
                      />
                    )}
                  </span>
                  <DebounceInput
                    disabled={propsDatas.edit && item.disabled}
                    name={item.keyName}
                    value={propsFormik.values[item.keyName]}
                    debounceTimeout={500}
                    onChange={propsFormik.handleChange}
                  />
                  {propsFormik.errors[item.keyName] &&
                  propsFormik.touched[item.keyName] ? (
                    <div style={{ color: "red" }}>
                      {propsFormik.errors[item.keyName]}
                    </div>
                  ) : null}
                </div>
              );
            case "date":
              return (
                <div key={index} className={s.inputDate}>
                  <span>
                    {item.label}:{" "}
                    {item.required == true && (
                      <SlackOutlined
                        style={{ fontSize: "12px", color: "#ff5722" }}
                      />
                    )}
                  </span>
                  <DatePicker
                    disabled={propsDatas.edit && item.disabled}
                    className={s.datePk}
                    value={
                      propsFormik.values[item.keyName] &&
                      moment(propsFormik.values[item.keyName], "YYYY-MM-DD")
                    }
                    //  moment(propsFormik.values[item.keyName],"YYYY-MM-DD")
                    name={item.keyName}
                    onChange={(value, dateString) => {
                      propsFormik.setFieldValue(item.keyName, dateString);
                    }}
                  />
                  {propsFormik.errors[item.keyName] &&
                  propsFormik.touched[item.keyName] ? (
                    <div style={{ color: "red" }}>
                      {propsFormik.errors[item.keyName]}
                    </div>
                  ) : null}
                </div>
              );
            case "time":
              return (
                <div key={index} className={s.inputDate}>
                  <span>
                    {item.label}:{" "}
                    {item.required == true && (
                      <SlackOutlined
                        style={{ fontSize: "12px", color: "#ff5722" }}
                      />
                    )}
                  </span>
                  <TimePicker
                    disabled={propsDatas.edit && item.disabled}
                    name={item.keyName}
                    value={
                      propsFormik.values[item.keyNam] &&
                      moment(propsFormik.values[item.keyName], "HH:mm:ss")
                    }
                    onChange={(time) => {
                      propsFormik.setFieldValue(
                        item.keyName,
                        dateTime.formatTime(time)
                      );
                    }}
                  />
                  {propsFormik.errors[item.keyName] &&
                  propsFormik.touched[item.keyName] ? (
                    <div style={{ color: "red" }}>
                      {propsFormik.errors[item.keyName]}
                    </div>
                  ) : null}
                </div>
              );
            case "file":
              return (
                <div key={index} className={s.inputSelect}>
                  <span>
                    {item.label}:{" "}
                    {item.required == true && (
                      <SlackOutlined
                        style={{ fontSize: "12px", color: "#ff5722" }}
                      />
                    )}
                  </span>
                  <input
                    disabled={propsDatas.edit && item.disabled}
                    type="file"
                    onChange={(e) => {
                      let checkExt = e.target.files[0].name.split(".");
                      if (checkExt[checkExt.length - 1] !== "zip") {
                        Swal.fire({ text: "File only .zip", icon: "warning" });
                      } else {
                        propsFormik.setFieldValue("file", e.target.files[0]);
                        
                      }
                    }}
                  />
                  {propsFormik.errors[item.keyName] &&
                  propsFormik.touched[item.keyName] ? (
                    <div style={{ color: "red" }}>
                      {propsFormik.errors[item.keyName]}
                    </div>
                  ) : null}
                </div>
              );
            case "select":
              return (
                <div key={index} className={s.inputSelect}>
                  <span>
                    {item.label}:{" "}
                    {item.required == true && (
                      <SlackOutlined
                        style={{ fontSize: "12px", color: "#ff5722" }}
                      />
                    )}
                  </span>
                  <Select
                    showSearch={true}
                    disabled={propsDatas.edit && item.disabled}
                    className={s.select}
                    value={propsFormik.values[item.keyName]}
                    onChange={(value, option) => {
                      propsFormik.setFieldValue(item.keyName, value);
                      if(propsDatas?.isSelectRender==item.keyName){
                        propsDatas?.renderSelect(value)
                      }
                     
                    }}
                    onSearch={(value) => {
                      propsDatas.onSearchOptions(value,item.keyName);
                    }}
                  >
                    {item.hasOwnProperty("children")
                      ? item.children?.map((chid, index) => {
                          return (
                            <Select.Option key={index} value={chid.value}>
                              {chid.name}
                            </Select.Option>
                          );
                        })
                      : propsDatas[item.keyName]?.map((option, index) => {
                          return (
                            <Select.Option key={index} value={option?.value}>
                              {option?.name}
                            </Select.Option>
                          );
                        })}
                  </Select>
                  {propsFormik.errors[item.keyName] &&
                  propsFormik.touched[item.keyName] ? (
                    <div style={{ color: "red" }}>
                      {propsFormik.errors[item.keyName]}
                    </div>
                  ) : null}
                </div>
              );
            case "description":
              return (
                <div key={index} className={s.input_description}>
                  <span>
                    {item.label}:{" "}
                    {item.required == true && (
                      <SlackOutlined
                        style={{ fontSize: "12px", color: "#ff5722" }}
                      />
                    )}
                  </span>
                  <DebounceInput
                    disabled={propsDatas.edit && item.disabled}
                    value={propsFormik.values[item.keyName]}
                    name={item.keyName}
                    debounceTimeout={500}
                    element={"textarea"}
                    onChange={propsFormik.handleChange}
                    maxLength={400}
                  />
                  {propsFormik.errors[item.keyName] &&
                  propsFormik.touched[item.keyName] ? (
                    <div style={{ color: "red" }}>
                      {propsFormik.errors[item.keyName]}
                    </div>
                  ) : null}
                </div>
              );
             
           
            default:
              return <DebounceInput key={index} debounceTimeout={500} />;
          }
        })}
      </div>
    );
  };
  
  console.log(formik);
  return (
    <Formik
      enableReinitialize
      initialValues={formik.initialValues}
      validationSchema={formik.validationSchema}
      onSubmit={(values, actions) => {
         console.log("values");
         propsDatas.onDispatch(values);
         actions.resetForm()
         showForm(false);
      }}
    >
      {(propsFormik) => {
        return (
          <Modal
          
            title={!propsDatas.edit ? `add form` : "edit form"}
            className={s.wrap_drawer}
            bodyStyle={{height:600,overflowY:"scroll"}}
            centered
            open={show}
            onOk={()=>propsFormik.submitForm(propsFormik.values)}
            onCancel={() => {
              propsFormik.resetForm()
              propsDatas.onCancel();
              showForm(false);
            }}
            okText={!propsDatas.edit ? `Add` : `Edit`}
          >
            <Form  >
              {viewOfComponent(propsFormik)}
            </Form>
          </Modal>
        );
      }}
    </Formik>
  );
}
export default FormikCommon;
