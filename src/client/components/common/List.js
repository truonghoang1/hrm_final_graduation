import React from "react";
import {
  Table,
  Pagination,
  Row,
  Col,
  Modal,
  Checkbox,
  Input,
  InputNumber,
} from "antd";
import s from "./common.module.scss";
import ImportFile from "./importInput";
import {
  FileAddOutlined,
  DownloadOutlined,
  AudioOutlined,
} from "@ant-design/icons";
import {useLocation} from "react-router-dom"
import { Link } from "react-router-dom";

function ListCommon(props) {
  const [page, setPage] = React.useState(10);
  const [filter, setFilter] = React.useState({opsSkip:[],start: 1,end: 2,
  });
 const location = useLocation()
  const condRender = location.pathname.includes("teacher") || location.pathname.includes("student")
    // || location.pathname.includes("salary")
  const [modal, setModal] = React.useState(false);
  const {
    columns,
    data,
    pageIndex,
    totalPage,
    onPagination,
    linkAdd,
    onSearch,
    onImportFile,
    dataFilterExport,onExportFile
  } = props;
  // console.log(filter);
  return (
    <div className={s.wrapListCommon}>
      <Row>
        <Col span={12}>
          {" "}
          <Input.Search
            placeholder=" search text or date(yyyy-mm-dd)"
            size="large"
            suffix={
              <AudioOutlined
                style={{
                  fontSize: 16,
                  color: "#1890ff",
                }}
              />
            }
            onPressEnter={(e) => {
              if (e.key === "Enter") {
                onSearch({ keySearch: e.target.value });
              }
            }}
            onSearch={(value) => onSearch({ keySearch: value })}
          />
        </Col>
        <Col span={1} offset={5}>
          <ImportFile onSaveFile={onImportFile} />
        </Col>
        <Col span={2} offset={1}>
          <Link to={linkAdd}>
            <FileAddOutlined className={s.iconAdd} />
          </Link>
        </Col>
       { condRender&&
       <Col span={2}>
          {" "}
          <Modal
            title="Chose information export "
            visible={modal}
            onOk={() => {
              onExportFile(filter)
              setModal(!modal)}}
            onCancel={() => setModal(!modal)}
            okText="Export"
          >
            <Checkbox.Group
              onChange={(checkValue) => { 
                const filterOps =  dataFilterExport.filter((item) => {
                  return !checkValue.includes(item);
                }).map((item)=>{ return [item,0]})
                let check =  Object.fromEntries(filterOps)
                setFilter({...filter,opsSkip: check })
              }}
            >
              <Row>
                <p>Total current record :{totalPage * page}</p>
              </Row>
              <Row>
                <Col>
                  <span>start index </span>{" "}
                  <InputNumber
                  required={true}
                    min={1}
                    max={totalPage * page - 1}
                    defaultValue={1}
                    onChange={(value) => {
                      setFilter({ ...filter, start: value });
                    }}
                  />
                </Col>
                <Col offset={1}>
                  {" "}
                  <span>end index </span>{" "}
                  <InputNumber
                  required={true}
                    min={2}
                    max={totalPage * page}
                    onChange={(value) => {
                      setFilter({ ...filter, end: value });
                    }}
                  />
                </Col>
              </Row>
              <Row><span>Choose properties:</span></Row>
              <Row>
                {dataFilterExport.map((item, index) => {
                  return (
                    <Col span={7} offset={1} key={index}>
                      {" "}
                      <Checkbox value={item}>{item}</Checkbox>
                    </Col>
                  );
                })}
              </Row>
            </Checkbox.Group>
          </Modal>
          <DownloadOutlined
            className={s.iconAdd}
            onClick={() => {
              setModal(!modal);
            }}
          />
        </Col>
       }
      </Row>

      <Table
        style={{ overflow: "scroll" }}
        columns={columns}
        dataSource={data}
        pagination={false}
      />
      <Pagination
        className={s.pagination}
        current={pageIndex}
        total={totalPage * 10}
        pageSizeOptions={[10, 25, 50, 100]}
        pageSize={page}
        onChange={(page, pageSize) => {
          onPagination({ page, limit: pageSize });
        }}
        onShowSizeChange={(current, size) => {
          setPage(size);
        }}
      />
    </div>
  );
}

export default ListCommon;
