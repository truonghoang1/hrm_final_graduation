import React from "react";
import { connect } from "react-redux";
import * as actions from "../../actions/user";
import { Row, Col } from "antd";
import { DebounceInput } from "react-debounce-input";
import { EyeInvisibleOutlined, EyeOutlined } from "@ant-design/icons";
import { Formik, Form, } from "formik";
import {useHistory,} from 'react-router-dom'
import logo from "../../../public/images/logoSoha.png";
import s from "./login.module.scss";
import * as yup from "yup";

export const LoginPage = (props) => {
  const [show, setShow] = React.useState(false);
  const history = useHistory()
  const handleShow = (e) => {
    setShow(!show);
  };
  const formik = {
    initialValues: { email: "", password: "" },
    validationSchema: yup.object({
      email: yup.string().email("Email invalid").required(),
      password: yup
        .string()
        .min(8, "password min 8")
        .max(24, "password max 24 character")
        .matches(
          /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W)/,
          "password contains at least [a-z][A-Z][0-9][/W]"
        ).required(),
    }),
  };
   React.useEffect(()=>{
    if(props.userState.token!==null){
      console.log(history);
      history.length<=3?history.push('/'):  history.goBack(-1)
    }
   },[props.userState.token
   ])
  return (
    <div className={s.wrap_login}>
      
      <Formik
        enableReinitialize
        initialValues={formik.initialValues}
        validationSchema={formik.validationSchema}
        onSubmit={(values, actions) => {
          console.log("abc", values);
          props.login(values)
        }}
      >
        {(propsFormik) => {
          return (
            <Row className={s.container}>
              <Col>
               
                <Form
                autoComplete="off"
                  className={s.form_formik}
                  onSubmit={(e)=>{
                    const email = e.target.elements.email.value;
                    const pass = e.target.elements.password.value;
                    propsFormik.setFieldValue('email',email)
                    propsFormik.setFieldValue('password',pass)
                    propsFormik.handleSubmit(e);
                  }}
                >
                  <DebounceInput
                    name="email"
                    value={propsFormik.values['email']}
                    className={s.input_email}
                    onChange={ propsFormik.handleChange}
                  />
                    {propsFormik.errors['email'] && propsFormik.touched['email'] ? (<div style={{color:"red"}}>{propsFormik.errors['email']}</div>) : null}
              

                  <div className={s.container_pass}>
                    <DebounceInput
                    value={propsFormik.values['password']}
                      name="password"
                      type={show ? "text" : "password"}
                      className={s.input_password}
                      onChange={propsFormik.handleChange}
                    />
                    {!show ? (
                      <EyeInvisibleOutlined
                        className={s.icon}
                        onClick={handleShow}
                      />
                    ) : (
                      <EyeOutlined onClick={handleShow} className={s.icon} />
                    )}
                  </div>
                  {propsFormik.errors['password'] && propsFormik.touched['password'] ? (<div style={{color:"red"}}>{propsFormik.errors['password']}</div>) : null}
                  <button type="submit"  >Summit</button>
                </Form>
              </Col>
            </Row>
          );
        }}
      </Formik>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    userState: state.UserReducer
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    login: (data) => {
      dispatch(actions.loginRequest(data));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
