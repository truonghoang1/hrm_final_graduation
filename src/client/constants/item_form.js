const employeeInput = [
  { type: "text", keyName: "full_name", label: "Họ và Tên ", required: true },
    { type: "date", keyName: "dob", label: "Ngày sinh", rquired: true },
    { type:"select",keyName:"gender",label:"Giới tính",required:true,children:[{ name: "female", value: "female" }, { name: "male", value: "male" }, { name: "other", value: "other" }]},
    {type:"select",keyName:"department",label:"Phòng ban",required:true},
    { type: "file", keyName: "employee_image", label: "Ảnh", required: false },
    { type: "text", keyName: "email", label: " Email", required: true,disabled:true },
    { type: "text", keyName: "cmnd", label: "Căn cước công dân", required: true },
    { type: "text", keyName: "phone", label: "Điện thoại", required: false },
  
    { type: "description", keyName: "present_address", label: "Chỗ ở hiên tại", required: false },
    { type: "description", keyName: "permanent_address", label: "Quê Quán", required: false },
    { type: "text", keyName: "tax_code", label: "Mã thuế", required: false },
    { type: "date", keyName: "joining_date", label: "Ngày kí hợp đồng", required: false },
   
  ]
  const employeeObject ={
    full_name:"",dob:"",email:"",phone:"",currentAddress:"",
    permanence_address:"",cmnd:"",gender:"",tax_code:"",department:"",
    joining_date:""
  }
  const accountInput =[
    { type: "text", keyName: "email", label: "email", required: false },
    { type: "text", keyName: "role", label: "quyền", required: false },
  ]
  const objectAccount ={
    email:"",role:""
  }
  //department
  const departmentInput =  [
      {
        type: "text",
        keyName: "name",
        label: "Tên phòng ban", required: true
      },
      { type: "text", keyName: "description", label: "Mô tả ", required: false },
      { type: "text", keyName: "position", label: "Vị trí chức vụ", required: true },
    ]
  const departmentObject ={
    name:"",position:"",description:""
  }
 //role
 const roleInput =  [
  {
    type: "text",
    keyName: "name",
    label: "Tên ", required: true
  },
  { type: "text", keyName: "description", label: "Mô tả ", required: false },
  { type: "select", keyName: "level", label: "Level", required: true,children:[{name:"admin",value:1},{name:"manager",value:2},{name:"employee",value:3},{name:"guest",value:4}] },
]
   const roleObject={
    name:"",description:"",level:4
   }
   //salary
   const salaryInput=[
    {
      type: "select",
      keyName: "staff",
      label: "Nhân viên ", required: true
    },
    { type: "text", keyName: "description", label: "Mô tả ", required: false },
    { type: "number", keyName: "salary", label: "Lương ", required: true },
    { type: "select", keyName: "bonus", label: "Thưởng/Trợ cấp ", required: false },
    { type: "date", keyName: "joining_date", label: "Ngày bắt đầu tính lương ", required: true },
   ]
   const salaryObject={
    staff:null,description:"",salary:null,bonus:null,joining_date:""
   }
   const bonusObject={
    name:"",amount:0,description:""
   }
   const bonusInput =[
    { type: "text", keyName: "name", label: "Tên trợ cấp ", required: true },
    { type: "text", keyName: "description", label: "Mô tả ", required: false },
    { type: "number", keyName: "amount", label: "mức hỗ trợ ", required: true },
   ]
   const timeSheetObject ={
    staff:"",time_log:"",date_log:""
   }
   const timeSheetInput =[
    { type: "date", keyName: "date", label: "Ngày ", required: true },
   ]
   const leaveAppInput=[
    { type: "text", keyName: "name", label: "Tên giấy phép ", required: true },
    { type: "select", keyName: "type", label: "Loại hình nghỉ",children:[
      {name:"phép năm",value:"phep_nam"},{name:'không lương',value:"không lương"}
    ], required: true },
    { type: "description", keyName: "description", label: "Lí do nghỉ", required: true },
    { type: "date", keyName: "date", label: "Ngày xin nghỉ ", required: true },
    {
      type:'select',keyName:'rol',label:'thời gian nghỉ',children:[{name:"một ngày",value:"a_day"},{name:'nửa ngày',value:"half_day"}]
    }
   ]
   const leaveAppObject ={
    name:'',
    type:'',description:'',date:'',rol:''
   }
  export {
employeeInput,employeeObject,departmentInput,departmentObject,
roleInput,roleObject,salaryInput,salaryObject,accountInput,objectAccount,
bonusInput,bonusObject,timeSheetInput,timeSheetObject,leaveAppInput,leaveAppObject

  };
  