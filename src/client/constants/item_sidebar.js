



// export {
//   areaDiv
// }
import React from 'react'
import {
  TeamOutlined
  , ContactsOutlined, InsertRowLeftOutlined, ReadOutlined, AccountBookOutlined, LockOutlined
} from '@ant-design/icons'
import Cookies from 'js-cookie';
const role = Number.parseInt(Cookies.get("role"))
const itemsMain = [

  {
    label: "Nhân Sự", key: 'sub2', icon: <TeamOutlined />,
    children: [
      { label: "List ", key: "6", nlink: "/employee/list", disabled: role > 3 && true },
    ]
  },
  {
    label: "Quyền", key: "sub3", icon: <ContactsOutlined />,
    children: [
      { label: "List ", key: "11", nlink: "/role/list", disabled: role > 2 && true },
      // ,{label:"View Detail",key:"12",nlink:"/role/detail",disabled:role>2&&true},
    ]
  },
  {
    label: "Phòng Ban", key: "sub4", icon: <InsertRowLeftOutlined />,
    children: [
      { label: "List ", key: "16", nlink: "/department/list", disabled: role > 2 && true },
    ]
  },
  {
    label: "Lương", key: "sub5", icon: <ReadOutlined />,
    children: [
      { label: "List ", key: "20", nlink: "/salary/list", disabled: role > 2 && true },
      { label: "Bonus ", key: "21", nlink: "/bonus", disabled: role > 2 && true },
    ]
  },
  {
    label: "TimeSheet", key: "25", nlink: "/admin/timesheet"
  }

];
const itemsManager = [
  {
    label: "Account", key: "sub6", icon: <AccountBookOutlined />,
    children:
      [{ label: "List", key: "24", nlink: "/account/list", disabled: role > 2 && true },

      ]
  },


]
const itemPages = [
  {
    label: "Authentication", key: "sub7", icon: <LockOutlined />,
    children: [
      // {label:"Login",key:"36",nlink:"/login"},
      { label: "Register", key: "37", nlink: "/admin/signup" },
      { label: "Forgot Password", key: "39", nlink: "/forgot-password" },
      // {label:"Error Page",key:"40",nlink:"/error-page"},
    ]
  },
  //


]
export let areaUser = [
  {
    nameArea: "User Time Log",
    items: [
      {
        label: "TimeLog", key: "timelog", icon: <LockOutlined />,nlink: "/user/timesheet",
      },
      {
        label: "Giấy nghỉ ", key: "leaveapp", icon: <LockOutlined />,
        children: [
          { label: "Làm đơn", key: "37", nlink: "/user/leave-app" },

        ]
      },
      // {
      //   label: "Làm ngoài giờ", key: "overtime", icon: <LockOutlined />,
      //   children: [
      //     // {label:"Login",key:"36",nlink:"/login"},
      //     { label: "Làm đơn", key: "37", nlink: "/user/over-time" },
      //     { label: "Danh sách đã gửi", key: "39", nlink: "/user/list-over-time" },
      //     // {label:"Error Page",key:"40",nlink:"/error-page"},
      //   ]
      // },
    ]
  }
]



const areaDiv = [
  {
    nameArea: 'Main Menu',
    items: itemsMain
  }, {
    nameArea: 'Account',
    items: itemsManager
  }, {
    nameArea: 'Pages',
    items: itemPages
  },

]
function item_sidebar() {
  return areaDiv
}

export default item_sidebar