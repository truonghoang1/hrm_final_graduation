const optionsChartColumn ={
    options: {
      chart: {
          id: "basic-bar",
      },
      xaxis: {
          color: '#ffc107',
          categories: [2010,2011,2012,2013,2014,2015,2016],
      },
      stroke: {
          curve: 'smooth',
      }, dataLabels: {
          enabled: false
      },
      legend: {
          show: true,
          position: 'bottom'
      }
  },
  series:[{
      name: 'Boy',
      data: [44, 55, 57, 56, 61, 58, 63, 60, 66]
    }, {
      name: 'Girl',
      data: [76, 85, 101, 98, 87, 105, 91, 114, 94]
    }],
  
        
      }
  
      const optionsChartArea ={
          series: [{
              name: 'Student',
              data: [49, 22, 23, 65, 43, 21, 56]
            }, {
              name: 'Teacher',
              data: [43, 21, 56, 57, 70, 23, 43]
            }],
            options: {
              chart: {
                
                type: 'area'
              },
              dataLabels: {
                enabled: false
              },
              stroke: {
                curve: 'smooth'
              },
              xaxis: {
                type: 'text',
                categories: ["Jan","Feb","Mar","Apr","May","Jun","July"]
              },
             
            },
          
          
          
        }
  const itemsChartDonut = { chart: {
      width: 380,
      type: 'donut',
      dropShadow: {
        enabled: true,
        color: '#111',
        top: -1,
        left: 3,
        blur: 3,
        opacity: 0.2
      }
    },
    stroke: {
      width: 0,
    },
    plotOptions: {
      pie: {
        donut: {
          labels: {
            show: true,
            total: {
              showAlways: true,
              show: true
            }
          }
        }
      }
    },
    labels: ["Comedy", "Action", "SciFi", "Drama", "Horror"],
    dataLabels: {
      dropShadow: {
        blur: 3,
        opacity: 0.8
      }
    },
    fill: {
    type: 'pattern',
      opacity: 1,
      pattern: {
        enabled: true,
        style: ['verticalLines', 'squares', 'horizontalLines', 'circles','slantedLines'],
      },
    },
    states: {
      hover: {
        filter: 'none'
      }
    },
    theme: {
      palette: 'palette2'
    },
    title: {
      text: "Favourite Movie Type"
    },
    responsive: [{
      breakpoint: 480,
      options: {
        chart: {
          width: 200
        },
        legend: {
          position: 'bottom'
        }
      }
    }]
  
    }
        export {
          optionsChartColumn,optionsChartArea,itemsChartDonut
        }