/* eslint-disable import/no-anonymous-default-export */
import * as actions from "../actions/roles";

const DEFALT_STATE = {
  isLoading: false,
  isFetching: false,
  dataDetail:null,
  data: [],
  error: false,
  errorMsg: null,
  totalPage: 1,
  page: 1,
  keySearch: null,
  limit:10
};

export default function (state = DEFALT_STATE, action) {
  switch (action.type) {
    case actions.getRolesRequest.type:
    case actions.addRolesRequest.type:
    case actions.updateRolesRequest.type:
    case actions.deleteRolesRequest.type:
      case actions.detailRolesRequest.type:
      return {
        ...state,
        isLoading: true,
      };
   
    case actions.getRolesSuccess.type:
      return {
        ...state,
        isLoading: false,
        isFetching: true,
        data: action.payload.data,
        page: action.payload.page,
        totalPage: action.payload.totalPage,
      };
      case actions.detailRolesSuccess.type:
        return{
          ...state,
          isLoading:false,
          isFetching:true,
          dataDetail:action.payload
        }
    case actions.deleteRolesSuccess.type:
    case actions.updateRolesSuccess.type:
    case actions.addRolesSuccess.type:
      return {
        ...state,
        isFetching: true,
        isLoading: false,
      };
   case actions.addRolesFailure.type:
    case actions.getRolesFailure.type:
    case actions.updateRolesFailure.type:
    case actions.deleteRolesFailure.type:
      case actions.detailRolesFailure.type:
      return {
        ...state,
        isLoading: false,
        isFetching: false,
        error: true,
        errorMsg: action.payload,
      };

    default:
      return state;
  }
}
