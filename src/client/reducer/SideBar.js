/* eslint-disable import/no-anonymous-default-export */
import *as actions from '../actions/sidebar'

const DEFAULT_STATE = {
    collapsed:false,
    modal:false,
    showProfile:false
   
   
}
export default function(state=DEFAULT_STATE,action){
   switch (action.type) {
       case actions.collapsed.type:
       
         return{
            ...state,
            collapsed:!state.collapsed
         }
         case actions.showModel.type:
          return{
            ...state,
            modal:!state.modal
          }
          case actions.showProfile.type:
            return{
               ...state,
               showProfile:!state.showProfile
            }
       default: return state
          
   }
}