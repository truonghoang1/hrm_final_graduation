/* eslint-disable import/no-anonymous-default-export */
import * as actions from "../actions/employee";

const DEFALT_STATE = {
  isLoading: false,
  isFetching: false,
  detail: null,
  data: [],
  error: false,
  errorMsg: null,
  totalPage: 1,
  page: 1,
  keySearch: null,
  limit: 10,
  
};

export default function (state = DEFALT_STATE, action) {
  switch (action.type) {
    case actions.getEmployeeRequest.type:
    case actions.addEmployeeRequest.type:
    case actions.updateEmployeeRequest.type:
    case actions.deleteEmployeeRequest.type:
    case actions.getEmployeeDetailRequest.type:
    case actions.importEmployeeRequest.type:
    case actions.exportEmployeeRequest.type:
      return {
        ...state,
        isLoading: true,
      };

    case actions.getEmployeesuccess.type:
      return {
        ...state,
        isLoading: false,
        isFetching: true,
        detail:null,
        data: action.payload.data,
        page: action.payload.page,
        totalPage: action.payload.totalPage,
      };
    case actions.getEmployeeDetailsuccess.type:
      return {
        ...state,
        isLoading: false,
        isFetching: true,
        detail: action.payload,
      };
    case actions.deleteEmployeesuccess.type:
    case actions.updateEmployeesuccess.type:
    case actions.addEmployeesuccess.type:
    case actions.importEmployeesuccess.type:
      case actions.exportEmployeesuccess.type:
      return {
        ...state,
        isFetching: true,
        isLoading: false,
      };
      
    case actions.addEmployeefailure.type:
    case actions.getEmployeefailure.type:
    case actions.updateEmployeefailure.type:
    case actions.deleteEmployeefailure.type:
    case actions.getEmployeeDetailfailure.type:
    case actions.importEmployeefailure.type:
      case actions.exportEmployeefailure.type:
      return {
        ...state,
        isLoading: false,
        isFetching: false,
        error: true,
        errorMsg: action.payload,
      };

    default:
      return state;
  }
}
