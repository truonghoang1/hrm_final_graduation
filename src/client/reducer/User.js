/* eslint-disable import/no-anonymous-default-export */
import * as actions from "../actions/user";

const DEFALT_STATE = {
  isLoading: false,
  isFetching: false,
  data: "",
  token: null,
  statistic:null,
  error: false,
  errorMsg: null,
  totalPage: 1,
  page: 1,
  keySearch: null,
  active:null,
  limit: 10,
};

export default function (state = DEFALT_STATE, action) {
  switch (action.type) {
    case actions.signInRequest.type:
    case actions.signupRequest.type:
    case actions.forgotPasswordRequest.type:
    case actions.signupGuestRequest.type:
    case actions.resetPasswordRequest.type:
    case actions.activationAccountRequest.type:
      case actions.statisticalRequest.type:
      return {
        ...state,
        isLoading: true,
      };
    case actions.signInSuccess.type:
      return {
        ...state,
        isLoading: false,
        isFetching: true,
        token: action.payload.token,
      };
      case actions.statisticalSuccess.type:
        return{
          ...state,
          isLoading:false,
          isFetching:true,
          statistic:action.payload
        }
    case actions.forgotPasswordSuccess.type:
    case actions.signupGuestSuccess.type:
    case actions.resetPasswordSuccess:
    case actions.activationAccountSuccess.type:
    case actions.signupSuccess.type:
      return {
        ...state,
        isLoading: false,
      };

    case actions.signInFailure.type:
    case actions.signupFailure.type:
    case actions.forgotPasswordFailure.type:
   case actions.statisticalFailure.type:
    case actions.resetPasswordFailure.type:
    case actions.signupGuestFailure.type:
      return {
        ...state,
        isLoading: false,
        isFetching: false,
        error: true,
        errorMsg: action.payload,
      };
      case actions.activationAccountFailure.type:
        return{
            ...state,
            ...state,
            isLoading: false,
            isFetching: false,
            error: true,
            errorMsg: action.payload,
            active:400
        }
    default:
      return state;
  }
}
