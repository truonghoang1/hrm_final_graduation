/* eslint-disable import/no-anonymous-default-export */
import * as actions from "../actions/account";

const DEFALT_STATE = {
  isLoading: false,
  isFetching: false,
  dataDetail:null,
  data: [],
  error: false,
  errorMsg: null,
  totalPage: 1,
  page: 1,
  keySearch: null,
  limit:10
};

export default function (state = DEFALT_STATE, action) {
  switch (action.type) {
    case actions.getAccountRequest.type:
    case actions.addAccountRequest.type:
    case actions.updateAccountRequest.type:
    case actions.deleteAccountRequest.type:
      case actions.getAccountDetailRequest.type:
      return {
        ...state,
        isLoading: true,
      };
   
    case actions.getAccountsuccess.type:
      return {
        ...state,
        isLoading: false,
        isFetching: true,
        data: action.payload.data,
        page: action.payload.page,
        totalPage: action.payload.totalPage,
      };
      case actions.getAccountDetailsuccess.type:
        return{
          ...state,
          isLoading:false,
          isFetching:true,
          dataDetail:action.payload
        }
    case actions.deleteAccountsuccess.type:
    case actions.updateAccountsuccess.type:
    case actions.addAccountsuccess.type:
      return {
        ...state,
        isFetching: true,
        isLoading: false,
      };
   case actions.addAccountfailure.type:
    case actions.getAccountfailure.type:
    case actions.updateAccountfailure.type:
    case actions.deleteAccountfailure.type:
      case actions.getAccountDetailfailure.type:
      return {
        ...state,
        isLoading: false,
        isFetching: false,
        error: true,
        errorMsg: action.payload,
      };

    default:
      return state;
  }
}
