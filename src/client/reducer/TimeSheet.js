/* eslint-disable import/no-anonymous-default-export */
import * as actions from "../actions/timesheet";

const DEFALT_STATE = {
  isLoading: false,
  isFetching: false,
  detail: null,
  data: [],
  error: false,
  errorMsg: null,
  totalPage: 1,
  page: 1,
  keySearch: null,
  limit: 10,

};

export default function (state = DEFALT_STATE, action) {
  switch (action.type) {
    case actions.getTimeSheetAdminRequest.type:
    case actions.addTimeSheetAdminRequest.type:
    case actions.updateTimeSheetAdminRequest.type:
    case actions.deleteTimeSheetAdminRequest.type:
    case actions.getTimeSheetAdminDetailRequest.type:
    case actions.importTimeSheetAdminRequest.type:
    case actions.exportTimeSheetAdminRequest.type:
    case actions.getTimeSheetEmployeeRequest.type:
    case actions.addTimeSheetEmployeeRequest.type:
    case actions.getLeaveDateRequest.type:
    case actions.getListLeaveApplicationRequest.type:
    case actions.addLeaveApplicationRequest.type:
      return {
        ...state,
        isLoading: true,
      };

    case actions.getTimeSheetAdminsuccess.type:
      return{
        ...state,
        isLoading: false,
        isFetching: true,
        data: action.payload.data,
        
      }
    case actions.getTimeSheetEmployeesuccess.type:
      case actions.getListLeaveApplicationsuccess.type:
      console.log("data--", action.payload);
      return {
        ...state,
        isLoading: false,
        isFetching: true,
        data: action.payload.data,
        page: action.payload.page,
        totalPage: action.payload.totalPage,
      };
    case actions.getTimeSheetAdminDetailsuccess.type:
      return {
        ...state,
        isLoading: false,
        isFetching: true,
        detail: action.payload,
      };
    case actions.deleteTimeSheetAdminsuccess.type:
    case actions.updateTimeSheetAdminsuccess.type:
    case actions.addTimeSheetAdminsuccess.type:
    case actions.importTimeSheetAdminsuccess.type:
    case actions.exportTimeSheetAdminsuccess.type:
    case actions.addTimeSheetEmployeesuccess.type:
      case actions.addLeaveApplicationsuccess.type:
      return {
        ...state,
        isFetching: true,
        isLoading: false,
      };

    case actions.addTimeSheetAdminfailure.type:
    case actions.getTimeSheetAdminfailure.type:
    case actions.updateTimeSheetAdminfailure.type:
    case actions.deleteTimeSheetAdminfailure.type:
    case actions.getTimeSheetAdminDetailfailure.type:
    case actions.importTimeSheetAdminfailure.type:
    case actions.exportTimeSheetAdminfailure.type:
    case actions.getTimeSheetEmployeefailure.type:
    case actions.addTimeSheetEmployeefailure.type:
      case actions.addLeaveApplicationfailure.type:
        case actions.getListLeaveApplicationfailure.type:
      return {
        ...state,
        isLoading: false,
        isFetching: false,
        error: true,
        errorMsg: action.payload,
      };

    default:
      return state;
  }
}
