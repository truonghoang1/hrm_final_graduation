/* eslint-disable import/no-anonymous-default-export */
import * as actions from "../actions/salary";

const DEFALT_STATE = {
  isLoading: false,
  isFetching: false,
  dataDetail:null,
  data: [],
  error: false,
  errorMsg: null,
  totalPage: 1,
  page: 1,
  keySearch: null,
  limit:10,
 
};

export default function (state = DEFALT_STATE, action) {
  switch (action.type) {
    case actions.getSalaryRequest.type:
    case actions.addSalaryRequest.type:
    case actions.updateSalaryRequest.type:
    case actions.deleteSalaryRequest.type:
      case actions.getSalaryDetailRequest.type:
        case actions.importSalarysRequest.type:
          case actions.exportSalarysRequest.type:
      return {
        ...state,
        isLoading: true,
      };
   
    case actions.getSalarysuccess.type:
      return {
        ...state,
        isLoading: false,
        isFetching: true,
        data: action.payload.data,
        page: action.payload.page,
        totalPage: action.payload.totalPage,
      };
      
      case actions.getSalaryDetailsuccess.type:
        return{
          ...state,
          isLoading:false,
          isFetching:true,
          dataDetail:action.payload
        }
    case actions.deleteSalarysuccess.type:
    case actions.updateSalarysuccess.type:
    case actions.addSalarysuccess.type:
      case actions.importSalarysSuccess.type:
        case actions.exportSalarysSuccess.type:
      return {
        ...state,
        isFetching: true,
        isLoading: false,
      };
   case actions.addSalaryfailure.type:
    case actions.getSalaryfailure.type:
    case actions.updateSalaryfailure.type:
    case actions.deleteSalaryfailure.type:
      case actions.getSalaryDetailfailure.type:
        case actions.importSalarysFailure.type:
          case actions.exportSalarysFailure.type:
      return {
        ...state,
        isLoading: false,
        isFetching: false,
        error: true,
        errorMsg: action.payload,
      };

    default:
      return state;
  }
}
