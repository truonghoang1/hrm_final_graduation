/* eslint-disable import/no-anonymous-default-export */
import * as actions from "../actions/department";

const DEFALT_STATE = {
  isLoading: false,
  isFetching: false,
  dataDetail:null,
  data: [],
  error: false,
  errorMsg: null,
  totalPage: 1,
  page: 1,
  keySearch: null,
  limit:10
};

export default function (state = DEFALT_STATE, action) {
  switch (action.type) {
    case actions.getDepartmentRequest.type:
    case actions.addDepartmentRequest.type:
    case actions.updateDepartmentRequest.type:
    case actions.deleteDepartmentRequest.type:
      case actions.getDepartmentDetailRequest.type:
      return {
        ...state,
        isLoading: true,
      };
   
    case actions.getDepartmentsuccess.type:
      return {
        ...state,
        isLoading: false,
        isFetching: true,
        data: action.payload.data,
        page: action.payload.page,
        totalPage: action.payload.totalPage,
      };
      case actions.getDepartmentDetailsuccess.type:
        return{
          ...state,
          isLoading:false,
          isFetching:true,
          dataDetail:action.payload
        }
    case actions.deleteDepartmentsuccess.type:
    case actions.updateDepartmentsuccess.type:
    case actions.addDepartmentsuccess.type:
      return {
        ...state,
        isFetching: true,
        isLoading: false,
      };
   case actions.addDepartmentfailure.type:
    case actions.getDepartmentfailure.type:
    case actions.updateDepartmentfailure.type:
    case actions.deleteDepartmentfailure.type:
      case actions.getDepartmentDetailfailure.type:
      return {
        ...state,
        isLoading: false,
        isFetching: false,
        error: true,
        errorMsg: action.payload,
      };

    default:
      return state;
  }
}
