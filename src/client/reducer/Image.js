/* eslint-disable import/no-anonymous-default-export */
import *as actions from '../actions/image'

const DEFAULT_STATE = {
    isLoading:false,
    isFetching:false,
    data:null,
    error:false,
    errMessage:null  
}
export default function(state=DEFAULT_STATE,action){
   switch (action.type) {
       case actions.addImageRequest.type:
         return{
            ...state,
            isLoading:true
         }
         case actions.addImageSuccess.type:
       
            return{
               ...state,
              isLoading:false,
              isFetching:true,
              data:action.payload
            }
            case actions.addImageFailure.type:
       
                return{
                   ...state,
                   isLoading:false,
                   isFetching:false,
                   error:true,
                   errMessage:action.payload
                }
         
       default: return state
          
   }
}