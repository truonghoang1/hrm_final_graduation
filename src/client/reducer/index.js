import { combineReducers } from "redux";
import UserReducer from "./User";
import RolesReducer from "./Roles";
import SidebarReducer from "./SideBar";
import ImageReducer from "./Image";
import EmployeeReducer from "./Employee";
import DepartmentReducer from "./Department";
import SalaryReducer from "./Salary";
import AccountReducer from "./Account"
import BonusReducer from "./Bonus"
import TimeSheetReducer from "./TimeSheet"
export default combineReducers({
    userState: UserReducer,
    roleState: RolesReducer,
    sideBarState: SidebarReducer,
    imageState: ImageReducer,
    departmentState: DepartmentReducer,
    salaryState: SalaryReducer,
    employeeState: EmployeeReducer,
    accountState:AccountReducer,
    bonusState:BonusReducer,
    timesheetState:TimeSheetReducer


}
)
