/* eslint-disable import/no-anonymous-default-export */
import * as actions from "../actions/bonus";

const DEFALT_STATE = {
  isLoading: false,
  isFetching: false,
  detail:null,
  data: [],
  error: false,
  errorMsg: null,
  totalPage: 1,
  page: 1,
  keySearch: null,
  limit:10
};

export default function (state = DEFALT_STATE, action) {
  switch (action.type) {
    case actions.getBonusRequest.type:
    case actions.addBonusRequest.type:
    case actions.updateBonusRequest.type:
    case actions.deleteBonusRequest.type:
      case actions.getBonusDetailRequest.type:
      return {
        ...state,
        isLoading: true,
      };
   
    case actions.getBonussuccess.type:
      return {
        ...state,
        isLoading: false,
        isFetching: true,
        data: action.payload.data,
        page: action.payload.page,
        totalPage: action.payload.totalPage,
      };
      case actions.getBonusDetailsuccess.type:
        return{
          ...state,
          isLoading:false,
          isFetching:true,
          detail:action.payload
        }
    case actions.deleteBonussuccess.type:
    case actions.updateBonussuccess.type:
    case actions.addBonussuccess.type:
      return {
        ...state,
        isFetching: true,
        isLoading: false,
      };
   case actions.addBonusfailure.type:
    case actions.getBonusfailure.type:
    case actions.updateBonusfailure.type:
    case actions.deleteBonusfailure.type:
      case actions.getBonusDetailfailure.type:
      return {
        ...state,
        isLoading: false,
        isFetching: false,
        error: true,
        errorMsg: action.payload,
      };

    default:
      return state;
  }
}
