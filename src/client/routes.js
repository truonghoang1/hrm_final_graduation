import {Redirect,Route,Switch} from "react-router-dom";
import React from 'react'
import cookie from 'js-cookie'
import * as pages from './pages'
function ProtectedRoute({ component: Component, ...restOfProps }) {
    const isAuthenticated = cookie.get("SESSION_ID")
    return (
      <Route
        {...restOfProps}
        render={(props) =>
          isAuthenticated ? <Component {...props} /> : <Redirect to="/login" />
        }
      />
    );
  }
function routes() {
  const auth = Number.parseInt(cookie.get("role"))
  return (
    <Switch>
     <Route  path='/login' exact component={pages.Login} />
    <Route  path='/register' exact component={pages.Register}/>
    <Route  path={"/forgot-password" }exact component={pages.ForgotPassword}/>
    <Route  path={"/error-page" } exact component={pages.ErrorPage}/>
    <Route   path={"/loading-page" } exact component={pages.ErrorPage}/>
    <Route path={"/user/reset/:id"}  component={pages.ResetPassword}/>
    <Route path={"/user/active/:user"} exact component={pages.ActiveAccount}/>
    {/* dashboard */}
      <ProtectedRoute path="/" exact >
        {/* {auth?  */}
        <Redirect to={"/employee/list"}/>
        {/* <Redirect to={"/user/timeshett"}/> */}
        {/* } */}
      </ProtectedRoute>
   
   //timesheet
    <ProtectedRoute path={'/admin/timesheet'} exact component={pages.TimeSheetAdmin} />
    {/* student */}
    <ProtectedRoute  path={`/employee/list`} exact component={pages.EmployeeList} />
    {/* teacher */}
    <ProtectedRoute  path="/role/list" exact component={pages.RoleList} />
   
    <ProtectedRoute path="/account/list" exact component={pages.Account}/>
<ProtectedRoute path="/user/timesheet" exact component={pages.TimeSheetEmployee}/>
<ProtectedRoute path="/user/leave-app" exact component={pages.LeaveApplication}/>
      <ProtectedRoute  path="/department/list" exact component={pages.DepartmentList} />
     <ProtectedRoute  path="/salary/list" exact component={pages.SalaryList} />
     <ProtectedRoute  path="/bonus" exact component={pages.Bonus} />
    {/* account */}
  
 {/* profile_user */}
<ProtectedRoute  path="/profile" exact component={pages.ProfileUser}/>
<ProtectedRoute  path="/admin/signup" exact component={pages.RegisterAdmin}/>
{/* <ProtectedRoute path ="/animation" exact component={pages.Animations}/> */}
  </Switch>
  )
}

export default routes 