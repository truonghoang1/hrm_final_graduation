import React from "react";
import { useLocation } from "react-router-dom";
import HeaderShare from "./components/shared/Header";
import Sidebar from "./components/shared/Sidebar";
import ModalSideBar from "./components/shared/DrawerSidebar";
import Breadcrum from "./components/shared/Breadcrum";

import { showModel, collapsed } from "./actions/sidebar";
import { Layout } from "antd";
import Routes from "./routes";
import { connect } from "react-redux";

import s from "./styles/app.module.scss";
const { Sider, Content,Footer } = Layout;

function App(props) {
  const [cond, setCond] = React.useState(
    window.location.pathname.includes("user/reset") ||
    window.location.pathname.includes("user/active") ||
    window.location.pathname === "/login" ||
      window.location.pathname === "/register" ||
      window.location.pathname === "/forgot-password" ||
      window.location.pathname === "/error-page" ||
      window.location.pathname === "/loading-page"
  );
  const location = useLocation();
  React.useEffect(() => {
    const cond =
    location.pathname.includes("user/reset") ||
    location.pathname.includes("user/active") ||
      location.pathname === "/login" ||
      location.pathname === "/register" ||
      location.pathname === "/forgot-password" ||
      location.pathname === "/error-page" ||
      location.pathname === "/loading-page";
    cond ? setCond(true) : setCond(false);
  }, [location]);
   console.log("condition",cond);
   

  return (
 
    <Layout  className={s.wrapAll} >
    {/* top layout */}
      {!cond?  <HeaderShare/>:""}
       {/* middle layout */}
    
      <Layout className={s.wrapContent} hasSider>
     
        {!cond ? (
         <Sider trigger={null} collapsible className={s.sideBar}  collapsed={props.collapse} theme="light"> <Sidebar /> </Sider> ) : ( "" )}
         <ModalSideBar/>
      <Layout className={s.container_content}>
     
     
          <Content className={s.content}  >
       
          {!cond ? <Breadcrum /> : ""}
          <Routes />
          {!cond ?  <Footer
          style={{
            textAlign: 'center',
            borderTop:"1px solid #e0e0e0"
          }}
          >
          HRM SYSTEM ©2022 Created by TRUONG HOANG
        </Footer> : ""}
         
        
          </Content>
         
          </Layout>
      </Layout>
    </Layout>
    
    

  );
}
const mapStateToProps = (state) => {
  return {
    collapse: state.sideBarState.collapsed,
    modal:state.sideBarState.modal
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    toggle: (data) => {
      dispatch(collapsed());
    },
    offline:(data)=>{
      dispatch()
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
