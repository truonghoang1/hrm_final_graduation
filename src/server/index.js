require('dotenv').config();
const express = require('express');
const cors = require('cors');
const path = require('path');
const mongose = require('mongoose');

require('./models');
const routes = require('./api/routes/admin');
const routeImg = require('./api/upload/imageUpload');
const routesUser = require('./api/routes/user');
const routerTest = require("./api/routes/test")
const fileUpload = require('express-fileupload');
const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(
  fileUpload({
    useTempFiles: true,
    tempFileDir: '/tmp/',
  })
);

//mongose connect
mongose.Promise = global.Promise;
 const url = 'mongodb+srv://truonghoang:truong652000@cluster0.yc1jt1w.mongodb.net/HRM?retryWrites=true&w=majority'
 const url2 = 'mongodb://localhost:27017/hrm';
mongose.connect(url2, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    // serverSelectionTimeoutMS: 5000,
    maxPoolSize: 10, // Maintain up to 10 socket connections
    // serverSelectionTimeoutMS: 5000, // Keep trying to send operations for 5 seconds
    // socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
    // family: 4 // Use IPv4, skip trying IPv6
  })
  .then(() => {
   
    console.log('Connected db');
  })
  .catch((err) => {
    console.log(err);
  });
  app.use('/admin', routes);
  app.use('/upload', routeImg);
  app.use('/user', routesUser);
  app.use(routerTest)
app.listen(process.env.PORT, () => {
  console.log(`Connect ${process.env.PORT}`);
});
