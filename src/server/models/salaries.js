const mongoose = require("mongoose")
const {model,Schema} = mongoose
const Salarys = new Schema({
   
    staff:{
       type:Schema.Types.ObjectId,
       ref:'Employees'
       
    },
    description:{
        type:String,
       
    },salary:{
        type: Number,
        require:true
        
    }
    ,joining_date:{
        type:String,
        require:true
    }
    ,bonus:{
        type:[Schema.Types.ObjectId],
        ref:'Bonuses'
       
    }
},{timestamps:true})
Salarys.index({ "$**": "text" })
module.exports = model('Salarys',Salarys)