const { model, Schema } = require("mongoose")

const Documents = new Schema({
    name:String,
    type: {
        type:String,
        
    },
    description: String,
    date: String,
    rol: {
        type: String,
        enum: ["a_day", "half_day"],
        default:"a_day"
    },
    complete:{
        type:Boolean,
        default:false
    },

    staff:{
        type:Schema.Types.ObjectId,
        ref:"Employees"
    }
}, {
    timestamps:true
})
Documents.index({ "$**": "text" })
module.exports = model("Documents",Documents)