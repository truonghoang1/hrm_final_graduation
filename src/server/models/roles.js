const mongoose = require("mongoose")
const {model,Schema} = mongoose
const Roles = new Schema({
    name:{
        type:String,
        require:true
    },
    description:{
        type:String,
       
    },
    level: {
        type: Number,
        enum: [1, 2, 3, 4],
        default:4
    },
},{
    timestamps:true
})
Roles.index({ "$**": "text" })
module.exports = model('Roles',Roles)