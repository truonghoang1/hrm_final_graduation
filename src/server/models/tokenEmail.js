const {model, Schema} = require('mongoose')

const TokenEmail = new Schema({
    email:String,
    token:String
},{
    timestamps:true
})

module.exports = model('TokenEmail',TokenEmail)