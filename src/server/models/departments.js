const mongoose = require("mongoose")
const {model,Schema} = mongoose
const Departments = new Schema({
    name: {
        type: String,
        require:true
    },
    description: {
        type:String
    },
    position: {
        type:String,
        require:true}
    
},{
    timestamps:true
})
Departments.index({ "$**": "text" })
module.exports = model('Departments',Departments)