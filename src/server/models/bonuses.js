const { model, Schema } = require("mongoose")

const Bonnuses = new Schema({
    name: {
        type: String,
        require: true,
        
    },
    amount: {
        type: Number,
        require:true
    },
    description:String
}, {
    timestamps:true
})
Bonnuses.index({ "$**": "text" })
module.exports= model("Bonuses",Bonnuses)