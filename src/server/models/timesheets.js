const { model, Schema } = require("mongoose")

const TimeSheets = new Schema({
    staff: {
        type:Schema.Types.ObjectId,
        ref:'Employees'
    },
    time_log: {
        type:String,
        require:true
    },
    date_log: {
        type: String,
        require:true
    },
    rol:{
        type:Number,
        enum:[0.5,1,0]
    }
}, {
    timestamps:true
})
TimeSheets.index({ "$**": "text" })
module.exports =model("TimeSheets",TimeSheets)