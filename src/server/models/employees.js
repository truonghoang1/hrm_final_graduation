const { model, Schema } = require("mongoose")

const Employees = new Schema({
   
    full_name:String,
    dob: String,
    email:{
        type:String,
        unique:true
    },
    phone: String,
    current_address: String,
    permanence_address: String,
    cmnd: {
        type: String,
        require:true
    },
    gender: {
        type:String,
        enum:["male","female","other"]
    },
    tax_code: String,
    department: {
        type:Schema.Types.ObjectId,
        ref:"Departments"
    },
    joining_date:String
}, {
    timestamps:true
})

module.exports = model("Employees",Employees)