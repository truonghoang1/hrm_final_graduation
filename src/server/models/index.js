const Employees = require('./employees'),
  Roles = require('./roles'),
  Accounts = require('./accounts'),
  Departments = require('./departments'),
  Salaries = require('./salaries'),
  Documents = require('./documents'),
  Bonuses = require('./bonuses'),
  TimeSheets = require('./timesheets');
const LeaveDays = require('./leaveDay');
const TokenEmail = require("./tokenEmail")

module.exports = {
  LeaveDays,
  Employees,
  Roles,
  Accounts,
  Departments,
  Salaries,
  Documents,
  TimeSheets,
  Bonuses,TokenEmail
};
