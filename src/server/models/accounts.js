const mongoose = require("mongoose")
const { model, Schema } = mongoose
const Accounts = new Schema({
    email: {
        type: String,
        require: true,
        unique:true
    }, 
    password: {
        type: String,
        require:true
    },
    role: {
        type: Schema.Types.ObjectId,
        ref: 'Roles'
    },
   

}, { timestamps: true })

Accounts.index({ "$**": "text" })
module.exports = model('Accounts', Accounts)
