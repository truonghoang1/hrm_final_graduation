const { model, Schema } = require("mongoose")

const LeaveDays = new Schema({
    staff: {
        type:Schema.Types.ObjectId,
        ref:'Employees'
    },
    date: {
        type: Number,
        require:true,
        default:6
    },
}, {
    timestamps:true
})

module.exports =model("LeaveDays",LeaveDays)