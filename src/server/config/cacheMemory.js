const nodeCache = require("node-cache");
const memory = new nodeCache();
const configCache = {
  localhost: {
    enable: false,
    lifetime: 3600,
  },
  development: {
    enable: true,
    lifetime: 3600,
  },
  production: {
    enable: true,
    lifetime: 6 * 3600,
  },
};
const NODE_ENV = process.env.NODE_ENV || "localhost";

function saveToMemCache(key, data, ttl = configCache[NODE_ENV].lifetime) {
//   console.log("🚀 ~ file: memory.js ~ line 20 ~ saveToMemCache ~ ttl", ttl);

  return memory.set(`mem:${key}`, data, ttl);
}
function saveToCache(key, data, ttl = configCache[NODE_ENV].lifetime) {
  // chua co redis

  return saveToMemCache(key, data);
}

function deleteFromMemCache(key) {
  return memory.del(`mem:${key}`);
}
function deleteFromCache(key) {
  // redisCache.del('cache:' + key);
  // redisCache.publish(REDIS_PUB_SUB_CHANNEL, key);
  deleteFromMemCache(key);
}
function cleanAll(){
  return memory.flushAll()
}

async function getFromCache(key) {
  let data = memory.get(`mem:${key}`);
  return data;
}

module.exports = { saveToCache, deleteFromCache, getFromCache, configCache,cleanAll };
