
const { failure } = require('../common/res')
const mongoose = require('mongoose')
const { model } = mongoose
const Accounts = model('Accounts')




const verifyEmail = async (req, res, next) => {
    const testEmail =/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/i;
    if (!testEmail.test(req.body.email)) {

        return failure(res, 400, 'Failed! Email type is not valid!', null)
    }
 
    const verifyUsernameAndEmail = await Accounts.findOne({ email:req.body.email });
    if (verifyUsernameAndEmail) {
        return failure(res, 400, 'Failed!  Email is already in used!')

    }

    next();
};
const verifyPhone = (req, res, next) => {
    const testPhoneNumber = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    if (!testPhoneNumber.test(req.body.mobile)) {
        return failure(res, 400, 'Failed! Phone number type is not valid!', null)

    }
    next()
}
const verifyPassword =(req,res,next)=>{
   const regexPw = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W)/
   
    const {password} =req.body
    if(password.length<8){
        return failure(res,400,"password length is too short")
    }
    if(!regexPw.test(password)){
        return failure(res,400,"password contains at least one digit,one lowercase char, on uppercase char and one special char")
    }
    next()
    
}

module.exports = {
    verifyEmail, verifyPhone,verifyPassword
}