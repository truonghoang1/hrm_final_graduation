const fs = require("fs");
const xlsx = require("xlsx");
const { model } = require("mongoose");
const { failure, success } = require("../common/res");
const moment = require("moment");


const exportFile =async (res, Model,omitted, filter, limit, start) => {
  try {
    console.log("---omit",omitted,filter);
    Model.find(
      { filter },
      omitted,
      { limit, skip: start },
      (err, data) => {
        console.log("---data---",data);
        if (err) return failure(res, 400, "---error--:", err);
        const fileName = `${moment(new Date()).format("YYYY-MM-DD")}.xlsx`;
        let ws_data = [];
        // parse data json => array to array (aoa) and push into ws_data
        data.map(itemObj=>{
         ws_data.push(Object.values(itemObj._doc))
        if(data.indexOf(itemObj)===data.length-1){
         const key = Object.keys(itemObj._doc)
         ws_data.unshift(key)
        }
        })
          // insert aoa into sheet
        let ws = xlsx.utils.aoa_to_sheet(ws_data);
        /* Create workbook */
        let wb = xlsx.utils.book_new();
        /* Add the worksheet to the workbook */
        xlsx.utils.book_append_sheet(wb, ws, fileName);
        /* Write to file */
     const buf = xlsx.write(wb, { type:"buffer", bookType:"xlsx" });
     /* prepare response headers */
     //send response type xlsx
        res.statusCode = 200;
        res.setHeader('Content-Disposition', `attachment; filename=${fileName}`);
        res.setHeader('Content-Type', 'application/vnd.ms-excel');
         res.end(buf);
        //  success(res,200,"success",buf)
      }
    );
  } catch (error) {
    
  }
};
module.exports = exportFile;
