const nodemailer = require("nodemailer")
require("dotenv").config()
const {google} =require("googleapis")
const {OAuth2} =google.auth
const {MAIL_SERVICE_CLIENT_ID,MAIL_SERVICE_CLIENT_SECRET,MAIL_REFRESH_TOKEN,MAIL_SENDER,MAIL_HTTP_DEVELOP_GOOGLE} = process.env
const OAuth2Client = new OAuth2(MAIL_SERVICE_CLIENT_ID,MAIL_SERVICE_CLIENT_SECRET,MAIL_HTTP_DEVELOP_GOOGLE,MAIL_SENDER)
OAuth2Client.setCredentials({
    refresh_token:MAIL_REFRESH_TOKEN
})
const sendMail = async (to,url,text)=>{
   
    const accessToken =await OAuth2Client.getAccessToken()
    
    const smtpTransports = nodemailer.createTransport({
        service:"gmail",
        auth:{
            type:"OAuth2",
            user:MAIL_SENDER,
            clientId:MAIL_SERVICE_CLIENT_ID,
            clientSecret:MAIL_SERVICE_CLIENT_SECRET,
            refreshToken:MAIL_REFRESH_TOKEN,
            accessToken
        }
    })
  
    const mailOptionsResetPassword ={
        from:MAIL_SENDER,
        to,
        subject:" Change password",
        html:`
        <div style="color:#fff;width:700px;margin:auto;background-color: black;">
        <p style="font-size:30px">Thay đổi mật khẩu từ hệ thống HRM</p>
        <p style="font-size:20px">Thay đổi mật khẩu của bạn để đảm bảo an toàn</p>
        <div style="text-decoration: underline;font-size:30px">${text}</div>
       
        </div>
        `
    }
    
    
    
    
    smtpTransports.sendMail(mailOptionsResetPassword,(err,data)=>{
        if(err) return err
        return data
    })
}
module.exports =sendMail