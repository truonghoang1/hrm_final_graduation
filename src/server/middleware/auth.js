
const jwt = require('jsonwebtoken'),
{TokenExpiredError}= jwt,
mongose = require('mongoose'),
{model}= mongose,
Accounts = model('Accounts'),

{failure}= require('../common/res')


const catchError = (err, res) => {
  if (err instanceof TokenExpiredError) {
    return failure(res,401,'Unauthorized! Access Token was expired!',null)
   
  }
  return failure(res,401, 'Unauthorized!',null)
};

const verifyToken = (req, res, next) => {
  
  const authHead = req.headers['token'];
  const token = authHead
  console.log('verifyToken',token);

  if (!token) {
    return failure(res,403, 'No token provided!',null)
  }

  jwt.verify(token, process.env.SECRET_KEY, async (err, decoded) => {
    if (err) {
      catchError(err, res);
      
    }else{
      console.log("decode",decoded);
      req.user = {
        id: decoded.id,
        email: decoded.email,
      };
  
      next();
    }
    
  });
};



const isAdmin = async (req, res, next) => {
 
 console.log('info',req.user);
  const checkRole = await Accounts.findById({_id:req.user.id}).populate('role')
 if(checkRole.role.id_role!==1){
 return failure(res,403,"Unauthorized!",null)
 }else{
  next();
 } 
};
const isAdminOrManager =async (req,res,next)=>{
  const checkRole = await Accounts.findById({_id:req.user.id}).populate('role')
  if(checkRole.role.id_role!==1 && checkRole.role.id_role!==2){
    return failure(res,403,"Unauthorized!",null)
  }else{
    next()
  }
}

// const isSuperAdmin = async (req, res, next) => {
  
// };



const authJwt = {
  verifyToken,
  isAdmin: isAdmin,
  isAdminOrManager
};

module.exports= authJwt;

