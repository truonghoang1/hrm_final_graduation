const {getFromCache,saveToCache,deleteFromCache} = require("../config/cacheMemory") 

const {failure,success} = require("../common/res")
const findAll=(model,res,cacheKey=null,limit,page,populate)=>{
   
   let {ref,title}= populate 
   let skip = (page-1)*limit
    if(!cacheKey){
        model.find({},{__v:0},{limit,skip},(err,data)=>{
            console.log("data ne",data)
            if (err) return failure(res, 400, err, null);
            model.countDocuments(
              {},
              (err2, dataCount) => {
                if (err2) return failure(res, 400, err, null);
                let totalRecord = dataCount;
                let totalPage = Math.ceil(totalRecord / limit);
                success(res, 200, "success", {
                  data,
                  totalPage,
                  page,
                  limit
                });
              }
            );
          }).populate(ref,title)
    }else{
        getFromCache(cacheKey).then(async (dataCache) => {
            console.log("data-cache", dataCache);
            if (dataCache) {
            let  parseDataCache = JSON.parse(dataCache);
              return success(res,200, "success from cache", parseDataCache);
            } else {
              console.log("heelo");
            model.find({},{__v:0},{limit,skip},(err,data)=>{
            console.log("data ne",data)
            if (err) return failure(res, 400, err, null);
            model.countDocuments(
              {},
              (err2, dataCount) => {
                if (err2) return failure(res, 400, err, null);
                let totalRecord = dataCount;
                let totalPage = Math.ceil(totalRecord / limit);
                let dataCache
                if(!data){ 
                  dataCache=undefined}
                else{
                   dataCache = {data,totalPage,page,limit}
                }
               
                saveToCache(cacheKey,JSON.stringify(dataCache))
                success(res, 200, "success",dataCache);
              }
            );
          }).populate(ref,title)
            }

          })
          .catch((err) => {
            return failure(res, "find cache fail:" + err);
          });
    }
}

async function findById(model, options, cacheKey = null) {
    // options={where:{id},attributes}
    if (!cacheKey) return model.findOne(options);
    return new Promise((resolve, reject) => {
      getFromCache(cacheKey)
        .then((data) => {
          if (data) {
            data = JSON.parse(data);
            return resolve(data);
          }
          return model
            .findOne(options)
            .then((data) => {
              if (data) saveToCache(cacheKey, JSON.stringify(data));
              return resolve(data);
            })
            .catch((err) => reject(err));
        })
        .catch((err) => reject(err));
    });
  }

   function search(model,res,options,cacheKey=null,limit,page,populate,keySearch){
    let {ref,title}= populate 
    console.log(ref);
    let skip = (page-1)*limit
     if(!cacheKey){
         model.find(options,{__v:0},{limit,skip},(err,data)=>{
             console.log("data ne",data)
             if (err) return failure(res, 400, err, null);
             model.countDocuments(
              options,
               (err2, dataCount) => {
                 if (err2) return failure(res, 400, err, null);
                 let totalRecord = dataCount;
                 let totalPage = Math.ceil(totalRecord / limit);
                 success(res, 200, "success", {
                   data,
                   totalPage,
                   page,
                   limit,keySearch
                 });
               }
             );
           }).populate(ref,title)
     }else{
         getFromCache(cacheKey).then(async (dataCache) => {
             console.log("data", dataCache);
             if (dataCache) {
             let  parseDataCache = JSON.parse(dataCache);
               return success(res, "success from cache", parseDataCache);
             } else {
 
  model.find(options,{__v:0},{limit,skip},(err,data)=>{
             console.log("data ne",data)
             if (err) return failure(res, 400, err, null);
             model.countDocuments(
                options,
               (err2, dataCount) => {
                 if (err2) return failure(res, 400, err, null);
                 let totalRecord = dataCount;
                 let totalPage = Math.ceil(totalRecord / limit);
                 const dataCache = {data,totalPage,page,limit,keySearch}
                 saveToCache(cacheKey,JSON.stringify(dataCache))
                 success(res, 200, "success",dataCache);
               }
             );
           }).populate(ref,title)
             }
           })
           .catch((err) => {
             return failure(res, "find cache fail:" + err);
           });
     }
  }

 module.exports= {
    search,findAll,findById
  }