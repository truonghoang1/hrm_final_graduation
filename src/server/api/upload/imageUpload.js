const express = require('express')
const router= express.Router()

require("dotenv").config()
const cloudinary = require('cloudinary')
const {failure,success}= require("../../common/res")
cloudinary.config({
    cloud_name:process.env.CLOUD_NAME,
    api_key:process.env.API_KEY,
    api_secret:process.env.API_SECRET,
    secuure:true
})
const messErr = {
    mimetypeErr: "The file is not in the correct format",
    notFoundErr: "Not found image",
    sizeErr: "image is too larger 5mb"
}
const upload_img = async (req,res)=>{
    
    try {
    const {file }= req.files
    console.log(file);
 
     if (!req.files) {
         resErr(res, 404, messErr.notFoundErr, null)
     }
     // file.type !== 'image/jpg' || file.type !== 'image/png' || file.type !== "image/jpeg" ||
     if (file.mimetype !== 'image/jpg' && file.mimetype !== 'image/png' && file.mimetype !== 'image/jpeg') {
         resErr(res, 404, messErr.mimetypeErr, null)
     }
     const imgSizeMb = file.size / (1024 * 1024)
     if (imgSizeMb > 5) {
         resErr(res, 404, messErr.sizeErr, null)
     }
     cloudinary.v2.uploader.upload(file.tempFilePath,{folder:"PreSchool"},(error,data)=>{
         if(error) return failure(res,400,error,null)
 
         success(res,200,"success",{
             public_id:data.public_id,
             url:data.secure_url
         })
        
     })
    } catch (error) {
      return failure(res,500,"Server Error",error)
    }
   
 
 
 
 }
router.route('/image').post(upload_img)

module.exports = router