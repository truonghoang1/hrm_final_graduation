const { model } = require('mongoose')
const TimeSheet = model('TimeSheets')
const Document =model('Documents')
const LeaveDay = model('LeaveDays')
const { success, failure } = require("../../../common/res")
const moment = require('moment')

const formatInputDate = timestamp => moment(timestamp).utcOffset(7).format("YYYY-MM-DD")
const formatTime = timestamp => moment(timestamp).utcOffset(7).format("HH:mm")

const getListTime = (req, res) => {
    const limit = Number.parseInt(req.query.limit) || 11
    const page = Number.parseInt(req.query.page)
    TimeSheet.find(
        { staff: req.user.id }, { __v: 0 }, { limit, skip: (page - 1) * limit }, (err, data) => {
            if (err) return failure(res, 400, "falure" + err)
            TimeSheet.countDocuments({ staff: req.user.id }).exec(
                (err1, count) => {
                    if (err1) return failure(res, 400, "fail" + err)
                    return success(res,200,"success",{data,totalPage:Math.ceil(count/limit),page})
                }
            )

        }
    )
}

const addTime = (req, res) => {
    const staff = req.user.id
    let date = Date.now()
    let time_log = formatTime(date)
    let date_log = formatInputDate(date)
    let rol;
    if(moment(time_log,'hh:mm').isBefore(moment('09:01','hh:mm'))){
        rol=1
    }else if(moment(time_log,'hh:mm').isBefore(moment('13:05','hh:mm'))&&moment(time_log,'hh:mm').isAfter(moment('09:00','hh:mm'))){
        rol=0.5
    }else{
        rol=0
    }
    TimeSheet.findOne({staff,date_log:formatInputDate(date)}).exec((error,data)=>{
        if(error) return failure(res,400,error)
        if(data){
            return success(res,200,"đã log chấm công")
        }else{
            const newTimeSheet = new TimeSheet({ staff, time_log, date_log,rol })
            newTimeSheet.save((err, data) => {
                if (err) return failure(res, 400, "fail" + err)
                success(res, 200, "success", data)
            })
        }
    })
    

}
const listLeaveApplication =(req,res)=>{
   const staff =req.user.id
  
   const limit = Number.parseInt(req.query.limit)||30
   const page = Number.parseInt(req.query.page)||1
//    Document.find({staff}).then(data=>{
//     console.log(data);
//    })
   Document.find({staff},{__v:0},{skip:(page-1)*limit,limit},(err,data)=>{
    console.log("--data--",data);
    if(err) return failure(res,400,"fail:"+err)
    Document.countDocuments({staff}).exec((err1,count)=>{
        if(err1) return failure(res,400,"fail"+err1)
        success(res,200,"success",{data,totalPage:Math.ceil(count/limit),page})
    })
   })
}
const addLeaveApplication=(req,res)=>{
  let {name,type,description,date,rol}= req.body
  let staff = req.user.id
  console.log("🚀 ~ file: timesheet.js:71 ~ addLeaveApplication ~ staff", staff)
// name:string, descript, date,rol:1or 0.5 staff,type:phep_nam\khong luong
LeaveDay.findOne({staff}).then(data=>{
    if(data==null){

        const newApp= new Document({staff,name,type:'khong_luong',description,date,rol})

        newApp.save().then(data2=>{
            return success(res,200,"send success ",data2)
        }).catch(err=>{ return failure(res,400,err)})
    }else{
        if(rol<=data.date&&type=="phep_nam"){
            LeaveDay.findOneAndUpdate({staff},{$set:{date:date-rol}},{new:true},(err2,data)=>{
                if(err2) return failure(res,400,err2)
                const newApp= new Document({staff,name,type,description,date,rol})
    
                newApp.save().then(data2=>{
                    return success(res,200,"send success ",data2)
                }).catch(err=>{ return failure(res,400,err)})
    
            })
        } 
    }
   
}).catch(err=>failure(res,400,err))

 

}
const getDateLeave =(req,res)=>{
    try {
        const staff = req.user.id
        LeaveDay.findOne({staff}).exec((err,data)=>{
            if(err) return failure(res,400,err)
            success(res,200,"success",data)
        })
        
    } catch (error) {
        
    }
}

const listOvertimeApplication =(req,res)=>{
    
}
const addOvertimeApplication=(req,res)=>{

}




module.exports = {getDateLeave, getListTime, addTime,listLeaveApplication,listOvertimeApplication,addLeaveApplication,addOvertimeApplication }