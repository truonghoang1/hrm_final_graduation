const { success, failure } = require("../../../common/res");
require("dotenv").config();
const bcrypt = require("bcryptjs");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const sendMail = require("../../../middleware/sendMail");
const { model } = mongoose,
  Accounts = model("Accounts"),
  Roles=model("Roles"),
  TokenEmail=model('TokenEmail'),
  Employee= model('Employees')
const registerGuest = async (req, res) => {
    //  try {
       const { password, email } = req.body;
       console.log(req.body);
      let checkAccount = await Accounts.findOne({});
      const hashPassword = bcrypt.hashSync(password, 10);
      let objUser;
      console.log(checkAccount);
    if (!checkAccount) {
      //tạo quyền admin trước
      const initRoles =  { level: 1, name: "admin",description:"role wrap all perm" }
      const checkInitRole = await Roles.findOne({ name: "admin" })
      if (checkInitRole) {
        objUser = {
          email,password:hashPassword,role:checkInitRole._id
        }
        console.log("---log",objUser);
        const saveUser = new Accounts(objUser);
         saveUser.save((error, data) => {
       if (error) return failure(res, 400, error, null);
       success(res, 200, "success", data);
     });
      }
       else {
        const newRole = new Roles(initRoles)
        newRole.save((err, role) => {
          if (err) return failure(res, 400, "error role:"+err)
          console.log("---role--",role);
          objUser = {
            email,password:hashPassword,role:role._id
          }
          console.log("0---",objUser);
          const saveUser = new Accounts(objUser);
           saveUser.save((error, data) => {
         if (error) return failure(res, 400, error, null);
         success(res, 200, "success", data);
       });
        })
      
      }
       //lưu account
    
    }else{
      const checkMail = await Accounts.findOne({ email });
      if (checkMail) {
        return failure(res, 400, "Email existed", null);
      } else {
        const user = {password, email };
        const activeAccount = generateAccessAccountRegister(user);
        const url = `${process.env.CLIENT_URL}/user/active/${activeAccount}`;
        sendMail(email, url, activeAccount).then(() => {
          success(res, 200, "verify your email to active");
        }).catch(err=>{return failure(res,400,err)})
       
      }
    }
    // } catch (error) {
    //    return failure(res,500,error,null)
    // }
};
  


  const activeAccount =async (req, res) => {
    try {
        
      const { user } = req.body;
       jwt.verify(user,process.env.TOKEN_SECRET,async(err1,decoded)=>{
        if(err1) return failure(res,400,"error",err1)
        console.log(decoded);
        let roleGuest = await Roles.findOne({ name: "guest" });
        const account = new Accounts({...decoded,password: await bcrypt.hash(decoded.password,10), role: roleGuest._id,});
        account.save((err2, data) => {
          if (err2) return failure(res, 400, "or", err2);
          success(res, 200, "account is actived successfully",data);
        });
      });
     
    } catch (error) {
        return failure(res,500,"Error Server",null)
    }
  };
  const forgotPassword = async (req,res)=>{
    // try {
       
      const {email} =req.body
   const checkEmail = await Accounts.findOne({email})
   if(!checkEmail){
    return failure(res,400,"Email does not  exist")
   }else{
    TokenEmail.findOne({email}).then(data=>{
      console.log("data----",data);
      if(data){
       try {
        const id = jwt.verify(data.token,process.env.TOKEN_SECRET)
        if(id){
          return failure(res,400,"Password đã được gửi, chờ phản hồi từ mail")
        }
       
       } catch (error) {
        console.log(error);
        TokenEmail.findOneAndDelete({email}).then(()=>{
          const idEmail = checkEmail._id.toString()
         
          const generateId =   generateAccessAccountRegister({id:idEmail})
       
           TokenEmail.create({
            email,token:generateId
          }).then(async(data)=>{
            let randomPw =Math.random().toString(36).replace(/[^a-z][0-9][*\W]+/g, '').substring(0,9);
          const hashPw = await bcrypt.hash(randomPw,10)
         Accounts.findByIdAndUpdate({_id:idEmail},{$set:{password:hashPw}},{new:true},(err,data)=>{
          if(err) return failure(res,401,"hệ thống đang trục trăc"+err)
          sendMail(email,"",randomPw).then(()=>{
            success(res,200,"check mail to check password, email expr 5m",null)
          }).catch(error=>{
            return failure(res,400,error)
          })
         })
          })
        }).catch(err=>{return failure(res,400,err)})
     
        // return failure(res,400,error)

       }
      
      }else{
        const id = checkEmail._id.toString()
          //  console.log(typeof id);
          const generateId =   generateAccessAccountRegister({id})
         
           TokenEmail.create({
            email,token:generateId
          }).then(async(data)=>{
            let randomPw =Math.random().toString(36).replace(/[^a-z][0-9][*\W]+/g, '').substring(0,9);
          const hashPw = await bcrypt.hash(randomPw,10)
         Accounts.findByIdAndUpdate({_id:id},{$set:{password:hashPw}},{new:true},(err,data)=>{
          if(err) return failure(res,401,"hệ thống đang trục trăc"+err)
          sendMail(email,"",randomPw).then(()=>{
            success(res,200,"check mail to check password, email expr 5m",null)
          }).catch(error=>{
            return failure(res,400,error)
          })
         })
          })
      }
    })
  //logic: neu mail ton tai lay id cua mail mã hóa json với thời gian là 5 phut trong 5 phut day thục hien doi mail thanh cong
    
    
    
  
   }
    // } catch (error) {
    //   failure(res,500,error,null)
    // }
  }
  const resetPassword =async(req,res)=>{
    try {
      const {token,password} = req.body
      const newPassword = await bcrypt.hash(password,10)
      const id = jwt.verify(token,process.env.TOKEN_SECRET)
      Accounts.findByIdAndUpdate({_id:id.id},{$set:{password:newPassword}},{new:true},(err,data)=>{
        if(err) return failure(res,400,err,null)
        success(res,200,"reset success",data)
      }).select("-password")
    } catch (error) {
      
    }
  }
  const changePw = async(req,res)=>{
    const {newP,password}= req.body
    try {
      Accounts.findById({_id:req.user.id},{__v:0},async(err,data)=>{
        console.log("data",data)
        if(err) return failure(res,400,"fail")
      const decode = await bcrypt.compare(password,data.password)
        if(decode==true){
          let newPW = bcrypt.hashSync(newP,10)
          Accounts.findByIdAndUpdate({_id:req.user.id},{$set:{password:newPW}},{new:true},(err,data)=>{
            if(err) return failure(res,400,err,null)
            success(res,200,"change success",data)
          }).select("-password")
        }
      })
    } catch (error) {
      
    }
  }
  const updateProfile =async (req,res)=>{
      const email = req.user.email
      const fields = req.body
      const checkMail = await Employee.findOne({email:fields?.email})
      if(checkMail){
        return failure("email đã tồn tại trong hệ thống")
      }else{
        Employee.findOne({email},{__v:0},(err,data)=>{
          if(err) return failure(res,400,"fail")
          if(!data) return failure(res,400,"email khong ton tai, điều này có thể do bạn chỉ được cấp tài khoản")
          Employee.findByIdAndUpdate({_id:data._id},{$set:fields},{new:true},(err2,data2)=>{
            if(err2) return failure(res,400,"fail 2")
            success(res,200,"success",data)
          })
        })
      }
 
      
  }
  

  function generateAccessAccountRegister(account) {
    return jwt.sign(account,process.env.TOKEN_SECRET,{ expiresIn: "5m" });
  
  
  }


  module.exports ={
    registerGuest,activeAccount,forgotPassword,resetPassword,changePw,updateProfile
  }