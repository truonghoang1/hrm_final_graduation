const { success, failure } = require("../../../common/res");
require("dotenv").config();
const bcrypt = require("bcryptjs");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const moment = require("moment")
const exportData = require("../../../middleware/exportData")
const { model } = mongoose,
  Salarys = model("Salarys")
  

const getSalaryAccount = (req, res) => {
  try {
    const { limit, page, keySearch } = req.query;
    // let objectSearch ={status:true, $regex: keySearch, $options: 'i' }
    const parseLimit = limit ? Number.parseInt(limit) : 10,
      parserPage = page ? Number.parseInt(page) : 1,
      skip = (parserPage - 1) * parseLimit;

      if (keySearch &&keySearch!=="undefined") {
     
        if( moment(keySearch,"YYYY-MM-DD",true).isValid()){
          let lte= JSON.stringify(Number.parseInt(moment(keySearch,"YYYY-MM-DD").format("X"))+86400) 
          // console.log("🚀 ~ file: student.services.js ~ line 31 ~ getSalarys ~ lte", lte)
          let gte = moment(keySearch,"YYYY-MM-DD").format("X")
          // search(Departments,res,{ $or:[{joining_date:{$gte:gte,$lte:lte}}] },`${department_cache+page}`,parseLimit,parserPage,{ref:"",title:""},keySearch)
          Salarys.find({ $or:[{joining_date:{$gte:gte,$lte:lte}}] },{__v:0},{skip,limit:parseLimit},(err, data) => {
            if (err) return failure(res, 400, err, null);
            Salarys.countDocuments(
              { $or:[{joining_date:{$gte:gte,$lte:lte}}] },
              (err2, dataCount) => {
                if (err2) return failure(res, 400, err, null);
               
                success(res, 200, "success", {
                  data,
                  total:dataCount,
                  page: parserPage,
                });
              }
            );
          }).populate({path:'staff',select:"full_name email"}).populate({path:"bonus",select:"name"})
          
         
        }else{
          Salarys.find({  $text: { $search: keySearch } })
          .limit(parseLimit)
          .skip(skip)
          .populate([{path:'staff',select:"full_name email "},{path:"bonus",select:"name"}])
          .exec((err, data) => {
            if (err) return failure(res, 400, err, null);
            Salarys.countDocuments(
              { status: true, $text: { $search: keySearch } },
              (err2, dataCount) => {
                if (err2) return failure(res, 400, err, null);
                success(res, 200, "success", {
                  data,
                  total:dataCount,
                  page: parserPage,
                });
              }
            );
          });
        }
       
      } else {
        Salarys.find({},{__v:0},{limit:parseLimit,skip},(err, data) => {
          //    console.log("data",data);
          if (err) return failure(res, 400, err, null);
          Salarys.countDocuments({ status: true }, (err2, dataCount) => {
            if (err2) return failure(res, 400, err, null);
            success(res, 200, "success", {
              data,
              totalPage:Math.ceil(dataCount/parseLimit),
              page: parserPage,
            });
          });
        }).populate([{path:'staff',select:"full_name email "},{path:"bonus",select:"name"}])
          
      }
  } catch (error) {
    failure(res, 500, "Server Error", error);
  }
};
const addSalaryAccount = async (req, res) => {
  try {
    const {limit} =req.query
    const fields= req.body;
    console.log("body ne",req.body);
   
        const newSalaryAccount = new Salarys(
            fields
          );
          newSalaryAccount.save(async (err, result) => {
            if (err) return failure(res, 400, err, null);
            const totalRecord = await Salarys.countDocuments({});
            const totalPage =Math.ceil(totalRecord/(Number.parseInt(limit)||10))
            success(res, 200, "success", { totalPage, data: result });
          });
    
   
  } catch (error) {
    failure(res, 500, "Server Error", error);
  }
};

const updateSalaryAccount = (req, res) => {
  const { id } = req.params;
  const options  = req.body;
  Salarys.findByIdAndUpdate(
    { _id: id },
    { $set: options },
    { new: true },
    (err, data) => {
      if (err) return failure(res, 400, err, null);
      success(res, 200, "success", data);
    }
  );
};
const deleteSalaryAccount = (req, res) => {
  try {
    const { id } = req.params;
   
    Salarys.findByIdAndDelete({_id:id},{new:true},(err,data)=>{
      if(err) return failure(res,400,res.err,null)
      success(res,200,"success",data)
    })
  } catch (error) {}
};
const getDetailSalaryAccount = async (req, res) => {
  try {
    const { id } = req.params;

    Salarys.findById({ _id: id }, { __v: 0 }, (err, data) => {
      console.log(data);
      if (err) return failure(res, 400, err, null);
      success(res, 200, "success", data);
    })
  } catch (error) {
    return failure(res, 500, "Error Server", error);
  }
};
const importExelSalary =(req,res)=>{
  try {
    const {file} =req.files
    console.log(file);
    const wb = xlsx.readFile(file.tempFilePath,{type:"buffer",cellDates:true,cellDates:true,cellNF: false,cellText: false})
    const wname =wb.SheetNames[0]
    const ws = wb.Sheets[wname]
    const data =xlsx.utils.sheet_to_json(ws)
    console.log("data",data);
    if(data.length>500){
        return failure(res,400,"error","not more 500 collections/first times")
    }
data.forEach(element => {
Object.assign(element,{joining_date:moment(element.joining_date).format("X")})    
});
    Salarys.insertMany(data,{__v:0},(err,data)=>{
      if(err) return failure(res,400,"error",err)
      Salarys.countDocuments({status:true},(error,result)=>{
          if(error) return failure(res,400,"error",error)
          const totalPage = Math.ceil(result/10)
          success(res,200,"success",{totalPage})
      })
     
    })
  // Students.deleteMany({},{new:true},(err,data)=>{
  //     if(err) return failure(res,400,"error",err)
  //     success(res,200,"success",data)
  // })
  
  } catch (error) {
      
  }
}
const exportSalary=async (req,res)=>{
  try {
  //  console.log(req.body);
   const {start,end} = req.query
   let parseStart = Number.parseInt(start)
   let parseEnd = Number.parseInt(end)
     if(parseEnd<parseStart) return failure(res,400,"error","end point data is not smaller start point ")
     exportData(res,Salarys,req.body,{status:true},parseEnd,parseStart)
 } catch (error) {
   
 }
}



module.exports = {
  getDetailSalaryAccount,
  getSalaryAccount,
  updateSalaryAccount,
  deleteSalaryAccount,
  addSalaryAccount,importExelSalary,exportSalary
};
