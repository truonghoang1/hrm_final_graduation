const { success, failure } = require("../../../common/res");
require("dotenv").config();
const bcrypt = require("bcryptjs");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const sendMail = require("../../../middleware/sendMail");
const moment = require("moment");
const { model } = mongoose,
  Accounts = model("Accounts"),

  Roles = model("Roles");

const signup = async (req, res) => {
  const {email, password, role,staff } = req.body;
  const hashPassword = bcrypt.hashSync(password, 10);
  let objUser;
  if (password)
    if (role) {
      objUser = {
        email,
        password: hashPassword,
        role,
        staff
      };
      const saveUser = new Accounts(objUser);
      saveUser.save((error, data) => {
        if (error) return failure(res, 400, error, null);
        success(res, 200, "success", data);
      });
    }else{
      return failure(res,400,"give permission")
    }
  //lưu account
 
};
const signin = async (req, res) => {
  try {
    const { password, email } = req.body;
    Accounts.findOne({ email }, async (err, data) => {
      if (err) return failure(res, 400, err, null);
      if (!data) {
        return failure(res, 404, "Account does not exist !");
      } else {
        const decode = await bcrypt.compare(password, data.password);
        if (decode === true) {
          const token = generateAccessToken({
            email: req.body.email,
            id: data._id,
          });
          console.log(data);
          
          success(res, 200, "success", { token, role: data.role.level });
        } else {
          return failure(res, 400, "Password is not true", null);
        }
      }
    }).populate("role","level");
  } catch (error) {
    return failure(res, 500, "Server Error", null);
  }
};

const forgotPassword = async (req, res) => {
  try {
    const { email } = req.body;
    const checkEmail = await Accounts.findOne({ email });
    if (Object.entries(checkEmail).length <= 0) {
      return failure(res, 400, "Email does not  exist");
    } else {
      const generateId = generateAccessAccountRegister(checkEmail._id);
      const url = `${process.env.CLIENT_URL}/user/reset/${generateId}`;
      sendMail(email, url, generateId);
      success(res, 200, "check mail to reset password", null);
    }
  } catch (error) {}
};
const resetPassword = async (req, res) => {
  try {
    const { token, password } = req.body;
    const newPassword = await bcrypt.hash(password, 10);
    const id = jwt.verify(token, process.env.TOKEN_SECRET);
    Accounts.findByIdAndUpdate(
      { _id: id },
      { $set: { password: newPassword } },
      { new: true },
      (err, data) => {
        if (err) return failure(res, 400, err, null);
        success(res, 200, "reset success", data);
      }
    ).select("- password");
  } catch (error) {}
};
const offline =(req,res)=>{
  const {id}= req.params
  Accounts.findByIdAndUpdate({_id:id},{online:false},{new:true}).exec((err,data)=>{
    if(err) return failure(res,400,err,{})
    success(res,200,"offline",data)
  })
}

const statistial = async (req, res) => {
  // try {
  const { filter } = req.query;
  const now = moment(new Date()).format("x");
  const totalStudents = await Students.countDocuments({
    $and: [{ status: true }, { joining_date: { $lte: now } }],
  });
  const totalDepartment = await Departments.countDocuments({
    department_start_date: { $lte: now } 
  });
  //chart
  //get current year and current month
  const current = moment(new Date()).format("YYYY-MM").split("-");
  const currentMonth = Number.parseInt(current[1]);
  const currentYear = Number.parseInt(current[0]);
  const countRevenseStudentOfYear = [];
  const countRevenseTeacherOfYear=[]
  const countMaleStudentManyYear = [];
  const countFemaleStudentManyYear=[]
   let venenue;
  //caculate revenue
  //$gte : greater than or equal
  //$lte: less than or equal
 Promise.all([ await ExpensesAccounts.find({
    purchase_date: {  $gte: moment(`${currentYear}-${currentMonth}-01`, "YYYY-MM-DD").format("X"),
    $lte: moment(`${currentYear}-${currentMonth}-30`, "YYYY-MM-DD").format("X"),}
  }),
 await SalaryAccounts.find({
    $and:[{status:true}, {
     purchase_date: {  $gte: moment(`${currentYear}-${currentMonth}-01`, "YYYY-MM-DD").format("X"),
     $lte: moment(`${currentYear}-${currentMonth}-30`, "YYYY-MM-DD").format("X"),}
   }]
   }),await FeesAccounts.find({paid_date:{  $gte: moment(`${currentYear}-${currentMonth}-01`, "YYYY-MM-DD").format("X"),
   $lte: moment(`${currentYear}-${currentMonth}-30`, "YYYY-MM-DD").format("X")
 }})]).then(
  totalAmount=>{
    console.log(totalAmount);
    const totalSalary =totalAmount[1].map(item=>{
      return item.amount
    }).reduce((pre,next)=>{
     return pre+next  
},0)
    const totalExpense =totalAmount[0].map(item=>{
      return item.expense_amount
    }).reduce((pre,next)=>{
       return pre+next  
  },0)
    const totalFees =totalAmount[2].map(item=>{
      return item.fees_amount
    }).reduce((pre,next)=>{
      return  pre+next  
      },0)
   venenue =totalFees-(totalExpense+totalSalary)
    
  }
 )
//count revense student and teacher
  for (let i = 1; i <= currentMonth; i++) {
    const countRevenseStudentMonth = await Students.countDocuments({
          joining_date: {
            $gte: moment(`${currentYear}-${i}-01`, "YYYY-MM-DD").format("X"),
            $lte: moment(`${currentYear}-${i}-30`, "YYYY-MM-DD").format("X"),
          },
    });
    countRevenseStudentOfYear.push(countRevenseStudentMonth);
  }
  for (let i = 1; i <= currentMonth; i++) {
    const countRevenseTeacherMonth = await Teachers.countDocuments({
          joining_date: {
            $gte: moment(`${currentYear}-${i}-01`, "YYYY-MM-DD").format("X"),
            $lte: moment(`${currentYear}-${i}-30`, "YYYY-MM-DD").format("X"),
          },
    });
    countRevenseTeacherOfYear.push(countRevenseTeacherMonth);
  }
  //query quality student male and female year
  for (let j = currentYear; j > currentYear - 10; j--) {
    const countYear = await Students.countDocuments({
      $and:[{gender:"male", joining_date: {
        $gte: moment(`${j}-01-01`, "YYYY-MM-DD").format("X"),
        $lte: moment(`${j}-12-30`, "YYYY-MM-DD").format("X"),
      }}]
    });
    countMaleStudentManyYear.unshift(countYear);
  }
  for (let j = currentYear; j > currentYear - 10; j--) {
    const countYear = await Students.countDocuments({
      $and:[{gender:"female", joining_date: {
        $gte: moment(`${j}-01-01`, "YYYY-MM-DD").format("X"),
        $lte: moment(`${j}-12-30`, "YYYY-MM-DD").format("X"),
      }}]
    });
    countFemaleStudentManyYear.unshift(countYear);
  }
  success(res, 200, "success", {
    totalStudents,
    totalDepartment,
    totalVevenue:venenue,
    revenseStudentsMonth: countRevenseStudentOfYear,
    revenseTeacherMonth:countRevenseTeacherOfYear,
    maleStudentManyYear: countMaleStudentManyYear,
    femaleStudentmanyYear:countFemaleStudentManyYear
  });

  // } catch (error) {

  // }
};

function generateAccessToken(username) {
  return jwt.sign(username, process.env.TOKEN_SECRET, { expiresIn: "1800s" });
}
function generateAccessAccountRegister(account) {
  return jwt.sign(account, process.env.TOKEN_SECRET, { expiresIn: "60m" });
}

module.exports = {
  signup,
  signin,
  resetPassword,
  forgotPassword,
  statistial,
  offline
};
