const { success, failure } = require("../../../common/res");
require("dotenv").config();
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const moment = require("moment")
const { model } = mongoose,
  Documents = model("Documents")
  const {findAll,findById,search} = require("../../../utils/query")
  const bonus_cache ="bonus_page"
  const bonus_id_cache = "bonus_id"
  const {deleteFromCache,cleanAll} = require("../../../config/cacheMemory")
  const getListDocuments =(req,res)=>{
    try {
      
        
    } catch (error) {
        
    }
  }

  const addDocuments = (req,res)=>{
    try {
        const fields = req.body
            const newDocuments = new Documents(fields)
            newDocuments.save((err,Documents)=>{
                if(err) return failure(res,400,err)
                success(res,200,"success",Documents)
            })
  
    } catch (error) {
        failure(res,500,"Server error")
    }
  }

  const updateDocuments =(req,res)=>{
    try {
        const {id} = req.params
        const fields = req.body
        Documents.findByIdAndUpdate({_id:id},{$set:fields},{new:true}).exec((err,data)=>{
            if(err) return failure(res,400,err)
            
            success(res,200,"success",data)
        })
        
    } catch (error) {
        
    }
  }
  const deleteDocuments =(req,res)=>{
    try {
        const {id} = req.params
        Documents.findByIdAndDelete({_id:id},(err,data)=>{
            if(err) return failure(res,400,err)
           success(res,200,"success",data)
        })
        
    } catch (error) {
        return failure(res,500,"Error Server")
    }
  }
  module.exports = {
    getListDocuments,addDocuments,updateDocuments,deleteDocuments
  }