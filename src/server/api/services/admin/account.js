const { success, failure } = require("../../../common/res");
require("dotenv").config();
const bcrypt = require("bcryptjs");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const { config } = require("dotenv");
const moment = require("moment")
const {findAll,findById,search} = require("../../../utils/query")
const { model } = mongoose,
  Accounts = model("Accounts")
const account_cache ="account_page"
const account_id_cache = "account_id"
const {deleteFromCache,cleanAll} = require("../../../config/cacheMemory")

const getAccount = (req, res) => {
  try {
    const { limit, page, keySearch } = req.query;
    // let objectSearch ={status:true, $regex: keySearch, $options: 'i' }
    const parseLimit = limit ? Number.parseInt(limit) : 10,
          parserPage = page ? Number.parseInt(page) : 1
    if (keySearch &&keySearch!=="undefined") {
      search(Accounts,res,{$text:{ $search: keySearch }},`${account_cache+page}`,parseLimit,parserPage,{ref:"role",title:"name level"},keySearch)
    } else {
      findAll(Accounts,res,`${account_cache+page}`,parseLimit,parserPage,{ref:"role",title:"name level"})
    }
  } catch (error) {
    failure(res, 500, "Server Error", error);
  }
};
const addAccount = async (req, res) => {
  try {
    const {limit} =req.query
    const fields = req.body;
        const newAccount = new Accounts(fields);
          newAccount.save(async (err, result) => {
            if (err) return failure(res, 400, err, null);
            const totalRecord = await Accounts.countDocuments({});
            let pageIndex = Math.ceil(totalRecord/Number.parseInt(limit))
           deleteFromCache(account_cache+pageIndex)
            success(res, 200, "success", { total:totalRecord, data: result });
          });
   
  }
   catch (error) {
    failure(res, 500, "Server Error", error);
  }
};

const updateAccount = (req, res) => {
  let  idDepatment  = req.params.id;
  const {id,...rest}  = req.body;
    Accounts.findByIdAndUpdate(
      { _id: idDepatment },
      { $set: {...rest} },
      { new: true },
      (err, data) => {
        if (err) return failure(res, 400, err, null);
        cleanAll()
        success(res, 200, "update success", data);
      }
    );
};
const deleteAccount = (req, res) => {
  try {
    const { id } = req.params;
    Accounts.findByIdAndDelete({_id:id},{new:true},(err,data)=>{
      if(err) return failure(res,400,res.err,null)
      cleanAll()
      success(res,200,"success",data)
    })
  } catch (error) {}
};
const getDetailAccount = async (req, res) => {
  try {
    const { id } = req.params;
    findById(Accounts,{ _id: id },account_id_cache+id).then((data)=>{
       success(res,200,"success",data)
    })
  } catch (error) {
    return failure(res, 500, "Error Server", error);
  }
};

module.exports = {
  getDetailAccount,
  getAccount,
  updateAccount,
  deleteAccount,
  addAccount,
};
