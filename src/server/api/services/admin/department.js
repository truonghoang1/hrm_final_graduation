const { success, failure } = require("../../../common/res");
require("dotenv").config();
const bcrypt = require("bcryptjs");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const { config } = require("dotenv");
const moment = require("moment")
const { model } = mongoose,
  Departments = model("Departments"),
  Roles = model("Roles"),
  Account = model("Accounts");
  const {findAll,findById,search} = require("../../../utils/query")
  const department_cache ="department_page"
  const department_id_cache = "department_id"
  const {deleteFromCache,cleanAll} = require("../../../config/cacheMemory")
const getDepartment = (req, res) => {
  try {
    const { limit, page, keySearch } = req.query;
    // let objectSearch ={status:true, $regex: keySearch, $options: 'i' }
    const parseLimit = limit ? Number.parseInt(limit) : 10,
      parserPage = page ? Number.parseInt(page) : 1,
      skip = (parserPage - 1) * parseLimit;

    if (keySearch &&keySearch!=="undefined") {
      search(Departments,res,{$text:{ $search: keySearch }},`${department_cache+page}`,parseLimit,parserPage,{ref:"",title:""},keySearch)
     
    } else {
      findAll(Departments,res,`${department_cache+page}`,parseLimit,parserPage,{ref:"",title:""})
    }
  } catch (error) {
    failure(res, 500, "Server Error", error);
  }
};
const addDepartment = async (req, res) => {
  try {
    const {limit} = req.query
    const fields = req.body;
        const newDepartment = new Departments(fields);
          newDepartment.save(async (err, result) => {
            if (err) return failure(res, 400, err, null);
            const totalRecord = await Departments.countDocuments({});
            let pageIndex = Math.ceil(totalRecord/Number.parseInt(limit))
            deleteFromCache(department_cache+pageIndex)
            success(res, 200, "success", { total:totalRecord, data: result });
          });
   
  }
   catch (error) {
    failure(res, 500, "Server Error", error);
  }
};

const updateDepartment = (req, res) => {
  let  idDepatment  = req.params.id;
  const {id,...rest}  = req.body;

    Departments.findByIdAndUpdate(
      { _id: idDepatment },
      { $set: {...rest} },
      { new: true },
      (err, data) => {
        if (err) return failure(res, 400, err, null);
        cleanAll()
        success(res, 200, "update success", data);
      }
    );
};
const deleteDepartment = (req, res) => {
  try {
    const { id } = req.params;
    Departments.findByIdAndDelete({_id:id},{new:true},(err,data)=>{
      if(err) return failure(res,400,res.err,null)
      cleanAll()
      success(res,200,"success",data)
    })
  } catch (error) {}
};
const getDetailDepartment = async (req, res) => {
  try {
    const { id } = req.params;
    findById(Departments,{_id:id},department_cache+id).then(data=>{
      success(res,200,"success",data)
    })
    // Departments.findById({ _id: id }, { __v: 0 }, (err, data) => {
    //   console.log(data);
    //   if (err) return failure(res, 400, err, null);
    //   success(res, 200, "success", data);
    // })
  } catch (error) {
    return failure(res, 500, "Error Server", error);
  }
};

module.exports = {
  getDetailDepartment,
  getDepartment,
  updateDepartment,
  deleteDepartment,
  addDepartment,
};
