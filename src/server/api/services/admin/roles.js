const { success, failure } = require("../../../common/res");
require("dotenv").config();
const bcrypt = require("bcryptjs");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const { config } = require("dotenv");
const moment = require("moment")
const { model } = mongoose,
  Roles = model("Roles"),
  Account = model("Accounts");
  const {findAll,findById,search} = require("../../../utils/query")
  const role_cache ="role_page"
  const role_id_cache = "role_id"
  const {deleteFromCache,cleanAll} = require("../../../config/cacheMemory")
const getRole = (req, res) => {
  try {
    const { limit, page, keySearch } = req.query;
    // let objectSearch ={status:true, $regex: keySearch, $options: 'i' }
    const parseLimit = limit ? Number.parseInt(limit) : 10,
      parserPage = page ? Number.parseInt(page) : 1,
      skip = (parserPage - 1) * parseLimit;

    if (keySearch &&keySearch!=="undefined") {
      search(Roles,res,{$text:{ $search: keySearch }},`${role_cache+page}`,parseLimit,parserPage,{ref:"",title:""},keySearch)
      // Roles.find({ status: true, $text: { $search: keySearch } })
      //   .limit(parseLimit)
      //   .skip(skip)
      //   .exec((err, data) => {
      //     if (err) return failure(res, 400, err, null);
      //     Roles.countDocuments(
      //       { status: true, $text: { $search: keySearch } },
      //       (err2, dataCount) => {
      //         if (err2) return failure(res, 400, err, null);
      //         let totalRecord = dataCount;
      //         let totalPage = Math.ceil(totalRecord / parseLimit);
      //         success(res, 200, "success", {
      //           data,
      //           totalPage,
      //           page: parserPage,
      //         });
      //       }
      //     );
      //   });
    } else {
      findAll(Roles,res,`${role_cache+page}`,parseLimit,parserPage,{ref:"",title:""})
      // Roles.find({ status: true })
      //   .limit(parseLimit)
      //   .skip(skip)
      //   .exec((err, data) => {
      //     if (err) return failure(res, 400, err, null);
      //     Roles.countDocuments({ status: true }, (err2, dataCount) => {
      //       if (err2) return failure(res, 400, err, null);
      //       let totalRecord = dataCount;
      //       let totalPage = Math.ceil(totalRecord / parseLimit);
      //       success(res, 200, "success", {
      //         data,
      //         totalPage,
      //         page: parserPage,
      //       });
      //     });
      //   });
    }
  } catch (error) {
    failure(res, 500, "Server Error", error);
  }
};
const addRole = async (req, res) => {
  try {
    const {limit} =req.query
    const fields = req.body;
        const newRoles = new Roles(fields);
          newRoles.save(async (err, result) => {
            if (err) return failure(res, 400, err, null);
            const totalRecord = await Roles.countDocuments({});
            let pageIndex = Math.ceil(totalRecord/Number.parseInt(limit))
            // deleteFromCache(role_cache+pageIndex)
            cleanAll()
            success(res, 200, "success", { total:totalRecord, data: result });
          });
   
  }
   catch (error) {
    failure(res, 500, "Server Error", error);
  }
};

const updateRole = (req, res) => {
  let  idDepatment  = req.params.id;
  const {id,...rest}  = req.body;

    Roles.findByIdAndUpdate(
      { _id: idDepatment },
      { $set: {...rest} },
      { new: true },
      (err, data) => {
        if (err) return failure(res, 400, err, null);
        cleanAll()
        success(res, 200, "update success", data);
      }
    );
};
const deleteRole = (req, res) => {
  try {
    const { id } = req.params;
    Roles.findByIdAndDelete({_id:id},{new:true},(err,data)=>{
      if(err) return failure(res,400,res.err,null)
      cleanAll()
      success(res,200,"success",data)
    })
  } catch (error) {}
};
const getDetailRole = async (req, res) => {
  try {
    const { id } = req.params;
    Roles.findById({ _id: id }, { __v: 0 }, (err, data) => {
      console.log(data);
      if (err) return failure(res, 400, err, null);
      success(res, 200, "success", data);
    })
  } catch (error) {
    return failure(res, 500, "Error Server", error);
  }
};

module.exports = {
  getDetailRole,
  getRole,
  updateRole,
  deleteRole,
  addRole,
};
