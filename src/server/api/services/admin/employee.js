const { success, failure } = require('../../../common/res');
require('dotenv').config();
const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');
const fs = require('fs');
const xlsx = require('xlsx');
const path = require('path');
const jwt = require('jsonwebtoken');
const moment = require('moment');

const exportData = require('../../../middleware/exportData');
const { model } = mongoose,
  Employees = model('Employees'),
  Roles = model('Roles'),
  Account = model('Accounts'),
LeaveDay = model('LeaveDays');
const {findAll,findById,search} = require("../../../utils/query")
const employee_cache ="employee_page"
const employee_id_cache = "employee_id"

const {deleteFromCache,cleanAll} = require("../../../config/cacheMemory")
const getEmployees = async (req, res) => {
  // try {
    const { limit, page, keySearch } = req.query;
   
    const parseLimit = limit ? Number.parseInt(limit) : 10,
      parserPage = page ? Number.parseInt(page) : 1,
      skip = (parserPage - 1) * parseLimit;
      let lte = JSON.stringify(
        Number.parseInt(moment(keySearch, 'YYYY-MM-DD').format('X')) + 86400
      );
      // console.log("🚀 ~ file: employee.services.js ~ line 31 ~ getEmployees ~ lte", lte)
      let gte = moment(keySearch, 'YYYY-MM-DD').format('X');
    if (keySearch && keySearch !== 'undefined') {
      if (moment(keySearch, 'YYYY-MM-DD', true).isValid()) {
       
        search(Employees,res,{
          $or: [{ dob: { $gte: gte, $lte: lte } }, { joining_date: { $gte: gte, $lte: lte } }],
        },null,parseLimit,parserPage,{ref:"department",title:""},keySearch)

        // Employees.find({
        //   $or: [{ dob: { $gte: gte, $lte: lte } }, { joining_date: { $gte: gte, $lte: lte } }],
        // })
        //   .limit(parseLimit)
        //   .skip(skip)
        //   .populate('department')
        //   .exec((err, data) => {
        //     if (err) return failure(res, 400, err, null);
            
        //     Employees.countDocuments(
        //       {
        //         $or: [
        //           { dob: { $gte: gte, $lte: lte } },
        //           { joining_date: { $gte: gte, $lte: lte } },
        //         ],
        //       },
        //       (err2, dataCount) => {
        //         if (err2) return failure(res, 400, err, null);
        //         let totalRecord = dataCount;
        //         let totalPage = Math.ceil(totalRecord / parseLimit);
        //         success(res, 200, 'success', {
        //           data,
        //           totalPage,
        //           page: parserPage,
        //         });
        //       }
        //     );
        //   });
      } else {
        console.log(keySearch);
        search(Employees,res,{
          $text:{ $search: keySearch }
        },null,parseLimit,parserPage,{ref:"department",title:""},keySearch)
     
      }
    } else {
      findAll(Employees,res,`${employee_cache+page}`,parseLimit,parserPage,{ref:"department",title:"name position"})
      
    }
  // } catch (error) {
  //   failure(res, 500, 'Server Error', error);
  // }
};

const addEmployee = async (req, res) => {
  // try {
    const fields = req.body;
    console.log(fields);
   let roleUser = await Roles.findOne({level:3})
    const accountEmployee = new Account({
      email:fields.email,
      password: await bcrypt.hash('Truong652000.',10),
      role:roleUser._id
    });
     if(fields.department){
      accountEmployee.save((err,account)=>{
        console.log("acc",account);
        if(err) return failure(res,400,"err email can be email used")
        console.log("req...",req.body);
          const newEmployee = new Employees(fields);
          newEmployee.save().then((value)=>{
            console.log("valuee---",value);
            if(value){  
                  const day = new LeaveDay({staff:value._id})
                  day.save().then((data)=>{
                    Employees.countDocuments().then(total=>{
                      cleanAll()
                      return success(res,200,"add success",{total,data:value})
                    })
                  }).catch((error)=>{ return failure(res,400,error)})
                 
            }else{
              return failure(res,400,"error add")
            }
          }).catch((err)=>{ return failure(res,500,err)})
        }
      );
     }else{
      return failure(res,400,"Chose deparment")
     }
      
   
    
  // } catch (error) {
  //   failure(res, 500, 'Server Error', error);
  // }
};

const updateEmployee = (req, res) => {
  const { id } = req.params;
  const  options  = req.body;
    Employees.findByIdAndUpdate(
      { _id: id },
      { $set: options },
      { new: true },
      (err, data) => {
        if (err) return failure(res, 400, err, null);
        cleanAll()
        success(res, 200, "success", data);
      }
    );
  
};
const deleteEmployee = (req, res) => {
  try {
  const { id } = req.params;
  Employees.findByIdAndDelete({ _id: id }, { news: true }, (err, data) => {
    if (err) return failure(res, 400, err, null);
    console.log(data);
    Account.findOneAndDelete(
      { email: data.email },
      { new: true },
      (error2, data2) => {
        if (error2) return failure(res, 400, "err", error2);
        cleanAll()
        success(res, 200, "success", data);
      }
    );
  });
  } catch (error) {
    return failure(res,500,"server er"+error)

  }
};
const getDetailEmployee = async (req, res) => {
  try {
  const { id } = req.params;
//  findById(Employees,{_id:id},employee_id_cache+id).then((data)=>{
//   success(res,200,"success",data)
//  })
    Employees.findById({ _id: id }, { __v: 0 }, (err, data) => {
      if (err) return failure(res, 400, err, null);
      success(res, 200, "success", data);
    }).populate("department","name position");
  } catch (error) {
    return failure(res, 500, "Error Server", error);
  }
};

const importExelEmployees = async (req, res) => {
  // try {
      const { file } = req.files;
      const wb = xlsx.readFile(file.tempFilePath, {
        type: "buffer",
        cellDates: true,
      });
      const wname = wb.SheetNames[0];
      const ws = wb.Sheets[wname];
      const data = xlsx.utils.sheet_to_json(ws);
      console.log("🚀 ~ file: employee.js:207 ~ importExelEmployees ~ data", data)
      if (data.length > 500) {
        return failure(res, 403, "error", "not more 500 collections/first times");
      }
      console.log("data----",data);
      let arrAccount =[];
      let roleEmployee 
      const role = await Roles.findOne({ level: 3 });
      if(!role){
         const newRole = new Roles({name:"employee",level:3})
         newRole.save().then((data)=>{
              roleEmployee=data._id
         }).catch(err=>{return failure(res,400,err)})
      }else{
        roleEmployee=role._id
      }
     const newPassword = await bcrypt.hash("Hrm652000.", 10)
      data.forEach((element) => {
        if(Object.hasOwnProperty("email")){
          return failure(res,400,"Email chưa được thêm")
        }else{
          console.time("map")
          arrAccount.push({
              email: element.email,
               password:newPassword,
              role: roleEmployee,
            })
          console.timeEnd("map")
        }
          
      });
      
      Promise.all([Account.insertMany(arrAccount),Employees.insertMany(data)]).then(result=>{
          if(result){
 Employees.countDocuments({},(err,data2)=>{
          success(res, 200, "success", { totalPage:Math.ceil(data2/10) });
        })

          }

      }).catch(err=>{return failure(res,400,err)})
    // Employees.deleteMany({},{new:true},(err,data)=>{
    //     if(err) return failure(res,400,"error",err)
    //     Account.deleteMany({},{new:true},(err1,data1)=>{
    //         if(err) return failure(res,400,"error",err)
    //        success(res,200,"success",{data,data1})
    //     })
    // })
  // } catch (error) {

  // }
};
const exportEmployee = async (req, res) => {
   try {
    console.log(req.body);
    const {start,end} = req.query
    let parseStart = Number.parseInt(start)
    let parseEnd = Number.parseInt(end)
      if(parseEnd<parseStart) return failure(res,400,"error","end point data is not smaller start point ")
      exportData(res,Employees,req.body,{},parseEnd,parseStart)
    } catch (error) {
      return failure(res,500,error)
    }
};
module.exports = {
  getEmployees,
  addEmployee,
  updateEmployee,
  deleteEmployee,
  getDetailEmployee,
  importExelEmployees,
  exportEmployee,
};
