const { success, failure } = require('../../../common/res');
require('dotenv').config();
const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');
const fs = require('fs');
const xlsx = require('xlsx');
const path = require('path');
const jwt = require('jsonwebtoken');
const moment = require('moment');

const exportData = require('../../../middleware/exportData');
const { model } = mongoose,
  Document = model('Documents'),
  Roles = model('Roles'),
  Account = model('Accounts');

const getListLeaveApp = async (req, res) => {
   Document.find({complete:false},{__v:0}).populate("staff","email").then(data=>{
    success(res,200,"success",data)
   }).catch(err=>failure(res,400,err))
};


const updateStatusDocoment = (req, res) => {
  const { id } = req.params;
  
    Document.findByIdAndUpdate(
      { _id: id },
      { $set: {complete:true} },
      { new: true },
      (err, data) => {
        if (err) return failure(res, 400, err, null);
        success(res, 200, "success", data);
      }
    );
  
};




// const exportTimeSheet = async (req, res) => {
//    try {
//     console.log(req.body);
//     const {start,end} = req.query
//     let parseStart = Number.parseInt(start)
//     let parseEnd = Number.parseInt(end)
//       if(parseEnd<parseStart) return failure(res,400,"error","end point data is not smaller start point ")
//       exportData(res,TimeSheets,req.body,{},parseEnd,parseStart)
//     } catch (error) {
//       return failure(res,500,error)
//     }
// };
module.exports = {
  getListLeaveApp,
  updateStatusDocoment,
  // exportTimeSheet,
};
