const { success, failure } = require("../../../common/res");
require("dotenv").config();
const bcrypt = require("bcryptjs");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const { config } = require("dotenv");
const moment = require("moment")
const { model } = mongoose,
  Bonuss = model("Bonuses")
  const {findAll,findById,search} = require("../../../utils/query")
  const bonus_cache ="bonus_page"
  const bonus_id_cache = "bonus_id"
  const {deleteFromCache,cleanAll} = require("../../../config/cacheMemory")

const getBonus = (req, res) => {
  try {
    const { limit, page, keySearch } = req.query;
    // let objectSearch ={status:true, $regex: keySearch, $options: 'i' }
    const parseLimit = limit ? Number.parseInt(limit) : 10,
      parserPage = page ? Number.parseInt(page) : 1,
      skip = (parserPage - 1) * parseLimit;

    if (keySearch &&keySearch!=="undefined") {
      search(Bonuss,res,{$text:{ $search: keySearch }},`${bonus_cache+page}`,parseLimit,parserPage,{ref:"",title:""},keySearch)
      
    } else {
      findAll(Bonuss,res,`${bonus_cache+page}`,parseLimit,parserPage,{ref:"",title:""})
    }
  } catch (error) {
    failure(res, 500, "Server Error", error);
  }
};
const addBonus = async (req, res) => {
  try {
    const {limit} =req.query
    const fields = req.body;
        const newBonus = new Bonuss(fields);
          newBonus.save(async (err, result) => {
            if (err) return failure(res, 400, err, null);
            const totalRecord = await Bonuss.countDocuments({});
            let pageIndex = Math.ceil(totalRecord/Number.parseInt(limit))
            deleteFromCache(bonus_cache+pageIndex)
            success(res, 200, "success", { total:totalRecord, data: result });
          });
   
  }
   catch (error) {
    failure(res, 500, "Server Error", error);
  }
};

const updateBonus = (req, res) => {
  let  idDepatment  = req.params.id;
  const {id,...rest}  = req.body;
    Bonuss.findByIdAndUpdate(
      { _id: idDepatment },
      { $set: {...rest} },
      { new: true },
      (err, data) => {
        if (err) return failure(res, 400, err, null);
        cleanAll()
        success(res, 200, "update success", data);
      }
    );
};
const deleteBonus = (req, res) => {
  try {
    const { id } = req.params;
    Bonuss.findByIdAndDelete({_id:id},{new:true},(err,data)=>{
      if(err) return failure(res,400,res.err,null)
      cleanAll()
      success(res,200,"success",data)
    })
  } catch (error) {}
};
const getDetailBonus = async (req, res) => {
  try {
    const { id } = req.params;
    findById(Bonuss,{ _id: id },bonus_id_cache+id).then((data)=>{
      success(res,200,"success",data)
    })
   
  } catch (error) {
    return failure(res, 500, "Error Server", error);
  }
};

module.exports = {
  getDetailBonus,
  getBonus,
  updateBonus,
  deleteBonus,
  addBonus,
};
