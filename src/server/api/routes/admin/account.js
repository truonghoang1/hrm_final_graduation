const express = require('express')
const router = express.Router()

const {isAdmin,isAdminOrManager,verifyToken} = require('../../../middleware/auth')
const {verifyEmail} =require("../../../middleware/verify")
const accountServices = require("../../services/admin/account")
router.route('/').
post(
    // verifyToken,isAdminOrManager,
    accountServices.addAccount)
.get(
    // verifyToken,
    accountServices.getAccount)
router.route('/:id')
.put(
    // verifyToken,isAdminOrManager,
    accountServices.updateAccount)
.delete(
    // verifyToken,isAdmin,
    accountServices.deleteAccount)
.get(
    // verifyToken,
    accountServices.getDetailAccount)
module.exports = router