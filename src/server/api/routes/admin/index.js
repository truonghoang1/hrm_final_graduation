const express = require('express')
const router= express.Router()
const userRoute = require('./user')
const roleRoute = require('./roles')
const employeeRoute = require("./employee")
const timesheetRoute = require("./timesheet")
const departmentRoute = require('./department')
const bonusRoute = require("./bonus")
const salaryRoute = require("./salary")
const accountRoute = require("./account")


router.use('/api/user',userRoute)
router.use('/api/role',roleRoute)
router.use('/api/employee',employeeRoute)
router.use('/api/bonus',bonusRoute)
router.use("/api/department",departmentRoute)
router.use("/api/salary",salaryRoute)
router.use("/api/timesheet",timesheetRoute)
router.use("/api/account",accountRoute)


module.exports = router