const express = require('express');
const router = express.Router();

const { isAdmin, isAdminOrManager, verifyToken } = require('../../../middleware/auth');
const { verifyEmail, verifyPhone } = require('../../../middleware/verify');
const employeeServices = require('../../services/admin/employee');
router
  .route('/')
  .post(
    // verifyToken, isAdminOrManager, verifyEmail,
     employeeServices.addEmployee)
  .get(
    // verifyToken,
     employeeServices.getEmployees);
router.route('/import').post(employeeServices.importExelEmployees);
router.route('/export').post(employeeServices.exportEmployee);
router
  .route('/:id')
  .put(
    // verifyToken, isAdminOrManager, 
    employeeServices.updateEmployee)
  .delete(
    // verifyToken, isAdmin,
     employeeServices.deleteEmployee)
  .get(
    // verifyToken,
     employeeServices.getDetailEmployee);
module.exports = router;
