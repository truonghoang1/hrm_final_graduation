const express = require('express')
const router = express.Router()
const { verifyEmail,verifyPassword } = require('../../../middleware/verify')
const {verifyToken}  = require("../../../middleware/auth")
const userServices = require('../../services/admin/user')

router.route('/signup').post(verifyEmail,verifyPassword,userServices.signup)
router.route('/signin').post(userServices.signin)
router.route("/statistial").get(verifyToken,userServices.statistial)


module.exports = router