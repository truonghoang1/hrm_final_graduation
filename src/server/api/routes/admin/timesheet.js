const express = require('express');
const router = express.Router();

const { isAdmin, isAdminOrManager, verifyToken } = require('../../../middleware/auth');
const { verifyEmail, verifyPhone } = require('../../../middleware/verify');
const documentServices = require('../../services/admin/timesheet');
router
  .route('/')
  .get(
    // verifyToken,
     documentServices.getListLeaveApp);

// router.route('/export').post(employeeServices.exportEmployee);
router
  .route('/:id')
  .put(
    // verifyToken, isAdminOrManager, 
    documentServices.updateStatusDocoment)
 
module.exports = router;
