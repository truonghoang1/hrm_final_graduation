const express = require('express')
const router = express.Router()

const {isAdmin,verifyToken} = require('../../../middleware/auth')
const bonusServices = require("../../services/admin/bonus")
router.route('/').
post(
    // verifyToken,
    bonusServices.addBonus)
.get(
    // verifyToken,
    bonusServices.getBonus)
router.route('/:id')
.put(
    // verifyToken,isAdmin,
    bonusServices.updateBonus)
.delete(
    // verifyToken,isAdmin,
    bonusServices.deleteBonus)

module.exports = router