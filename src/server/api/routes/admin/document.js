const express = require('express')
const router = express.Router()

const {isAdmin,isAdminOrManager,verifyToken} = require('../../../middleware/auth')
const {verifyEmail} =require("../../../middleware/verify")
const documentServices = require("../../services/admin/document")
router.route('/').
post(
    // verifyToken,isAdminOrManager,
    documentServices.addDocuments)
.get(
    // verifyToken,
    documentServices.getListDocuments)
router.route('/:id')
.put(
    // verifyToken,isAdminOrManager,
    documentServices.updateDocuments)
.delete(
    // verifyToken,isAdmin,
    documentServices.deleteDocuments)

module.exports = router