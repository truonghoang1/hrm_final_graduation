const express = require('express')
const router = express.Router()

const {isSuperAdmin,verifyToken} = require('../../../middleware/auth')
const rolesServices = require('../../services/admin/roles')
router.route('/').post(rolesServices.addRole).get(rolesServices.getRole)
router.route("/:id").get(rolesServices.getDetailRole).put(rolesServices.updateRole).delete(rolesServices.deleteRole)
module.exports = router