const express = require('express')
const router = express.Router()

const {isAdmin,isAdminOrManager,verifyToken} = require('../../../middleware/auth')
const {verifyEmail} =require("../../../middleware/verify")
const departmentServices = require("../../services/admin/department")
router.route('/').
post(
    // verifyToken,isAdminOrManager,
    departmentServices.addDepartment)
.get(
    // verifyToken,
    departmentServices.getDepartment)
router.route('/:id')
.put(
    // verifyToken,isAdminOrManager,
    departmentServices.updateDepartment)
.delete(
    // verifyToken,isAdmin,
    departmentServices.deleteDepartment)
.get(
    // verifyToken,
    departmentServices.getDetailDepartment)
module.exports = router