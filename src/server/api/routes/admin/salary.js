const express = require('express')
const router = express.Router()

const { isAdmin, isAdminOrManager, verifyToken } = require('../../../middleware/auth')
const { verifyEmail } = require("../../../middleware/verify")
const salaryAccountServices = require("../../services/admin/salary")
router.route('/').
    post(
        // verifyToken, isAdminOrManager,
        salaryAccountServices.addSalaryAccount)
    .get(
        // verifyToken,
        salaryAccountServices.getSalaryAccount)
router.route('/:id')
    .put(
        // verifyToken, isAdminOrManager,
        salaryAccountServices.updateSalaryAccount)
    .delete(
        // verifyToken, isAdmin,
        salaryAccountServices.deleteSalaryAccount)
    .get(
        // verifyToken,
        salaryAccountServices.getDetailSalaryAccount)
router.route("/import/salary").post(salaryAccountServices.importExelSalary)
router.route("/export/salary").post(salaryAccountServices.exportSalary)
module.exports = router