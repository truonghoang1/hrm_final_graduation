const express = require('express')
const router= express.Router()
const userRoute = require('./user')
const timesheetRoute = require('./timesheet')
router.use('/api',userRoute)
router.use('/api',timesheetRoute)
module.exports= router