const express = require('express')
const router = express.Router()
const { verifyEmail, verifyPhone,verifyPassword } = require('../../../middleware/verify')
const {verifyToken,isAdmin} = require('../../../middleware/auth')
const userServices = require('../../services/user/user')

router.route('/register').post( verifyEmail,verifyPassword,userServices.registerGuest)
router.route('/activation').post(userServices.activeAccount)
router.route('/forgot-password').post(userServices.forgotPassword)
router.route('/reset').post(verifyPassword,userServices.resetPassword)
router.route("/changepass").post(verifyToken,verifyPassword,userServices.changePw)
router.route("/update-profile").post(verifyToken,userServices.updateProfile)
module.exports = router