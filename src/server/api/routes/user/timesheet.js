const express = require('express')
const router = express.Router()
const {verifyToken} = require('../../../middleware/auth')
const timeServices = require('../../services/user/timesheet')

router.route('/timesheet').get(verifyToken,timeServices.getListTime)
router.route('/log-time').post(verifyToken,timeServices.addTime)
router.route('/leave-date').get(verifyToken,timeServices.getDateLeave)
router.route('/leave-app').get(verifyToken,timeServices.listLeaveApplication).post(verifyToken,timeServices.addLeaveApplication)
module.exports = router