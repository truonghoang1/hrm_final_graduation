const cpuLength = require('os').cpus().length
module.exports = {
  "apps": [
      {
          "name": "HRM-server",
          "script": "./build/server",
          "instances":1,
        //   "instances": cpuLength > 32? 32 : cpuLength - 1,
          "exec_mode":"fork",
          "watch":true,
          "ignore_watch":["node_modules"],
          "env": {
              "NODE_ENV": "development",
               "PORT":3001,
          },
          "log_date_format": "YYYY-MM-DD HH:mm:ss",
          "error_file": "./logs/pm2.err",
          "out_file": "./logs/pm2.log"
      }
  ]
};