require('dotenv').config();
const mongoose = require('mongoose');
const {model} = mongoose
const { success, failure } = require('../src/server/common/res');

const bcrypt = require('bcryptjs');
const   Employees = model('Employees'),
Roles = model('Roles'),
Account = model('Accounts'),
LeaveDay = model('LeaveDays');

const Test = async(req,res)=>{
    const generatePerson = () => {
        const firstNames = ["John", "Jane", "Jessica", "Jim", "Jill","Truong","Hoang","Hien","Huyen","Minh","Lien","Tuan","Edsel"];
        const lastNames = ["Doe", "Smith", "Johnson", "Williams", "Brown","Madesn","Andrew","Susan","Taylor"];
        const genders = ["male", "female"];
        const department = "63ea22693c2db6e4715f5efd";
        const emailDomains = ["gmail.com", "vccorp.vn", "yahoo.com"];
      
        const fullName = `${firstNames[Math.floor(Math.random() * firstNames.length)]} ${
          lastNames[Math.floor(Math.random() * lastNames.length)]
        }`;
        const gender = genders[Math.floor(Math.random() * genders.length)];
        
        const cmnd = Math.floor(Math.random() * 1000000000000).toString();
        const email = `${fullName.replace(" ", ".")}@${
          emailDomains[Math.floor(Math.random() * emailDomains.length)]
        }`;
      
        return { fullName, gender, department, cmnd, email };
      };
      
      const people = [];
      for (let i = 0; i < 10; i++) {
        people.push(generatePerson());
      }
      console.log(people);
     
    
    //   let arrAccount =[];
    //       let roleEmployee 
    //       const role = await Roles.findOne({ level: 3 });
    //       if(!role){
    //          const newRole = new Roles({name:"employee",level:3})
    //          newRole.save().then((data)=>{
    //               roleEmployee=data._id
    //          }).catch(err=>{return failure(res,400,err)})
    //       }else{
    //         roleEmployee=role._id
    //       }
    //      const newPassword = await bcrypt.hash("Truong652000.", 10)
    //      people.forEach((element) => {
    //         if(Object.hasOwnProperty("email")){
    //           return failure(res,400,"Email chưa được thêm")
    //         }else{
    //           console.time("map")
    //           arrAccount.push({
    //               email: element.email,
    //                password:newPassword,
    //               role: roleEmployee,
    //             })
    //           console.timeEnd("map")
    //         }
              
    //       });
          
    //       Promise.all([Account.insertMany(arrAccount),Employees.insertMany(people)]).then(result=>{
    //           if(result){
    //          Employees.countDocuments({},(err,data2)=>{
    //           success(res, 200, "success", { totalPage:Math.ceil(data2/10) });
    //         })
    
    //           }
    
    //       }).catch(err=>{return failure(res,400,err)})
}
const deleteAll =(req,res)=>{
    Account.deleteMany().then(()=>{
        res.json("success")
    })
}
module.exports={Test,deleteAll}