const webpack = require("webpack");
const path = require("path");
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin')

const config = {
    mode: process.env.NODE_ENV,
    entry: {
        app: path.resolve(__dirname, '../src/client/index.js'),
       
    },
    module: {
        rules: [ {
            test: /\.(js|jsx)$/, //check for all js files
            exclude: /(node_modules)/,
            use: ['babel-loader'],
          } , {
            test: /\.(css|scss|sass|less)?$/,
            use: ['style-loader','css-loader','sass-loader'],
          },
          {
            test: /\.(jpg|png|woff|woff2|eot|ttf|svg|otf|ico)$/i,
            type: 'asset/resource', // load resource same as url image
           
          },
       
        ]
    },
    devServer: {
        historyApiFallback: true, // khi server response 400 return public path
        port: 3000, //port client
        hot: true,
        open: true,
       
      },
      optimization:{
        splitChunks:{
            chunks:'async',
            minChunks:1,
            filename:'[name].js'
        }
      },
    plugins:[
        //default cleanwbplugin choose output.path
        //  new CleanWebpackPlugin(),
         new webpack.DefinePlugin({'process.env.NODE_ENV':JSON.stringify(process.env.NODE_ENV)}),
        new HtmlWebpackPlugin({
            template: 'src/public/index.html',
            // favicon: 'src/public/favicon.ico',
            inject: true,
          }),
         
      
    ],
    resolve: {
        // extensions: ['.js', '.jsx'],
        alias: {
           
          },
    },
    output: {
        path: path.resolve(__dirname, '../src/public/dist/client'),
        publicPath: '/',
        filename: '[name].js',
        chunkFilename:'[name].[hash].js'
    }
};

if (process.env.NODE_ENV == 'production') {
    config.plugins.push(new webpack.optimize.UglifyJsPlugin({
        compress: {
            warnings: false,
            drop_console: true
        },
        uglifyOptions: {
            compress: {
                warnings: false,
                drop_console: true,
                drop_debug: true
            },
            output: {
                minimize: true,
                comments: false,
            },
            mangle: true,
        },
        extractComments: {
            banner: banner
        }
    }));
} else {
    config.devtool = 'source-map';
}

module.exports = config;